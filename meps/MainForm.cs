﻿using meps.Forms;
using System;
using System.Collections.Generic;
using meps.Table;
using System.Windows.Forms;
using System.Drawing;
using meps.Utils;

using System.Linq;
using System.Text;

namespace meps
{
    public partial class MainForm : Form
    {
        private BindingSource bsIYM = new BindingSource();

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ログイン画面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            //初期設定の確認
            while (!DB.ConnectTest())
            {
                //データベースに接続できなかった場合
                var ret = MessageBox.Show("設定メニューからサーバーのIPアドレスを指定してください",
                    "データベースに接続できません", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                //キャンセルが押されたらプログラム終了
                if (ret != DialogResult.OK)
                {
                    Environment.Exit(0);
                    Close();
                    return;
                }
                //設定画面を表示する
                using (var f = new SettingForm())
                //using (var f = new SettingForm(true))
                {
                    f.ShowDialog();
                }
            }

            //20190725111802 furukawa st ////////////////////////
            //試験サーバの場合色変える            
            CommonTool.setFormColor(this);
            //20190725111802 furukawa ed ////////////////////////

            //設定情報等をロード
            Pref.GetPrefList();
            User.GetUsers();

            //ログイン画面表示
            using (var f = new LoginForm())
            {
                f.ShowDialog();
            }

            //審査年月別登録件数を表示
            iymListUpdate();

            //20201124095006 furukawa st ////////////////////////
            //version
            
            System.Reflection.Assembly ass = System.Reflection.Assembly.GetExecutingAssembly();
            Version ver = ass.GetName().Version;
            labelVersion.Text = $"version {ver.Major}.{ver.Minor}.{ver.Build}.{ver.Revision.ToString().Substring(2)}";
            //20201124095006 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 審査年月別登録件数を更新します
        /// </summary>
        private void iymListUpdate()
        {
            var list = MediAppInfo.GetCountList();
            bsIYM.DataSource = list;
            dataGridViewMonth.DataSource = bsIYM;
            dataGridViewMonth.Columns[nameof(MediAppInfo.MAID)].Visible = false;
            dataGridViewMonth.Columns[nameof(MediAppInfo.IYM)].Visible = false;
            dataGridViewMonth.Columns[nameof(MediAppInfo.PID)].Visible = false;
            dataGridViewMonth.Columns[nameof(MediAppInfo.IY)].HeaderText = "年";
            dataGridViewMonth.Columns[nameof(MediAppInfo.IM)].HeaderText = "月";
            dataGridViewMonth.Columns[nameof(MediAppInfo.MAICount)].HeaderText = "件数";
            dataGridViewMonth.ColumnHeadersVisible = true;
            dataGridViewMonth.RowHeadersVisible = false;
            dataGridViewMonth.Columns[nameof(MediAppInfo.IY)].Width = 50;
            dataGridViewMonth.Columns[nameof(MediAppInfo.IM)].Width = 50;
            dataGridViewMonth.Columns[nameof(MediAppInfo.MAICount)].Width = 80;

            dataGridViewMonth.DefaultCellStyle.Font = new Font("Arial", 12);
            dataGridViewMonth.DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            dataGridViewMonth.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        /// <summary>
        /// 終了
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
            Close();
            return;
        }

        /// <summary>
        /// システム設定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSettings_Click(object sender, EventArgs e)
        {
            using (var f = new SettingForm())
            {
                f.ShowDialog();
            }
        }        

        /// <summary>
        /// 画像管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonScan_Click(object sender, EventArgs e)
        {
            var mai = (MediAppInfo)bsIYM.Current;
            using (var f = new ScanForm())
            {
                f.ShowDialog();
            }
            //リスト更新 
            iymListUpdate();
        }

        /// <summary>
        /// データ入力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput_Click(object sender, EventArgs e)
        {
            var mai = (MediAppInfo)bsIYM.Current;
            if (mai == null) return;
            using (var f = new PrefSelectForm(mai.IYM, mai.IY, mai.IM))
            {
                f.ShowDialog();
            }
        }

        /// <summary>
        /// 納品データ出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDataExport_Click(object sender, EventArgs e)
        {
            MessageBox.Show("現在出力はMejorからのみ行えます。", "システム担当より");
            return;
            //var mai = (MediAppInfo)bsIYM.Current;
            //if (mai == null) return;
            //using (var f=new OutputForm(mai.IYM))
            //{
            //    f.ShowDialog();
            //}
        }

        /// <summary>
        /// 照会管理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInquiry_Click(object sender, EventArgs e)
        {
            MessageBox.Show("申し訳ございません。\r\n現在この機能はご利用になれません。", "お詫び");
            return;
            //using (var f = new InquiryForm())
            //{
            //    f.ShowDialog();s
            //}
        }

        /// <summary>
        /// 簡易検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSimpleSearch_Click(object sender, EventArgs e)
        {
            /*
             * 未着手
             */
        }

        /// <summary>
        /// その他
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOther_Click(object sender, EventArgs e)
        {
            /*
             * 未着手
             */
        }
        
    }
}
