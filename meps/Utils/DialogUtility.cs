﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Utils
{
    public class DialogUtility
    {
        public class OpenFolderDialog
        {
            public string FolderPath { get; private set; } = string.Empty;

            public DialogResult ShowDialog()
            {
                using (var f = new CommonOpenFileDialog())
                {
                    // フォルダーを開く設定に
                    f.IsFolderPicker = true;

                    // 読み取り専用フォルダ/コントロールパネルは開かない
                    f.EnsureReadOnly = false;
                    f.AllowNonFileSystemItems = false;

                    if (f.ShowDialog(ownerWindowHandle: Form.ActiveForm.Handle) == CommonFileDialogResult.Ok)
                    {
                        FolderPath = f.FileName;
                        return DialogResult.OK;
                    }
                    else
                    {
                        return DialogResult.Cancel;
                    }
                }
            }
        }
    }
}
