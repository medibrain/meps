﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Utils
{
    class Converter
    {
        /// <summary>
        /// 文字列をint型に変換します。失敗した場合は0を返します。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(string value)
        {
            int result;
            if(int.TryParse(value, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }
    }
}
