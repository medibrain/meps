﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Utils
{
    static class CommonTool
    {
        //20190725111638 furukawa 試験サーバの場合色変える

        /// <summary>
        /// 本番サーバでない場合、フォーム背景色をThistleにする
        /// </summary>
        /// <param name="frm">フォーム</param>
        public static void setFormColor(Form frm)
        {
            //20201123181052 furukawa st ////////////////////////
            //aws用追加

            //本番サーバでない場合、フォーム背景色をThistleにする


            //20210402134326 furukawa st ////////////////////////
            //testが入っていたらawsテスト用、内部サーバ削除
            System.Drawing.Color clr =
                 !Settings.DataBaseHost.Contains("test") ? System.Drawing.Color.Honeydew : System.Drawing.ColorTranslator.FromHtml("#FF48D1CC");


            //      System.Drawing.Color clr =
            //           Settings.DataBaseHost.Contains("aws") ? System.Drawing.Color.Honeydew
            //           : Settings.DataBaseHost != "192.168.200.100" ? System.Drawing.Color.Thistle : System.Drawing.SystemColors.Control;

            //              //Settings.DataBaseHost != "192.168.200.100" ? System.Drawing.Color.Thistle : System.Drawing.SystemColors.Control;


            //20210402134326 furukawa ed ////////////////////////

            //20201123181052 furukawa ed ////////////////////////

            frm.BackColor = clr;
        }
    }
}
