﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace meps.Utils
{
    public class Settings
    {
        static string fileName = System.Windows.Forms.Application.StartupPath + "\\settings.xml";
        static XDocument xdoc;

        static Settings()
        {
            if (System.IO.File.Exists(fileName))
            {
                xdoc = XDocument.Load(fileName);
            }
            else
            {
                xdoc = new XDocument();
                xdoc.Add(new XElement("Settings"));
                xdoc.Save(fileName);
            }
        }


        private static void setValue(string name, string value)
        {
            xdoc.Element("Settings").SetElementValue(name, value);
            xdoc.Save(fileName);
        }

        private static string getValue(string name)
        {
            if (xdoc.Element("Settings").Element(name) == null) return string.Empty;
            return xdoc.Element("Settings").Element(name).Value;
        }

        /// <summary>
        /// 共通設定プリンタ
        /// </summary>
        public static string DefaultPrinterName
        {
            get { return getValue("DefaultPrinterName"); }
            set { setValue("DefaultPrinterName", value); }
        }

        /// <summary>
        /// 点検画面での使用プリンタ名
        /// </summary>
        public static string InspectPrinterName
        {
            get { return getValue("InspectPrinterName"); }
            set { setValue("InspectPrinterName", value); }
        }

        /// <summary>
        /// 照会画面での使用プリンタ名
        /// </summary>
        public static string ShokaiPrinterName
        {
            get { return getValue("ShokaiPrinterName"); }
            set { setValue("ShokaiPrinterName", value); }
        }

        /// <summary>
        /// データベースのホストアドレス
        /// </summary>
        public static string DataBaseHost
        {
            get { return getValue("DataBaseHost"); }
            set { setValue("DataBaseHost", value); }
        }

        /// <summary>
        /// データベースのタイムアウト秒
        /// </summary>
        public static string CommandTimeout
        {
            get { return getValue("CommandTimeout"); }
            set { setValue("CommandTimeout", value); }
        }

        /// <summary>
        /// 印刷時マージン
        /// </summary>
        public static int PrintMargin
        {
            get { return int.Parse(getValue("PrintMargin")); }
            set { setValue("PrintMargin", value.ToString()); }
        }

        /// <summary>
        /// スキャン画像のホストアドレス
        /// </summary>
        public static string ImageHost
        {
            get { return getValue("ImageHost"); }
            set { setValue("ImageHost", value); }
        }
    }
}
