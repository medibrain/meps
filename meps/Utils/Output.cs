﻿using meps.Forms;
using meps.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Utils
{
    class Output
    {
        /// <summary>
        /// 申請書データをCSV形式で出力します。
        /// 画像ファイルも同時に出力する場合はtrueを指定します。
        /// oneImageOutputtedCallbackには出力したMediApp#MAIDが渡されます。
        /// </summary>
        /// <param name="iym"></param>
        /// <param name="pref"></param>
        /// <param name="imageNeed"></param>
        /// <param name="oneImageOutputtedCallback"></param>
        /// <returns></returns>
        public static bool MediApp(int iym, Pref pref, string saveFolderPath, bool imageNeed, Action<int> oneImageOutputtedCallback)
        {
            string DIR_BASE_NAME = $"{saveFolderPath}\\{pref.PID.ToString("00")}_{pref.Name}\\{iym}";
            string DIR_HNR_NAME = "再審査返戻";

            var wf = new WaitForm();
            try
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
                while (!wf.Visible) System.Threading.Thread.Sleep(10);

                wf.LogPrint("申請書を取得しています");
                var maList = Table.MediApp.GetMediAppsByPIDIYM(iym, pref.PID);

                if(imageNeed) wf.LogPrint("CSVファイルと画像の出力を開始します");
                else wf.LogPrint("CSVファイルを作成しています");

                wf.SetMax(maList.Count());
                wf.BarStyle = ProgressBarStyle.Continuous;

                //保存先フォルダ作成（バッチフォルダはループ内で）
                if (!FileUtility.CreateDirectories($"{DIR_BASE_NAME}\\")) return false;

                //出力先
                var today = DateTime.Today;
                var yy = today.Year.ToString().Substring(2, 2);
                var MM = today.Month.ToString("00");
                var dd = today.Day.ToString("00");
                var csvPath = $"{DIR_BASE_NAME}\\RER{yy}{MM}{dd}.csv";

                //出力(画像込み)
                using (var sw = new System.IO.StreamWriter(csvPath, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    //高速コピー
                    var fc = new TiffUtility.FastCopy();

                    //（再審査再入力対応のため）
                    //CSV書き込みはバッファに貯めて行う
                    var maBuffer = new List<MediApp>();

                    int pageCount = 1;
                    string currBatch = "NULL";
                    int currNumbering = -1;

                    foreach (var ma in maList)
                    {
                        //バッチ
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.バッチ)
                        {
                            //バッチ番号を覚える
                            currBatch = getVal(ma.Batch);

                            //バッチフォルダ作成
                            if (!FileUtility.CreateDirectories($"{DIR_BASE_NAME}\\{currBatch}\\")) return false;

                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        //申請書
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.申請書)
                        {
                            //バッファにある申請書を書き込む
                            foreach (var bufMA in maBuffer)
                            {
                                if (bufMA.MAType == (int)Consts.MEDIAPP_TYPE.申請書)
                                {
                                    var vals = getCSVBase(bufMA, bufMA.Again);
                                    sw.WriteLine(string.Join(",", vals));
                                    break;
                                }
                            }

                            //ナンバリングを覚える（上書き）
                            currNumbering = ma.Numbering;

                            //すべて放出（２連続申請書による重複書き込みを回避するため）
                            maBuffer.Clear();

                            //続紙カウント初期化
                            pageCount = 1;

                            //バッチ番号を入れる（次の申請書までにバッチが登場すると上書きされる為）
                            ma.Batch = currBatch;

                            //再審査再入力用に今すぐには書き込まずバッファにためる
                            maBuffer.Add(ma);

                            //画像コピー
                            var cResult = imageCopy(
                                fc, iym, ma, currBatch, pageCount, false,
                                DIR_BASE_NAME, DIR_HNR_NAME);
                            if (!cResult) return false;

                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        //続紙
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.続紙)
                        {
                            //ページカウントを増やす
                            pageCount++;

                            //ナンバリングを引き継ぐ
                            ma.Numbering = currNumbering;

                            //画像コピー
                            var cResult = imageCopy(
                                fc, iym, ma, currBatch, pageCount, false,
                                DIR_BASE_NAME, DIR_HNR_NAME);
                            if (!cResult) return false;

                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        //付箋
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.付箋)
                        {
                            //ページカウントを増やす
                            pageCount++;

                            //バッファにある申請書の再審査再入力フラグたてて書き込む（それ以外はCSVには不要）
                            foreach (var bufMA in maBuffer)
                            {
                                if (bufMA.MAType == (int)Consts.MEDIAPP_TYPE.申請書)
                                {
                                    var vals = getCSVBase(bufMA, ma.Again);
                                    sw.WriteLine(string.Join(",", vals));
                                    break;
                                }
                            }

                            //すべて放出（２連続付箋による重複書き込みを回避するため）
                            maBuffer.Clear();

                            //ナンバリングを引き継ぐ
                            ma.Numbering = currNumbering;

                            //画像コピー
                            var cResult = imageCopy(
                                fc, iym, ma, currBatch, pageCount, false,
                                DIR_BASE_NAME, DIR_HNR_NAME);
                            if (!cResult) return false;

                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        //不要
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.不要)
                        {
                            //何もしない
                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        //再審査返戻
                        if (ma.MAType == (int)Consts.MEDIAPP_TYPE.再審査返戻)
                        {
                            //保存先フォルダ作成
                            if (!FileUtility.CreateDirectories($"{DIR_BASE_NAME}\\{DIR_HNR_NAME}\\")) return false;

                            //続紙カウント初期化
                            pageCount = 1;

                            //再審査返戻の続紙対応のために覚える（自分のは入ってる）
                            currNumbering = ma.Numbering;

                            //画像コピー
                            var cResult = imageCopy(
                                fc, iym, ma, currBatch, pageCount, true,
                                DIR_BASE_NAME, DIR_HNR_NAME);
                            if (!cResult) return false;

                            oneImageOutputtedCallback(ma.MAID);
                            continue;
                        }

                        oneImageOutputtedCallback(ma.MAID);
                    }

                    //最後のが書き込まれないので残っていたらここで書き込む
                    foreach (var bufMA in maBuffer)
                    {
                        if (bufMA.MAType == (int)Consts.MEDIAPP_TYPE.申請書)
                        {
                            var vals = getCSVBase(bufMA, bufMA.Again);
                            sw.WriteLine(string.Join(",", vals));
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            finally
            {
                wf.InvokeCloseDispose();
            }

            return true;            
        }

        /// <summary>
        /// CSV書き込み用配列を取得します。
        /// </summary>
        /// <param name="ma"></param>
        /// <param name="again"></param>
        /// <returns></returns>
        private static string[] getCSVBase(MediApp ma, int again)
        {
            var vals = new string[44];
            var i = 0;
            vals[i++] = getVal(DateTimeEx.GetGyymmFromAdYM(ma.IYM));
            vals[i++] = getVal(ma.Batch);
            vals[i++] = getVal(ma.Numbering, 6);
            vals[i++] = getVal(DateTimeEx.GetGyymmFromAdYM(ma.MYM));
            vals[i++] = getVal(ma.MeisaiType);
            vals[i++] = getVal(ma.IppanRouken);
            vals[i++] = getVal(ma.SyahoKouhi);
            vals[i++] = getVal(ma.HonninKazoku);
            vals[i++] = getVal(ma.PrefNum);
            vals[i++] = getVal(ma.HospitalCode);
            vals[i++] = "";//予備
            vals[i++] = getVal(ma.Kouhi1Code, 2);
            vals[i++] = getVal(ma.INum);
            vals[i++] = getVal(ma.HNum);
            vals[i++] = getVal(ma.Sex);
            vals[i++] = getVal((DateTimeEx.GetIntJpDateWithEraNumber(ma.Birthday) / 100));
            vals[i++] = getVal(ma.Tokki, 2);
            vals[i++] = getValOrDefault(ma.TuuchiType, "0");
            vals[i++] = "";//予備
            vals[i++] = "";//処方箋
            vals[i++] = getVal(ma.IhoDays, 2);
            vals[i++] = getVal(ma.IhoPoint);
            vals[i++] = getVal(ma.IhoPrice);
            vals[i++] = getVal(ma.Kouhi1Days, 2);
            vals[i++] = getVal(ma.Kouhi1Point);
            vals[i++] = getVal(ma.Kouhi1Price);
            vals[i++] = getVal(ma.Kouhi2Days, 2);
            vals[i++] = getVal(ma.Kouhi2Point);
            vals[i++] = getVal(ma.Kouhi2Price);
            vals[i++] = getVal(ma.SyokujiIhoDays, 2);
            vals[i++] = getVal(ma.SyokujiIhoMediPrice);
            vals[i++] = getVal(ma.SyokujiIhoHutanPrice);
            vals[i++] = getVal(ma.SyokujiKouhi1Days, 2);
            vals[i++] = getVal(ma.SyokujiKouhi1MediPrice);
            vals[i++] = getVal(ma.SyokujiKouhi1HutanPrice);
            vals[i++] = getVal(ma.SyokujiKouhi2Days, 2);
            vals[i++] = getVal(ma.SyokujiKouhi2MediPrice);
            vals[i++] = getVal(ma.SyokujiKouhi2HutanPrice);
            vals[i++] = getVal(ma.KougakuPrice);
            vals[i++] = getValOrDefault(again, "0");
            vals[i++] = "";
            vals[i++] = "";
            vals[i++] = "";
            vals[i++] = getVal(ma.Kouhi2Code, 2);

            return vals;
        }

        /// <summary>
        /// 画像をコピーします。
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="iym"></param>
        /// <param name="ma"></param>
        /// <param name="batch"></param>
        /// <param name="pageCount"></param>
        /// <param name="hnr"></param>
        /// <param name="DIR_BASE_NAME"></param>
        /// <param name="DIR_HNR_NAME"></param>
        /// <param name="DIR_IMAGE_NAME"></param>
        /// <returns></returns>
        private static bool imageCopy(
            TiffUtility.FastCopy fc, int iym, MediApp ma, string batch, int pageCount, bool hnr, 
            string DIR_BASE_NAME, string DIR_HNR_NAME)
        {
            //コピー元ファイルパス
            var distPath = $"{ScanImage.GetImageFolderPath()}\\{iym}\\{ma.PID}\\{ma.FileName}";

            //送信先ファイルパス
            var yy = iym.ToString().Substring(2, 2);
            var MM = iym.ToString().Substring(4, 2);
            var fileName = $"{yy}{MM}{batch}{getVal(ma.Numbering, 6)}{getVal(pageCount, 2)}.tif";

            string destPath;
            if (hnr)
            {
                //再審査返戻
                destPath = $"{DIR_BASE_NAME}\\{DIR_HNR_NAME}\\{fileName}";
            }
            else
            {
                //以外
                destPath = $"{DIR_BASE_NAME}\\{batch}\\{fileName}";
            }

            return fc.FileCopy(distPath, destPath);
        }

        /// <summary>
        /// int型の値を文字列に変換します。値が-1である場合は空文字列が返ります。
        /// ０埋めが必要な場合は引数を指定します。
        /// </summary>
        /// <param name="val"></param>
        /// <param name="need0Length"></param>
        /// <returns></returns>
        private static string getVal(int val, int need0Length = 0)
        {
            if (val == -1) return "";
            if (need0Length > 0)
            {
                var need0 = "";
                for (var i = 0; i < need0Length; i++) need0 += "0";
                return val.ToString(need0);
            }
            else return val.ToString();
        }

        /// <summary>
        /// string型の値を文字列に変換します。値がnullである場合は空文字列が返ります。
        /// ０埋めが必要な場合は引数を指定します。
        /// </summary>
        /// <param name="val"></param>
        /// <param name="need0Length"></param>
        /// <returns></returns>
        private static string getVal(string val, int need0Length = 0)
        {
            if (string.IsNullOrWhiteSpace(val)) return "";
            if (need0Length > 0)
            {
                return val.PadLeft(need0Length, '0');
            }
            else return val;
        }

        /// <summary>
        /// int型の値を文字列に変換します。値が-1の場合は引数に指定した値を返します。
        /// </summary>
        /// <param name="val"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static string getValOrDefault(int val, string defaultValue)
        {
            if (val == -1) return defaultValue;
            else return val.ToString();
        }
    }
}
