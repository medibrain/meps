﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace meps.Utils
{
    static class DateTimeEx
    {
        public static DateTime DateTimeNull = DateTime.MinValue;
        public static string ErrorMessage = string.Empty;
        public static string EraString { get; private set; }
        static List<EraJ> EraList = new List<EraJ>();
        public static System.Globalization.CultureInfo culture;

        static DateTimeEx()
        {
            culture = new System.Globalization.CultureInfo("ja-JP", true);
            culture.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
            GetEraList();
        }

        internal class EraJ
        {
            internal DateTime SDate = DateTime.MaxValue;
            internal DateTime EDate = DateTime.MaxValue;
            internal string Name = string.Empty;
            internal string ShortName = string.Empty;
            internal string Initial = string.Empty;
            internal int Number = 0;
            internal int Difference = 0;
            internal int max = 0;//20190724142509 furukawa 和暦の最大年数
                                 
        }

        /// <summary>
        /// 和暦年号一覧を指定します
        /// </summary>
        /// <returns></returns>
        private static void GetEraList()
        {
            var m = new EraJ();
            m.SDate = new DateTime(1868, 9, 8);
            m.EDate = new DateTime(1912, 7, 29);
            m.Name = "明治";
            m.ShortName = "明";
            m.Number = 1;
            m.Initial = "M";
            m.Difference = 1867;
            m.max = 45;//20190724142612 furukawa 和暦の最大年数
            EraList.Add(m);

            var t = new EraJ();
            t.SDate = new DateTime(1912, 7, 30);
            t.EDate = new DateTime(1926, 12, 24);
            t.Name = "大正";
            t.ShortName = "大";
            t.Number = 2;
            t.Initial = "T";
            t.Difference = 1911;
            t.max = 15;//20190724142612 furukawa 和暦の最大年数
            EraList.Add(t);

            var s = new EraJ();
            s.SDate = new DateTime(1926, 12, 25);
            s.EDate = new DateTime(1989, 1, 7);
            s.Name = "昭和";
            s.ShortName = "昭";
            s.Number = 3;
            s.Initial = "S";
            s.Difference = 1925;
            s.max = 64;//20190724142612 furukawa 和暦の最大年数
            EraList.Add(s);

            var h = new EraJ();
            h.SDate = new DateTime(1989, 1, 8);

            //20190724140057 furukawa st ////////////////////////
            //平成最後の日
            
            h.EDate = new DateTime(2019, 4, 30);
            //h.EDate = DateTime.MaxValue;
            //20190724140057 furukawa ed ////////////////////////


            h.Name = "平成";
            h.ShortName = "平";
            h.Number = 4;
            h.Initial = "H";
            h.Difference = 1988;
            h.max = 31;//20190724142612 furukawa 和暦の最大年数
            EraList.Add(h);


            //20190724140029 furukawa st ////////////////////////
            //令和対応
            
            var r = new EraJ();
            r.SDate = new DateTime(2019, 5, 1);
            r.EDate = DateTime.MaxValue;
            r.Name = "令和";
            r.ShortName = "令";
            r.Number = 5;
            r.Initial = "R";
            r.Difference = 2018;
            r.max = 99;//20190724142612 furukawa 和暦の最大年数
            EraList.Add(r);
            //20190724140029 furukawa ed ////////////////////////



            //年号を表す文字列を整備
            foreach (var item in EraList)
            {
                EraString += item.ShortName;
                EraString += item.Initial;
                EraString += item.Initial.ToLower();
            }
        }


        /// <summary>
        /// 年号の略(英頭文字)を取得します。なかった場合、空白が返ります。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetEraShort(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].ShortName;
            }

            return string.Empty;
        }

        /// <summary>
        /// 年号を漢字正式名称で取得します
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetEra(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// 年号を表す数字(昭和=3、平成=4)を取得します。なかった場合、0が返ります。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetEraNumber(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Number;
            }

            return 0;
        }

        /// <summary>
        /// 対応する和暦の年を返します。なかった場合、0が返ります。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetJpYear(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return dt.Year - EraList[i].Difference;
            }

            return 0;
        }

        /// <summary>
        /// 対応する和暦の年月を正式年号付きで返します。なかった場合、0が返ります。
        /// </summary>
        public static string GetEraJpYearMonth(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00年") + dt.ToString("MM月");
            }

            return string.Empty;
        }

        /// <summary>
        /// 対応する和暦の年を正式年号付きで返します。なかった場合、0が返ります。
        /// </summary>
        public static string GetEraJpYear(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
            }

            return string.Empty;
        }


        /// <summary>
        /// 対応する和暦の年を英頭文字付きで返します。なかった場合、0が返ります。
        /// </summary>
        public static string GetShortEraJpYear(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00");
            }

            return string.Empty;
        }

        /// <summary>
        /// 対応する和暦の年を昭和=3とするJYY形式のint型で返します。
        /// 失敗した場合0が返ります。
        /// </summary>
        public static int GetEraNumberYear(DateTime dt)
        {
            for (int i = 0; i < EraList.Count; i++)
            {
                if (EraList[i].SDate > dt) continue;
                if (EraList[i].EDate < dt) continue;
                return EraList[i].Number * 100 + (dt.Year - EraList[i].Difference);
            }

            return 0;
        }

        /// <summary>
        /// 年号(1or2文字)から昭和=3とする番号を取得します。失敗した場合0が返ります。
        /// </summary>
        /// <param name="era"></param>
        /// <returns></returns>
        public static int GetEraNumber(string era)
        {
            int number = 0;
            if (era == null || era.Length < 1 || era.Length > 3) return number;
            foreach (var item in EraList)
            {
                if (era.Length == 1)
                {
                    if (item.ShortName == era)
                    {
                        number = item.Number;
                        break;
                    }
                }
                else
                {
                    if (item.Name == era)
                    {
                        number = item.Number;
                        break;
                    }
                }
            }
            return number;
        }


        /// <summary>
        /// 年号(2文字)つきの和暦年から、西暦年を取得します。失敗した場合0が返ります。
        /// </summary>
        /// <param name="jyear">JJyy</param>
        /// <returns></returns>
        public static int GetAdYear(string jyear)
        {
            int year = 0;
            if (jyear.Length < 3) return 0;
            string era = jyear.Remove(2);
            if (!int.TryParse(jyear.Substring(2), out year)) return 0;

            int diff = 0;
            foreach (var item in EraList)
            {
                if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
                {
                    diff = item.Difference;
                    break;
                }
            }
            if (diff == 0)
            {
                return 0;
            }

            year += diff;
            return year;
        }

        /// <summary>
        /// 昭和=3とした和暦番号と和暦年から、西暦年を取得します。失敗した場合0が返ります。
        /// </summary>
        /// <param name="jyear">JJyy</param>
        /// <returns></returns>
        public static int GetAdYear(int eraNumber, int jYear)
        {
            int diff = 0;
            foreach (var item in EraList)
            {
                if (item.Number == eraNumber)
                {
                    diff = item.Difference;
                    break;
                }
            }
            return diff == 0 ? 0 : jYear += diff;
        }

        /// <summary>
        /// 昭和=3とした年号つきの和暦年月から、西暦年月を取得します。失敗した場合0が返ります。
        /// </summary>
        /// <param name="jyymm"></param>
        /// <returns></returns>
        public static int GetAdYearMonthFromJyymm(int jyymm)
        {
            //20190724140645 furukawa st ////////////////////////
            //令和(5)対応
            
            if (jyymm <= 10000 || 60000 <= jyymm) return 0;
            //if (jyymm <= 10000 || 50000 <= jyymm) return 0;
            //20190724140645 furukawa ed ////////////////////////


            int era = jyymm / 10000;
            int yymm = jyymm % 10000;

            int diff = 0;
            foreach (var item in EraList)
            {
                if (item.Number == era)
                {
                    diff = item.Difference;
                    break;
                }
            }

            if (diff == 0) return 0;
            return yymm += diff * 100;
        }




        /// <summary>
        /// 昭和=3をはじめとする7ケタの数字文字列から日付を取得します
        /// 失敗した場合、DateTime.MinValueが返ります
        /// </summary>
        /// <param name="jdateInt7"></param>
        /// <returns></returns>
        public static DateTime GetDateFromJstr7(string jdate7)
        {
            int jdateInt7;
            if (!int.TryParse(jdate7, out jdateInt7)) return DateTime.MinValue;
            return GetDateFromJInt7(jdateInt7);
        }


        /// <summary>
        /// 昭和=3をはじめとする7ケタの数字から日付を取得します
        /// 失敗した場合、DateTime.MinValueが返ります
        /// </summary>
        /// <param name="jdateInt7"></param>
        /// <returns></returns>
        public static DateTime GetDateFromJInt7(int jdateInt7)
        {
            var eraNum = jdateInt7 / 1000000;
            var era = EraList.FirstOrDefault(e => e.Number == eraNum);
            if (era == null) return DateTime.MinValue;

            var y = jdateInt7 / 10000 % 100;
            y = y + era.Difference;
            var m = jdateInt7 / 100 % 100;
            var d = jdateInt7 % 100;

            return IsDate(y, m, d) ? new DateTime(y, m, d) : DateTime.MinValue;
        }

       



        /// <summary>
        /// 正しい日付かどうかチェックします。
        /// </summary>
        public static bool IsDate(int iYear, int iMonth, int iDay)
        {
            if ((DateTime.MinValue.Year > iYear) || (iYear > DateTime.MaxValue.Year))
            {
                return false;
            }

            if ((DateTime.MinValue.Month > iMonth) || (iMonth > DateTime.MaxValue.Month))
            {
                return false;
            }

            int iLastDay = DateTime.DaysInMonth(iYear, iMonth);

            if ((DateTime.MinValue.Day > iDay) || (iDay > iLastDay))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// 和暦正式文字で表示される日付文字列を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetJpDateStr(DateTime dt)
        {
            var year = GetEraJpYear(dt);
            if (year == string.Empty) return string.Empty;
            return year + "年" + dt.Month.ToString("00") + "月" + dt.Day.ToString("00") + "日";
        }

        /// <summary>
        /// 和暦頭文字で表示される日付文字列を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetShortJpDateStr(DateTime dt)
        {
            var year = GetShortEraJpYear(dt);
            if (year == string.Empty) return string.Empty;
            return year + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
        }

        /// <summary>
        /// 昭和=3としたJYYMMDD形式のInt型で日付を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int GetIntJpDateWithEraNumber(DateTime dt)
        {
            var year = GetEraNumberYear(dt);
            if (year == 0) return 0;
            return year * 10000 + dt.Month * 100 + dt.Day;
        }


        /// <summary>
        /// 数字8ケタからなる西暦をDateTimeに変換します。
        /// </summary>
        /// <param name="dateInt8">8桁のint</param>
        /// <returns></returns>
        public static DateTime ToDateTime(this int dateInt8)
        {
            int year = dateInt8 / 10000;
            int month = (dateInt8 % 10000) / 100;
            int day = dateInt8 % 100;
            if (!IsDate(year, month, day)) return DateTimeNull;
            return new DateTime(year, month, day);
        }

        /// <summary>
        /// 数字6ケタからなる西暦(yyyymm)をDateTimeに変換します。
        /// </summary>
        /// <param name="dateInt8">8桁のint</param>
        /// <returns></returns>
        public static DateTime ToDateTime6(this int dateInt6)
        {
            int year = dateInt6 / 100;
            int month = dateInt6 % 100;
            int day = 1;
            if (!IsDate(year, month, day)) return DateTimeNull;
            return new DateTime(year, month, day);
        }

        /// <summary>
        /// 数字8文字からなる西暦をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
        /// </summary>
        /// <param name="dateInt8"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string dateStr)
        {
            int dateInt8;
            if (dateStr == null) return DateTimeNull;
            if (!int.TryParse(dateStr, out dateInt8)) return DateTimeNull;

            return ToDateTime(dateInt8);
        }

        /// <summary>
        /// 数字6文字からなる西暦(yyyymm)をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
        /// </summary>
        /// <param name="dateInt8"></param>
        /// <returns></returns>
        public static DateTime ToDateTime6(this string dateStr6)
        {
            int dateInt6;
            if (dateStr6 == null) return DateTimeNull;
            if (!int.TryParse(dateStr6, out dateInt6)) return DateTimeNull;

            return ToDateTime6(dateInt6);
        }

        /// <summary>
        /// 数字8ケタのIntに変換します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static int ToInt(this DateTime dt)
        {
            int di = dt.Day;
            di = di + dt.Month * 100;
            di = di + dt.Year * 10000;
            return di;
        }

        /// <summary>
        /// 現在挿入されている日付がNULLかどうかを返します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsNullDate(this DateTime dt)
        {
            return dt == DateTimeNull;
        }

        public static string ToJDateShortStr(this DateTime dt)
        {
            return GetShortJpDateStr(dt);
        }

        public static string ToJDateStr(this DateTime dt)
        {
            return GetJpDateStr(dt);
        }

        /// <summary>
        /// 和暦頭文字で表示される年月文字列を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJyearMonthShortStr(this DateTime dt)
        {
            var year = GetShortEraJpYear(dt);
            if (year == string.Empty) return string.Empty;
            return year + "/" + dt.Month.ToString("00");
        }

        /// <summary>
        /// 和暦頭文字で表示される年の文字列を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJyearShortStr(this DateTime dt)
        {
            var year = GetShortEraJpYear(dt);
            if (year == string.Empty) return string.Empty;
            return year;
        }

        /// <summary>
        /// 和暦で表示される年月文字列を取得します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJyearMonthStr(this DateTime dt)
        {
            var year = GetEraJpYear(dt);
            if (year == string.Empty) return string.Empty;
            return year + "年" + dt.Month.ToString("00") + "月";
        }

        /// <summary>
        /// 指定された月の1日の日付を返します。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime GetMonthFirstDate(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        /// <summary>
        /// 半角にコンバートします。
        /// </summary>
        private static string KanaToHalf(string str)
        {
            return Strings.StrConv(str, VbStrConv.Narrow);
        }


        //20190724143107 furukawa st ////////////////////////
        //令和対応
        
        /// <summary>
        /// 和暦年から西暦年を取得します
        /// 失敗した場合、0が返ります
        /// <paramref name="eemm"/>和暦YYMM
        /// </summary>
        public static int GetAdYearFromWareki(int eemm)
        {
            //20190725105956 furukawa st ////////////////////////
            //初期化            
            int y = 0;// eemm / 100;
            //20190725105956 furukawa ed ////////////////////////


            CngWareki(eemm, out y);
            return y;
        }
        //20190724143107 furukawa ed ////////////////////////



        //20190724143011 furukawa st ////////////////////////
        //平成のみ対応なので削除

        //public static int GetAdYearFromHs(int hensei)
        //{
        //    if (hensei < 1 || 70 < hensei) return 0;
        //    var h = EraList.First(e => e.Initial == "H");
        //    return hensei += h.Difference;
        //}
        //20190724143011 furukawa ed ////////////////////////


        //20190724143150 furukawa st ////////////////////////
        //令和対応
        
        /// <summary>
        /// 令和対応
        /// </summary>
        /// <param name="geemm">和暦年月</param>
        /// <returns>該当和暦情報</returns>
        private static void CngWareki(int geemm, out int cng_y)        
        {
            string strgeemm = geemm.ToString().PadLeft(4, '0');

            int y = int.Parse(strgeemm.Substring(0, 2));
            int m = int.Parse(strgeemm.Substring(2, 2));
            cng_y = y;
            EraJ clsEraJ = new EraJ();


            //20190725105039 furukawa st ////////////////////////
            //平成年の範囲を２０～３１とする
                        
            if (y >= 20 && y < 31)
                    //if (y >= 10 && y < 31)//年が10～30の間は平成
            //20190725105039 furukawa ed ////////////////////////
            {
                clsEraJ = EraList.First(e => e.Initial == "H");
                cng_y = y + clsEraJ.Difference;
                return;
            }

            //年=31、月が4以下は平成
            if (y == 31 && m <= 4)
            {
                clsEraJ = EraList.First(e => e.Initial == "H");
                cng_y = y + clsEraJ.Difference;
                return;
            }

            //年=31、月が５以上は令和
            if (y == 31 && m >= 5)
            {
                clsEraJ = EraList.First(e => e.Initial == "H");
                y -= (clsEraJ.max - 1);
                EraJ diff = EraList.First(e => e.Initial == "R");
                cng_y = y + diff.Difference;
                return;
            }

            //年=1、月が4以下は平成
            if (y == 1 && m <= 4)
            {
                clsEraJ = EraList.First(e => e.Initial == "H");
                y += (clsEraJ.max - 1);
                EraJ diff = EraList.First(e => e.Initial == "H");
                cng_y = y + clsEraJ.Difference;
                return;
            }

            //年=1、月が5以上は令和
            if (y == 1 && m >= 5)
            {
                clsEraJ = EraList.First(e => e.Initial == "R");
                cng_y = y + clsEraJ.Difference;
                return;
            }

            //年>=32以上は令和
            if (y >= 32)
            {
                //20190725182945 furukawa st ////////////////////////
                //32以上はないが、入力するとしたら平成年としてなので、平成の西暦差を足す

                //clsEraJ = EraList.First(e => e.Initial == "R");
                clsEraJ = EraList.First(e => e.Initial == "H");
                //20190725182945 furukawa ed ////////////////////////

                cng_y = y + clsEraJ.Difference;
                return;
            }

            //年が2以上、月が1以上は令和
            if (y > 1 && m >= 1)
            {
                clsEraJ = EraList.First(e => e.Initial == "R");
                cng_y = y + clsEraJ.Difference;
                return;
            }

        }
        //20190724143150 furukawa ed ////////////////////////


        //20190724143423 furukawa st ////////////////////////
        //平成のみ対応なので削除

        /// <summary>
        /// 西暦年から平成年を取得します
        /// </summary>
        //public static int GetHsYearFromAd(int adYear)
        //{
        //    var h = EraList.First(e => e.Initial == "H");
        //    return adYear -= h.Difference;
        //}
        //20190724143423 furukawa ed ////////////////////////


        /// <summary>
        /// 西暦年から和暦年を取得します
        /// <paramref name="adYear"/>年
        /// <param name="mm">月</param>
        /// </summary>
        public static int GetHsYearFromAd(int adYear, int mm)
        {
            return cngToWareki(adYear, mm);
        }

        private static int cngToWareki(int adYear, int mm)
        {

            EraJ era = new EraJ();

            if (adYear == 2019)
            {
                if (mm < 5) era = EraList.First(e => e.Initial == "H");
                if (mm >= 5) era = EraList.First(e => e.Initial == "R");
            }

            //20190725133239 furukawa st ////////////////////////
            //2019年以外は通常判定
            
            //else if (adYear < 2019)
            //{
            //    era = EraList.Last(e => e.Difference < adYear);

            //}
            else
            {
                era = EraList.Last(e => e.Difference < adYear);
            }
            //20190725133239 furukawa ed ////////////////////////

            return adYear -= era.Difference;


        }



        /// <summary>
        /// 西暦年月から年号月和暦年月を取得します
        /// </summary>
        public static int GetGyymmFromAdYM(int adYearMonth)
        {
            var y = adYearMonth / 100;
            var m = adYearMonth % 100;
            var dt = new DateTime(y, m, 1);
            var g = GetEraNumber(dt);
            var jy = GetJpYear(dt);

            return g * 10000 + jy * 100 + m;
        }

        /// <summary>
        /// 西暦年月から平成年を取得します
        /// </summary>
        /// <param name="yyyyMM"></param>
        /// <returns></returns>
        public static int GetHYYFromYYYYMM(int yyyyMM)
        {
            if (yyyyMM < 100000) return -1;
            var yyyy = yyyyMM / 100;

            //20190724143752 furukawa st ////////////////////////
            //引数変更により月も入れる
            
            var mm = yyyyMM % 100;
            return GetHsYearFromAd(yyyy,mm);

            //return GetHsYearFromAd(yyyy);

            //20190724143752 furukawa ed ////////////////////////
        }

        /// <summary>
        /// 西暦年月から月を取得します
        /// </summary>
        /// <param name="yyyyMM"></param>
        /// <returns></returns>
        public static int GetMMFromYYYYMM(int yyyyMM)
        {
            if (yyyyMM < 100000) return -1;
            return yyyyMM - ((yyyyMM / 100) * 100);
        }





        /// <summary>
        /// 平成年と月から西暦年月を取得します
        /// </summary>
        /// <param name="jy"></param>
        /// <param name="jM"></param>
        /// <returns></returns>
        public static int GetYYYYMMFromHYYandMM(int jy, int jM)
        {
            return (GetAdYearFromWareki(jy* 100 + jM) * 100) + jM;
            //return (GetAdYearFromHs(jy) * 100) + jM;
        }


        #region 2019/07/24現在未使用

        ///// <summary>
        ///// 西暦年月から平成年月を取得します
        ///// </summary>
        ///// <param name="yyyyMM"></param>
        ///// <returns></returns>
        //public static int GetHYYMMFromYYYYMM(int yyyyMM)
        //{
        //    if (yyyyMM < 100000) return -1;
        //    return (GetHYYFromYYYYMM(yyyyMM) * 100) + GetMMFromYYYYMM(yyyyMM);
        //}



        ///// <summary>
        ///// 西暦年4ケタの文字列を和暦年に変換します。
        ///// </summary>
        ///// <param name="yearStr4">西暦年四桁文字列</param>
        ///// <returns></returns>
        //public static string GetJpYearStr(string yearStr4)
        //{
        //    int year;
        //    int.TryParse(yearStr4, out year);
        //    if (year == 0) return string.Empty;
        //    var dt = new DateTime(year, 1, 1);
        //    for (int i = 0; i < EraList.Count; i++)
        //    {
        //        if (EraList[i].SDate > dt) continue;
        //        if (EraList[i].EDate < dt) continue;
        //        return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        //    }
        //    return string.Empty;
        //}

        ///// <summary>
        ///// 西暦年4ケタの文字列を和暦年に変換します。
        ///// </summary>
        ///// <param name="yearStr4">西暦年四桁数値</param>
        ///// <returns></returns>
        //public static string GetJpYearStr(int year)
        //{
        //    if (year == 0) return string.Empty;
        //    var dt = new DateTime(year, 1, 1);
        //    for (int i = 0; i < EraList.Count; i++)
        //    {
        //        if (EraList[i].SDate > dt) continue;
        //        if (EraList[i].EDate < dt) continue;
        //        return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        //    }
        //    return string.Empty;
        //}

        ///// <summary>
        ///// 年号つきの和暦年から、西暦年を取得します。
        ///// </summary>
        ///// <param name="jyear">JJyy</param>
        ///// <returns></returns>
        //public static bool TryGetYear(string jyear, out int year)
        //{
        //    year = 0;
        //    if (jyear.Length < 3) return false;
        //    string era = jyear.Remove(2);
        //    if (!int.TryParse(jyear.Substring(2), out year)) return false;

        //    int diff = 0;
        //    foreach (var item in EraList)
        //    {
        //        if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
        //        {
        //            diff = item.Difference;
        //            break;
        //        }
        //    }
        //    if (diff == 0)
        //    {
        //        year = 0;
        //        return false;
        //    }

        //    year += diff;
        //    return true;
        //}

        ///// <summary>
        ///// 文字列を日付に変換します。
        ///// </summary>
        ///// <param name="jDate"></param>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public static bool TryGetYearMonthTimeFromJdate(string jDate, out DateTime dt)
        //{
        //    dt = DateTimeNull;

        //    //アンダーラインはスペースに
        //    jDate = jDate.Replace('_', ' ');

        //    //年月日区切りをスラッシュに
        //    StringBuilder sb = new StringBuilder();
        //    const string separator = "年月・.";
        //    foreach (var c in jDate)
        //    {
        //        if (separator.Contains(c)) sb.Append('/');
        //        else sb.Append(c);
        //    }
        //    jDate = sb.ToString();
        //    sb.Clear();

        //    //半角に置き換え
        //    jDate = KanaToHalf(jDate);

        //    var dateStr = jDate.Split('/');

        //    //区切りなしの場合は調整
        //    if (dateStr.Length == 1)
        //    {
        //        dateStr = new string[2];
        //        if (jDate.Length == 6)
        //        {
        //            //西暦として扱う
        //            dateStr[0] = jDate.Remove(2);
        //            dateStr[1] = jDate.Substring(2, 2);
        //        }
        //        else if (jDate.Length == 7)
        //        {
        //            //和暦として扱う
        //            dateStr[0] = jDate.Remove(3);
        //            dateStr[1] = jDate.Substring(3, 2);
        //        }
        //        else if (jDate.Length == 8)
        //        {
        //            //西暦として扱う
        //            dateStr[0] = jDate.Remove(4);
        //            dateStr[1] = jDate.Substring(4, 2);
        //        }
        //        else return false;
        //    }

        //    if (dateStr.Length < 2) return false;

        //    //年の数字だけ取出し
        //    const string numStr = "0123456789";
        //    foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        //    string yearNumStr = sb.ToString();

        //    //数字部分が3ケタ時、1文字目を年号に変換
        //    if (yearNumStr.Length == 3)
        //    {
        //        int eraNum;
        //        if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
        //        foreach (var item in EraList)
        //        {
        //            if (item.Number == eraNum)
        //            {
        //                dateStr[0] = item.Initial + yearNumStr.Substring(1);
        //                yearNumStr = yearNumStr.Substring(1);
        //                break;
        //            }
        //        }
        //    }

        //    int year, month;
        //    if (!int.TryParse(yearNumStr, out year)) return false;
        //    if (!int.TryParse(dateStr[1], out month)) return false;

        //    foreach (var item in EraList)
        //    {
        //        if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
        //        {
        //            year += item.Difference;
        //            break;
        //        }
        //    }

        //    if (!IsDate(year, month, 1)) return false;
        //    dt = new DateTime(year, month, 1);
        //    return true;
        //}



        ///// <summary>
        ///// 文字列を日付に変換します。
        ///// </summary>
        ///// <param name="jDate"></param>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public static bool TryGetDateTimeFromJdate(string jDate, out DateTime dt)
        //{
        //    dt = DateTimeNull;

        //    //アンダーラインはスペースに
        //    jDate = jDate.Replace('_', ' ');

        //    //年月日区切りをスラッシュに
        //    StringBuilder sb = new StringBuilder();
        //    const string separator = "年月日・.";
        //    foreach (var c in jDate)
        //    {
        //        if (separator.Contains(c)) sb.Append('/');
        //        else sb.Append(c);
        //    }
        //    jDate = sb.ToString();
        //    sb.Clear();

        //    //半角に置き換え
        //    jDate = KanaToHalf(jDate);

        //    var dateStr = jDate.Split('/');

        //    //区切りなしの場合は調整
        //    if (dateStr.Length == 1)
        //    {
        //        dateStr = new string[3];
        //        if (jDate.Length == 6)
        //        {
        //            //西暦として扱う
        //            dateStr[0] = jDate.Remove(2);
        //            dateStr[1] = jDate.Substring(2, 2);
        //            dateStr[2] = jDate.Substring(4, 2);
        //        }
        //        else if (jDate.Length == 7)
        //        {
        //            //和暦として扱う
        //            dateStr[0] = jDate.Remove(3);
        //            dateStr[1] = jDate.Substring(3, 2);
        //            dateStr[2] = jDate.Substring(5, 2);
        //        }
        //        else if (jDate.Length == 8)
        //        {
        //            //西暦として扱う
        //            dateStr[0] = jDate.Remove(4);
        //            dateStr[1] = jDate.Substring(4, 2);
        //            dateStr[2] = jDate.Substring(6, 2);
        //        }
        //        else return false;
        //    }

        //    if (dateStr.Length < 3) return false;

        //    //年の数字だけ取出し
        //    const string numStr = "0123456789";
        //    foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        //    string yearNumStr = sb.ToString();

        //    //数字部分が3ケタ時、1文字目を年号に変換
        //    if (yearNumStr.Length == 3)
        //    {
        //        int eraNum;
        //        if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
        //        foreach (var item in EraList)
        //        {
        //            if (item.Number == eraNum)
        //            {
        //                dateStr[0] = item.Initial + yearNumStr.Substring(1);
        //                yearNumStr = yearNumStr.Substring(1);
        //                break;
        //            }
        //        }
        //    }

        //    int year, month, day;
        //    if (!int.TryParse(yearNumStr, out year)) return false;
        //    if (!int.TryParse(dateStr[1], out month)) return false;
        //    if (!int.TryParse(dateStr[2], out day)) return false;

        //    foreach (var item in EraList)
        //    {
        //        if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
        //        {
        //            year += item.Difference;
        //            break;
        //        }
        //    }

        //    if (!IsDate(year, month, day)) return false;
        //    dt = new DateTime(year, month, day);
        //    return true;
        //}


        ///// <summary>
        ///// コードまたは番号から、年号を返します。なかった場合空白が返ります。
        ///// </summary>
        ///// <param name="str"></param>
        ///// <returns></returns>
        //public static string GetEraName(string codeStr)
        //{
        //    codeStr = codeStr.ToUpper();
        //    int code;
        //    foreach (var item in EraList)
        //    {
        //        if (codeStr.Contains(item.Initial)) return item.Name;
        //        if (int.TryParse(codeStr, out code)) if (code == item.Number) return item.Name;
        //    }
        //    return string.Empty;
        //}

        ///// <summary>
        ///// 4月から始まる、西暦の「年度」を取得します。
        ///// </summary>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public static int GetTheYear(this DateTime dt)
        //{
        //    int year = dt.Year;
        //    if (dt.Month < 4) year--;
        //    return year;
        //}

        ///// <summary>
        ///// 4月から始まる、西暦の「年度」の下2桁を取得します。
        ///// </summary>
        ///// <param name="dt"></param>
        ///// <returns></returns>
        //public static int GetTheYear2(this DateTime dt)
        //{
        //    int year = dt.Year;
        //    if (dt.Month < 4) year--;
        //    year %= 100;
        //    return year;
        //}

        #endregion


    }


}
