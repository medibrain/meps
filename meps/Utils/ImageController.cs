﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.IO;

namespace meps.Utils
{
    class ImageController
    {
        //表示している画像を９０度回転させる（画像ファイルも回転）
        //trueなら時計まわり

        /// <summary>
        /// 表示している画像を９０度回転させる（画像ファイルも回転）
        /// </summary>
        /// <param name="fileName">画像ファイル名</param>
        /// <param name="clockwise">trueなら時計まわり</param>
        public static bool ImageRotate(string fileName, bool clockwise)
        {
            try
            {
                using (var bmp = Image.FromFile(fileName))
                {
                    bmp.RotateFlip(clockwise ? RotateFlipType.Rotate90FlipNone : RotateFlipType.Rotate270FlipNone);

                    var ep = new EncoderParameters(1);
                    ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
                    ImageCodecInfo ici = GetEncoderInfo("image/tiff");
                    bmp.Save(fileName, ici, ep);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("画像の回転に失敗しました\r\n" + ex, "UserControlImage", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        //MimeTypeで指定されたImageCodecInfoを探して返す
        private static ImageCodecInfo GetEncoderInfo(string mineType)
        {
            //GDI+ に組み込まれたイメージ エンコーダに関する情報をすべて取得
            System.Drawing.Imaging.ImageCodecInfo[] encs =
                System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            //指定されたMimeTypeを探して見つかれば返す
            foreach (System.Drawing.Imaging.ImageCodecInfo enc in encs)
            {
                if (enc.MimeType == mineType)
                {
                    return enc;
                }
            }
            return null;
        }

        //ImageFormatで指定されたImageCodecInfoを探して返す
        private static ImageCodecInfo GetEncoderInfo(ImageFormat f)
        {
            System.Drawing.Imaging.ImageCodecInfo[] encs =
                System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            foreach (System.Drawing.Imaging.ImageCodecInfo enc in encs)
            {
                if (enc.FormatID == f.Guid)
                {
                    return enc;
                }
            }
            return null;
        }

        public static bool Save(List<string> imageFileNames, string saveName)
        {
            List<Bitmap> bmps = new List<Bitmap>();

            try
            {
                foreach (var item in imageFileNames)
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        var img = (Bitmap)Image.FromFile(item);
                        if (img == null)
                        {
                            if (i == 2) return false;
                            continue;
                        }
                        bmps.Add(img);
                        break;
                    }
                }

                return Save(bmps, saveName);
            }
            catch
            {
                return false;
            }
            finally
            {
                foreach (var item in bmps) item.Dispose();
            }
        }

        /// <summary>
        /// 画像が複数枚の場合、マルチページTiffに変換
        /// </summary>
        /// <param name="imageFileNames"></param>
        /// <param name="sid"></param>
        /// <returns></returns>
        public static bool Save(List<Bitmap> bmps, string saveName)
        {
            try
            {
                // TIFF 形式のエンコーダ。
                ImageCodecInfo[] imageEncoders = ImageCodecInfo.GetImageEncoders();
                var pre = new Predicate<ImageCodecInfo>(input => input.FormatID == ImageFormat.Tiff.Guid);
                ImageCodecInfo tiffEncoder = Array.Find(imageEncoders, pre);

                // エンコーダのパラメータ。
                EncoderParameters tiffEncoderParameters = new EncoderParameters(2);
                tiffEncoderParameters.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
                tiffEncoderParameters.Param[1] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);

                // 作業領域となるメモリストリーム。
                using (MemoryStream tiffStream = new MemoryStream())
                {
                    // Save メソッドによって、1ページ目が保存され、更に Image オブジェクトとメモリストリームが関連付けられる。
                    bmps[0].Save(tiffStream, tiffEncoder, tiffEncoderParameters);

                    // 2ページ目, 3ページ目の保存に使用するエンコーダのパラメータ。
                    EncoderParameters pageEncoderParameters = new EncoderParameters(2);
                    pageEncoderParameters.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
                    pageEncoderParameters.Param[1] = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionCCITT4);

                    // 先ほどの Save メソッドで関連付けられたメモリストリームに対して保存される。
                    // この操作は page1 にフレームを追加するわけではない。
                    for (int i = 1; i < bmps.Count; i++)
                    {
                        bmps[0].SaveAdd(bmps[i], pageEncoderParameters);
                    }

                    // メモリストリームの内容をファイルに保存する。
                    File.WriteAllBytes(saveName, tiffStream.ToArray());
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 画像が一枚の場合、Tiff圧縮形式のチェックなしで単純ファイルコピー
        /// </summary>
        /// <param name="imageFileNames"></param>
        /// <param name="saveName"></param>
        /// <returns></returns>
        public static bool SaveOneWithoutCheck(List<string> imageFileNames, string saveName)
        {
            try
            {
                System.IO.File.Copy(imageFileNames[0], saveName);
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }
        }

        /// <summary>
        /// 画像が一枚の場合、Tiff圧縮形式をチェックしてコピー
        /// </summary>
        /// <param name="imageFileNames"></param>
        /// <param name="saveName"></param>
        /// <returns></returns>
        public static bool SaveOne(List<string> imageFileNames, string saveName)
        {
            //tiffファイルの圧縮形式のチェック
            using (var img = System.Drawing.Image.FromFile(imageFileNames[0]))
            {
                int encCCITT4 = (int)EncoderValue.CompressionCCITT4;
                int compTagIndex = Array.IndexOf(img.PropertyIdList, 0x103);
                PropertyItem compTag = img.PropertyItems[compTagIndex];

                int encValue = BitConverter.ToInt16(compTag.Value, 0);

                if (encValue == encCCITT4)
                {
                    //Tiffが正しい形式で圧縮されている場合
                    try
                    {
                        System.IO.File.Copy(imageFileNames[0], saveName);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWrite(ex);
                        return false;
                    }
                }
                else
                {
                    //Tiffの圧縮形式が異なる場合
                    if (!Save(imageFileNames, saveName)) return false;
                }
            }

            return true;
        }

        public static bool SaveOne(string imageFileName, string saveName)
        {
            //tiffファイルの圧縮形式のチェック
            using (var img = System.Drawing.Image.FromFile(imageFileName))
            {
                int encCCITT4 = (int)EncoderValue.CompressionCCITT4;
                int compTagIndex = Array.IndexOf(img.PropertyIdList, 0x103);
                PropertyItem compTag = img.PropertyItems[compTagIndex];
                int encValue = BitConverter.ToInt16(compTag.Value, 0);

                if (encValue == encCCITT4)
                {
                    //Tiffが正しい形式で圧縮されている場合
                    try
                    {
                        System.IO.File.Copy(imageFileName, saveName);
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWrite(ex);
                        return false;
                    }
                }
                else
                {
                    List<string> imageFileNames = new List<string> { imageFileName };
                    //Tiffの圧縮形式が異なる場合
                    if (!Save(imageFileNames, saveName)) return false;
                }
            }
            return true;
        }
    }
}
