﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace meps.Utils
{
    public class TiffUtility
    {
        const int StripOffsets = 0x0111;
        const int StripByteCounts = 0x0117;

        public class TiffTag
        {
            public int Code;
            public int TypeNum;
            public int Count;
            public byte[] Data = new byte[4];
            public byte[] ExData;
        }

        public int StripCount = 0;
        public List<TiffTag> Tags = new List<TiffTag>();
        public List<byte[]> ImageData = new List<byte[]>();
        public int ImageDataTotalLen => ImageData.Sum(data => data.Length);
        public byte[] OptionData;


        private static int typeNumGetByteCount(int typeNum)
        {
            if (typeNum == 1) return 1;     //コード１…BYTE型(１バイト整数)
            if (typeNum == 2) return 1;     //コード２…ASCII型(１バイトのASCII文字)
            if (typeNum == 3) return 2;     //コード３…SHORT型(２バイト短整数)
            if (typeNum == 4) return 4;     //コード４…LONG型(４バイト長整数)
            if (typeNum == 5) return 8;     //コード５…RATIONAL型(８バイト分数、４バイトの分子とそれに続く４バイトの分母)
            if (typeNum == 6) return 1;     //コード６…SBYTE型(１バイト符号付き整数)
            if (typeNum == 7) return 1;     //コード７…UNDEFINED型(あらゆる１バイトデータ)
            if (typeNum == 8) return 2;     //コード８…SSHORT型(２バイト符号付き短整数)
            if (typeNum == 9) return 4;     //コード９…SLONG型(４バイト符号付き長整数)
            if (typeNum == 10) return 8;    //コード10…SRATIONAL型(８バイト符号付き分数、４バイトの分子とそれに続く４バイトの分母)
            if (typeNum == 11) return 4;    //コード11…FLOAT型(４バイト実数、IEEE浮動小数点形式)
            if (typeNum == 12) return 8;    //コード12…DOUBLE型(８バイト倍精度実数、IEEE倍精度浮動小数点形式)
            return 0;
        }

        private void getImageDatas(byte[] bs)
        {
            var dataOffset = Tags.First(t => t.Code == StripOffsets);
            var dataLength = Tags.First(t => t.Code == StripByteCounts);

            if (StripCount == 1)
            {
                var data = new byte[BitConverter.ToInt32(dataLength.Data, 0)];
                Array.Copy(bs, BitConverter.ToInt32(dataOffset.Data, 0), data, 0, data.Length);
                ImageData.Add(data);
                return;
            }

            for (int i = 0; i < StripCount; i++)
            {
                var offset = BitConverter.ToUInt32(dataOffset.ExData, i * 4);
                var length = BitConverter.ToUInt32(dataLength.ExData, i * 4);
                var data = new byte[length];
                Array.Copy(bs, offset, data, 0, length);
                ImageData.Add(data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        private static TiffUtility getIFDs(byte[] bs)
        {
            var intelMode = bs[0] == 0x49;
            int readIndex = BitConverter.ToInt32(bs, 4);

            for (int e = 0; e < 1000; e++)
            {
                int tagCount = BitConverter.ToInt16(bs, readIndex);
                readIndex += 2;

                var ifd = new TiffUtility();

                for (int i = 0; i < tagCount; i++)
                {
                    var t = new TiffTag();
                    t.Code = BitConverter.ToInt16(bs, readIndex + i * 12);
                    t.TypeNum = BitConverter.ToInt16(bs, readIndex + i * 12 + 2);
                    t.Count = BitConverter.ToInt16(bs, readIndex + i * 12 + 4);
                    Array.Copy(bs, readIndex + i * 12 + 8, t.Data, 0, 4);

                    //ポインタ処理
                    var length = typeNumGetByteCount(t.TypeNum) * t.Count;
                    if (length > 4)
                    {
                        t.ExData = new byte[length];
                        Array.Copy(bs, BitConverter.ToInt32(t.Data, 0), t.ExData, 0, length);
                        t.Data = new byte[4];
                    }

                    ifd.Tags.Add(t);
                    if (t.Code == StripOffsets) ifd.StripCount = t.Count;
                }

                readIndex = BitConverter.ToInt32(bs, readIndex + tagCount * 12);
                ifd.getImageDatas(bs);

                if (readIndex == 0) return ifd;
            }
            throw new Exception("IFD count is over 1000. Maybe file broken.");
        }

        private void setDataPointers(List<int> list)
        {
            var tag = Tags.First(t => t.Code == StripOffsets);
            if (list.Count == 1)
            {
                tag.Data = BitConverter.GetBytes(list[0]);
            }
            else
            {
                tag.ExData = new byte[list.Count * 4];
                for (int i = 0; i < list.Count; i++)
                    Array.Copy(BitConverter.GetBytes(list[i]), 0, tag.ExData, i * 4, 4);

                tag.Data = new byte[4];
            }
        }

        private byte[] createNewIFD(int startAdd)
        {
            var bl = new List<byte>();
            var optionBl = new List<byte>();
            startAdd = startAdd + Tags.Count * 12 + 6;

            //IFD数を記録
            bl.AddRange(BitConverter.GetBytes((short)Tags.Count));

            var act = new Action<TiffTag>(t =>
            {
                bl.AddRange(BitConverter.GetBytes((short)t.Code));
                bl.AddRange(BitConverter.GetBytes((short)t.TypeNum));
                bl.AddRange(BitConverter.GetBytes(t.Count));

                var len = t.Count * typeNumGetByteCount(t.TypeNum);
                if (len > 4)
                {
                    bl.AddRange(BitConverter.GetBytes(startAdd + optionBl.Count));
                    optionBl.AddRange(t.ExData);
                }
                else
                {
                    bl.AddRange(t.Data);
                }
            });

            foreach (var item in Tags) act(item);

            OptionData = optionBl.ToArray();
            return bl.ToArray();
        }

        /// <summary>
        /// 複数枚のtiffであればマルチページ化し、1枚であればコピーします
        /// </summary>
        /// <param name="fc"></param>
        /// <param name="fileNames"></param>
        /// <param name="saveFileName"></param>
        public static bool MargeOrCopyTiff(FastCopy fc, IEnumerable<string> fileNames, string saveFileName)
        {
            if (fileNames.Count() == 0)
            {
                return fc.FileCopy(fileNames.First(), saveFileName);
            }
            else
            {
                return MargeTiff(fileNames, saveFileName);
            }
        }

        public static bool MargeTiff(IEnumerable<string> fileNames, string saveFileName)
        {
            var ifds = new List<TiffUtility>();
            byte[] bs;

            try
            {
                foreach (var item in fileNames)
                {
                    using (var fs = new FileStream(item, FileMode.Open))
                    {
                        bs = new byte[fs.Length];
                        fs.Read(bs, 0, (int)fs.Length);
                        ifds.Add(TiffUtility.getIFDs(bs));
                    }
                }

                //新ファイル用byte配列
                var newFile = new List<byte>();
                var dataPointers = new List<int>();

                //ヘッダ
                newFile.AddRange(new byte[] { 0x49, 0x49, 0x2A, 0x00 });

                for (int i = 0; i < ifds.Count; i++)
                {
                    //IFD情報ポインタ記録
                    int ifdAdd = newFile.Count + ifds[i].ImageDataTotalLen + 4;
                    if (i != 0) ifdAdd += ifds[i - 1].OptionData.Length;
                    newFile.AddRange(BitConverter.GetBytes(ifdAdd));

                    //前IFDに関係するオプション情報記録
                    if (i != 0) newFile.AddRange(ifds[i - 1].OptionData);

                    //画像データの記録
                    foreach (var item in ifds[i].ImageData)
                    {
                        dataPointers.Add(newFile.Count);
                        newFile.AddRange(item);
                    }

                    //記録用IFD情報作成＆記録
                    ifds[i].setDataPointers(dataPointers);
                    var ifd = ifds[i].createNewIFD(newFile.Count);
                    newFile.AddRange(ifd);

                    dataPointers.Clear();
                }

                //終了コード
                newFile.AddRange(new byte[] { 0x00, 0x00, 0x00, 0x00 });
                newFile.AddRange(ifds[ifds.Count - 1].OptionData);

                using (var fs = new FileStream(saveFileName, FileMode.Create))
                {
                    fs.Write(newFile.ToArray(), 0, newFile.Count);
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public class FastCopy
        {
            //1メガバイト確保
            byte[] bs = new byte[1000000];
            int len = 0;

            public bool FileCopy(string sourceFileName, string destFileName)
            {
                try
                {
                    using (var rs = new FileStream(sourceFileName, FileMode.Open))
                    {
                        len = (int)rs.Length;
                        if (len > 1000000) throw new Exception("buffer over");
                        rs.Read(bs, 0, len);
                    }

                    using (var ws = new FileStream(destFileName, FileMode.Create))
                    {
                        ws.Write(bs, 0, len);
                    }
                }
                catch
                {
                    return false;
                }
                return true;
            }

            public bool DirCopy(string copyDir, string sendDir)
            {
                var fs = Directory.GetFiles(copyDir);
                int len = 0;

                try
                {
                    foreach (var item in fs)
                    {
                        using (var rs = new FileStream(item, FileMode.Open))
                        {
                            len = (int)rs.Length;
                            if (len > 1000000) throw new Exception("buffer over");
                            rs.Read(bs, 0, len);
                        }

                        var fileName = sendDir + "\\" + Path.GetFileName(item);
                        using (var ws = new FileStream(fileName, FileMode.Create))
                        {
                            ws.Write(bs, 0, len);
                        }
                    }
                }
                catch
                {
                    return false;
                }
                return true;
            }
        }
    }
}
