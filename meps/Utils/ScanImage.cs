﻿using meps.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Utils
{
    class ScanImage
    {
        /// <summary>
        /// エラーメッセージ
        /// </summary>
        public string[] ErrorMessages => ems.ToArray();
        private List<string> ems;

        public ScanImage()
        {
            ems = new List<string>();
        }

        /// <summary>
        /// 単一フォルダ内の画像ファイルを取り込みます。
        /// 取込中に失敗した場合はリセットし、取り込む前の状態に戻します。
        /// false：ScanImage#ErrorMessages参照
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="iym"></param>
        /// <param name="importedOneImageCallback"></param>
        /// <returns></returns>
        public bool DoImagesImport(
            string folderPath, int iym, Action<string> importedOneImageCallback)
        {
            resetErrorMessage();
            return doImportByFolder(folderPath, iym, importedOneImageCallback);
        }

        /// <summary>
        /// 複数のフォルダを同時に取り込みます。
        /// 取込中に失敗した場合はそのフォルダに関係する情報をすべてリセットし、取り込む前の状態に戻します。そして次のフォルダが実行されます。
        /// false：ScanImage#ErrorMessages参照
        /// </summary>
        /// <param name="subFoldersPath"></param>
        /// <param name="iym"></param>
        /// <param name="importedOneImageCallback"></param>
        /// <returns></returns>
        public bool DoImagesImportInFolders(
            string[] subFoldersPath, int iym, Action<string> importedOneImageCallback)
        {
            resetErrorMessage();
            var importResult = true;
            bool folderResult;
            foreach (var folderPath in subFoldersPath)
            {
                folderResult = doImportByFolder(folderPath, iym, importedOneImageCallback);
                if (!folderResult)
                {
                    addErrorMessage($"取り込みに失敗しました（{folderPath}）");
                }
                importResult &= folderResult;
            }
            return importResult;
        }

        private bool doImportByFolder(string folderPath, int iym, Action<string> importedOneImageCallback)
        {
            //tifファイルのみ取得
            var fl = FileUtility.GetFilesInFolder(folderPath).Where(s =>
            {
                var ex = FileUtility.GetExtension(s);
                return ex == ".tiff" || ex == ".tif";
            }).OrderBy(s => s);//名前順

            //ファイル数をカウント
            var fcount = fl.Count();

            //指定されたフォルダにtifファイルが存在するかチェック
            if (fcount != 0)
            {
                //フォルダ名の取得
                var folderName = FileUtility.GetDirectoryName(folderPath);

                //都道府県番号取得
                var pid = getPID(folderName);
                if (pid == -1)
                {
                    addErrorMessage($"フォルダ名から都道府県を特定することができませんでした（{folderName}）");
                    return false;
                }

                //重複していないか確認
                var targetFolder = GetImageFolderPathToIYMPID(iym, pid);
                if (FileUtility.FolderExist(targetFolder))
                {
                    addErrorMessage($"このフォルダは既に取り込みが完了しています（{folderName}）");
                    return false;
                }

                //取込開始
                var tran = new DB.Transaction();
                int insertMAID;
                MediApp ma;
                MediAppInfo mai;
                PrefInfo pi;
                bool imaResult;
                bool imaiResult;
                bool ipiResult;
                var count = 1;
                foreach(var path in fl)
                {
                    //ファイル名作成(yyyyMMPPNNNNNN.tif)
                    var newFileName = $"{iym}{pid.ToString("00")}{count.ToString("000000")}.tif";

                    //表示用に画像を指定個所にコピー
                    var newPath = $"{targetFolder}\\{newFileName}";
                    var cResult = FileUtility.FileCopy(path, newPath);
                    if (!cResult)
                    {
                        addErrorMessage($"画像をコピーできませんでした（{path}）");
                        FileUtility.FolderDelete(targetFolder);//失敗時はフォルダごと中身を削除
                        tran.Rollback();
                        return false;
                    }

                    //MediAppにインサート
                    try
                    {
                        ma = new MediApp();
                        ma.IYM = iym;
                        ma.PID = pid;
                        ma.FileName = newFileName;
                        imaResult = ma.PreInsert(tran, out insertMAID);
                        if (!imaResult)
                        {
                            throw new Exception("mediapp insert failed.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWrite(ex);
                        addErrorMessage($"画像に対応するデータを作成できませんでした（{path}）");
                        FileUtility.FolderDelete(targetFolder);//失敗時はフォルダごと中身を削除
                        tran.Rollback();
                        return false;
                    }

                    //MediAppInfoにもインサート
                    try
                    {
                        mai = new MediAppInfo();
                        mai.MAID = insertMAID;
                        mai.IYM = iym;
                        mai.PID = pid;
                        imaiResult = mai.Insert(tran);
                        if (!imaiResult)
                        {
                            throw new Exception("mediappinfo insert failed.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.ErrorWrite(ex);
                        addErrorMessage($"画像に対応する計算用データを作成できませんでした（{path}）");
                        FileUtility.FolderDelete(targetFolder);//失敗時はフォルダごと中身を削除
                        tran.Rollback();
                        return false;
                    }

                    //コールバック
                    importedOneImageCallback(path);

                    count++;
                }

                //最後にPrefInfoにもインサート
                try
                {
                    pi = new PrefInfo();
                    pi.IYM = iym;
                    pi.PID = pid;
                    pi.ImportDatetime = DateTime.Now;
                    pi.ImportUser = User.CurrentUser.UserID;
                    ipiResult = pi.Insert(tran);
                    if (!ipiResult)
                    {
                        throw new Exception("prefinfo insert failed.");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    addErrorMessage($"都道府県に対応するデータを作成できませんでした（{folderName}）");
                    FileUtility.FolderDelete(targetFolder);//失敗時はフォルダごと中身を削除
                    tran.Rollback();
                    return false;
                }

                tran.Commit();
            }

            return true;
        }

        private int getPID(string folderName)
        {
            if (folderName == null || folderName == "") return -1;

            //番号　の場合
            int num;
            if(int.TryParse(folderName, out num))
            {
                var prefFromNumber = Pref.GetPref(num);
                if (prefFromNumber != null)
                {
                    return prefFromNumber.PID;
                }
            }

            //都道府県名　の場合
            var prefFromName = Pref.GetPrefByName(folderName);
            if (prefFromName != null)
            {
                return prefFromName.PID;
            }

            //番号＋都道府県名（半角or全角スペース区切り）　の場合
            var items = folderName.Split(new char[] { ' ', '　' }, StringSplitOptions.RemoveEmptyEntries);
            if (items.Count() == 2)
            {
                //番号で...
                int numFromSplit1;
                if (!int.TryParse(items[0], out numFromSplit1)) return -1;
                var prefFromSplit1 = Pref.GetPref(numFromSplit1);
                if (prefFromSplit1 == null) return -1;

                //都道府県名で...
                var prefFromSplit2 = Pref.GetPrefByName(items[1]);
                if (prefFromSplit2 == null) return -1;

                //番号と都道府県名が一致しているか確認する
                if (prefFromSplit1.PID == prefFromSplit2.PID)
                {
                    return prefFromSplit1.PID;
                }
                else
                {
                    addErrorMessage($"都道府県番号と名前が一致していません（{folderName}）");
                }
            }

            return -1;
        }

        /// <summary>
        /// 画像フォルダパスの審査年月指定直前までのパス返します。
        /// </summary>
        /// <returns></returns>
        public static string GetImageFolderPath()
        {
            return $"{Settings.ImageHost}\\scan\\{DB.GetDBName()}";
        }

        /// <summary>
        /// 審査年月と都道府県番号から画像フォルダパスを取得します。
        /// </summary>
        /// <param name="iym"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static string GetImageFolderPathToIYMPID(int iym, int pid)
        {
            return $"{GetImageFolderPath()}\\{iym}\\{pid}";
        }

        private void resetErrorMessage()
        {
            ems.Clear();
        }

        private void addErrorMessage(string em)
        {
            ems.Add(em);
        }
    }
}
