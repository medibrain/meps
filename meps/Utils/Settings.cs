﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace meps
{
    class Settings
    {
        //リスト作成画面のベースとなるExcelフォルダパスはSettings.xmlに持たせた
        static string fileName = System.Windows.Forms.Application.StartupPath + "\\settings.xml";
        static XDocument xdoc;

        static Settings()
        {
            if (System.IO.File.Exists(fileName))
            {
                xdoc = XDocument.Load(fileName);
            }
            else
            {
                xdoc = new XDocument();
                xdoc.Add(new XElement("Settings"));
                xdoc.Save(fileName);
            }
        }


        private static void setValue(string name, string value)
        {
            xdoc.Element("Settings").SetElementValue(name, value);
            xdoc.Save(fileName);
        }

        private static string getValue(string name)
        {
            if (xdoc.Element("Settings").Element(name) == null) return string.Empty;
            return xdoc.Element("Settings").Element(name).Value;
        }

        /// <summary>
        /// 共通設定プリンタ
        /// </summary>
        public static string DefaultPrinterName
        {
            get { return getValue("DefaultPrinterName"); }
            set { setValue("DefaultPrinterName", value); }
        }

        /// <summary>
        /// 点検画面での使用プリンタ名
        /// </summary>
        public static string InspectPrinterName
        {
            get { return getValue("InspectPrinterName"); }
            set { setValue("InspectPrinterName", value); }
        }

        /// <summary>
        /// 照会画面での使用プリンタ名
        /// </summary>
        public static string ShokaiPrinterName
        {
            get { return getValue("ShokaiPrinterName"); }
            set { setValue("ShokaiPrinterName", value); }
        }

        /// <summary>
        /// データベースのホストアドレス
        /// </summary>
        public static string DataBaseHost
        {
            get { return getValue("DataBaseHost"); }
            set { setValue("DataBaseHost", value); }
        }

        /// <summary>
        /// データベースのタイムアウト秒
        /// </summary>
        public static string CommandTimeout
        {
            get { return getValue("CommandTimeout"); }
            set { setValue("CommandTimeout", value); }
        }

        /// <summary>
        /// 印刷時マージン
        /// </summary>
        public static int PrintMargin
        {
            get { return int.Parse(getValue("PrintMargin")); }
            set { setValue("PrintMargin", value.ToString()); }
        }

        /// <summary>
        /// 1グループに割り当てられる画像枚数
        /// </summary>
        public static int GroupCount
        {
            get
            {
                int gc = 0;
                int.TryParse(getValue("GroupCount"), out gc);
                return gc;
            }

            set { setValue("GroupCount", value.ToString()); }
        }

        /// <summary>
        /// スキャン画像の保存先フォルダ
        /// </summary>
        public static string ImageHost
        {
            get { return getValue("ImageHost"); }
            set { setValue("ImageHost", value); }
        }

        /// <summary>
        /// バージョン管理フォルダ
        /// </summary>
        public static string VersionFolder
        {
            get { return getValue("VersionFolder"); }
            set { setValue("VersionFolder", value); }
        }


        //20191128091135 furukawa st ////////////////////////
        //postgresqlのポート指定もsettingsから取得
        
        /// <summary>
        /// db接続ポート
        /// </summary>
        public static string dbPort
        {
            get { return getValue("port"); }
            set { setValue("port", value); }
        }
        //20191128091135 furukawa ed ////////////////////////


        //20200212141554 furukawa st ////////////////////////
        //リスト作成画面のベースとなるExcelフォルダパスの取得/設定
        
        public static string BaseExcelDir
        {
            get { return getValue("BaseExcelDir"); }
            set { setValue("BaseExcelDir", value); }
        }
        //20200212141554 furukawa ed ////////////////////////

    
    }
}
