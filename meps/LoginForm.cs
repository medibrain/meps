﻿using meps.Table;
using meps.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (textBoxUID.Text == "")
            {
                MessageBox.Show("ユーザーIDを入力してください", "エラー");
                return;
            }
            if (textBoxPass.Text == "")
            {
                MessageBox.Show("パスワードを入力してください", "エラー");
                return;
            }
            var uid = Converter.ToInt(textBoxUID.Text);
            if (uid == 0)
            {
                labelUsername.Text = "";
                MessageBox.Show("ユーザーIDが正しくありません", "エラー");
                textBoxUID.Focus();
                return;
            }
            var name = User.GetUserName(uid);
            if (name == null)
            {
                labelUsername.Text = "";
                MessageBox.Show("ユーザーIDが正しくありません", "エラー");
                textBoxUID.Focus();
                return;
            }
            var user = User.GetUser(uid);
            if (user == null)
            {
                MessageBox.Show("ユーザーが見つかりません", "エラー");
                return;
            }
            if(user.Pass != textBoxPass.Text)
            {
                MessageBox.Show("パスワードが違います", "エラー");
                return;
            }
            User.SetCurrentUser(uid);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
            Close();
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult != DialogResult.OK)
            {
                Environment.Exit(0);
                Close();
            }
        }

        /// <summary>
        /// Enter = Tab
        /// (Form#KeyPreview=true)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        /// <summary>
        /// ユーザー名表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBoxUID_Leave(object sender, EventArgs e)
        {
            if (textBoxUID.Text == "") return;

            var uid = Converter.ToInt(textBoxUID.Text);
            if (uid == 0)
            {
                labelUsername.Text = "";
                MessageBox.Show("ユーザーIDが正しくありません", "エラー");
                textBoxUID.Focus();
                return;
            }

            var name = User.GetUserName(uid);
            if (name == null)
            {
                labelUsername.Text = "";
                MessageBox.Show("ユーザーIDが正しくありません", "エラー");
                textBoxUID.Focus();
                return;
            }

            labelUsername.Text = name;
        }

        private void textBoxPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) buttonOK_Click(null, null);
        }
    }
}
