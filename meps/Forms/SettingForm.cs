﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;
using meps.Table;
using meps.Utils;

namespace meps
{
    public partial class SettingForm : Form
    {
        BindingSource bs = new BindingSource();

        public SettingForm()
        {
            InitializeComponent();
            User.GetUsers();

            textBoxIP.Text = Settings.DataBaseHost;
            textBoxImageFolder.Text = Settings.ImageHost;
            textBoxGroupCount.Text = Settings.GroupCount.ToString();
            
            //ポート、エクセルフォルダ設定ロード            
            textBoxBaseExcelDir.Text = Settings.BaseExcelDir;
            textBoxDBPort.Text = Settings.dbPort;


            dataGridViewUser.DataSource = bs;
            UserListUpdate();
            dataGridViewUser.Columns[nameof(User.Pass)].Visible = false;
            bs.CurrentItemChanged += Bs_CurrentItemChanged;
            setUser();

            string defPrinterName;
            using (var pd = new PrintDocument())
            {
                defPrinterName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == defPrinterName)
                    comboBox1.SelectedIndex = i;
            }

            var pn = Settings.DefaultPrinterName;
            if (comboBox1.Items.Contains(pn)) comboBox1.SelectedItem = pn;

        }

        private void Bs_CurrentItemChanged(object sender, EventArgs e)
        {
            setUser();
        }

        private void setUser()
        {
            var u =(User)bs.Current;

            if (u.UserID == 0)
            {
                textBoxUserID.Clear();
                textBoxUserID.ReadOnly = false;
                textBoxuserName.Clear();
                textBoxUserPass.Clear();
                textBoxUserRoman.Clear();
                checkBoxEnable.Checked = true;
                checkBoxAdmin.Checked = false;
                buttonAddUser.Text = "追加";
            }
            else
            {
                textBoxUserID.Text = u.UserID.ToString();
                textBoxUserID.ReadOnly = true;
                textBoxuserName.Text = u.Name;
                textBoxUserPass.Text = u.Pass;
                textBoxUserRoman.Text = u.LoginName;
                checkBoxEnable.Checked = u.Enable;
                checkBoxAdmin.Checked = u.Admin;
                buttonAddUser.Text = "変更";
            }


            buttonAddUser.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

            Settings.DataBaseHost = textBoxIP.Text;
            Settings.ImageHost = textBoxImageFolder.Text;
            Settings.DefaultPrinterName = (string)comboBox1.SelectedItem;


            //ポート、エクセルフォルダ設定書き込み
            
            Settings.BaseExcelDir = textBoxBaseExcelDir.Text.Trim();
            Settings.dbPort = textBoxDBPort.Text.Trim();

            int gc;
            int.TryParse(textBoxGroupCount.Text, out gc);
            Settings.GroupCount = gc;

            Properties.Settings.Default.Save();

            this.Close();

            
            //設定を有効化するため再起動
            
            MessageBox.Show("再度起動します。", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //プログラム終了
            try
            {
                Application.Exit();
            }
            catch
            {
                try
                {
                    //強制終了
                    Environment.Exit(0);
                }
                catch
                {
                    //握りつぶす
                }
            }

            //新プログラム起動
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// User管理機能(追加・変更のみ)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            //データチェック
            if (textBoxUserID.Text == "" || textBoxuserName.Text == ""
                || textBoxUserRoman.Text == "" || textBoxUserPass.Text == "")
            {
                MessageBox.Show("空白の欄があります");
                return;
            }
            
            int userid;
            userid = int.TryParse(textBoxUserID.Text, out userid) ? userid : 0;
            if (userid == 0)
            {
                MessageBox.Show("UserIDには数字のみ入力してください");
                return;
            }

            bool update = true;
            if (((User)bs.Current).UserID == 0)
            {
                if (User.GetList().FirstOrDefault(item => item.UserID == userid) != null)
                {
                    MessageBox.Show("指定されたIDは既に使用されています");
                    return;
                }
                update = false;
            }

            var msg = (update ? "選択中のユーザーを更新します。" : "新規ユーザーを登録します。") + "よろしいですか？";
            if (MessageBox.Show(msg, "ユーザー確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                 DialogResult.OK) return;

            if (update)
            {
                var u = User.GetUser(userid);
                u.Name = textBoxuserName.Text;
                u.LoginName = textBoxUserRoman.Text;
                u.Pass = textBoxUserPass.Text;
                u.Enable = checkBoxEnable.Checked;
                u.Admin = checkBoxAdmin.Checked;
                u.Update();
            }
            else
            {
                var u = new User(userid, textBoxuserName.Text);
                u.LoginName = textBoxUserRoman.Text;
                u.Pass = textBoxUserPass.Text;
                u.Enable = checkBoxEnable.Checked;
                u.Admin = checkBoxAdmin.Checked;
                u.Insert();
            }

            //表示を更新
            UserListUpdate();
            MessageBox.Show(update ? "更新しました" : "登録しました");
        }

        //Userリスト表示の更新
        private void UserListUpdate()
        {
            User.GetUsers();
            var l = User.GetList();

            l.Add(new User(0, "新規"));
            l.Sort((x, y) => x.UserID.CompareTo(y.UserID));
            bs.DataSource = l;

            dgSetting();
        }

        private void FormSetting_Shown(object sender, EventArgs e)
        {
            //20201123190136 furukawa st ////////////////////////
            //ユーザ名NULLエラー回避
            
            if (User.CurrentUser == null) return;
            //20201123190136 furukawa ed ////////////////////////


            //開発者のみIP関連変更可能            
            btn_insurer.Visible = User.CurrentUser.Developer;
            textBoxIP.ReadOnly = !User.CurrentUser.Developer;
            textBoxImageFolder.ReadOnly = !User.CurrentUser.Developer;

            //ポート、エクセルフォルダは開発者のみ変更可能            
            textBoxBaseExcelDir.ReadOnly = !User.CurrentUser.Developer;
            textBoxDBPort.ReadOnly = !User.CurrentUser.Developer;
            



            if (User.CurrentUser.Admin) return;
            MessageBox.Show("現在ログインしているユーザーには管理権限がありません", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.Close();

        }

        
        //グリッド調整、文字サイズ調整(デザイナ)

        private void dgSetting()
        {
            this.dataGridViewUser.Columns["userID"].Width = 50;
            this.dataGridViewUser.Columns["Name"].Width = 150;
            this.dataGridViewUser.Columns["LoginName"].Width = 200;
            this.dataGridViewUser.Columns["Enable"].Width = 70;
            this.dataGridViewUser.Columns["Admin"].Width = 70;            
        }

        private void btn_insurer_Click(object sender, EventArgs e)
        {
            SettingForm_Insurer frm = new SettingForm_Insurer();
            frm.ShowDialog();
        }
    }
}
