﻿using System;
using System.Windows.Forms;

namespace meps.Forms
{
    public partial class WaitFormSimple : Form
    {
        public WaitFormSimple()
        {
            InitializeComponent();
        }

        public void InvokeCloseDispose()
        {
            Invoke(new Action(() =>
            {
                this.Close();
                this.Dispose();
            }));
        }
    }
}
