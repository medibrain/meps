﻿namespace meps.Forms
{
    partial class InquiryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonExit = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.textBoxMonth = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSearchYM = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonSimpleSearch = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonOutput = new System.Windows.Forms.Button();
            this.buttonMediVery = new System.Windows.Forms.Button();
            this.buttonImport = new System.Windows.Forms.Button();
            this.comboBoxPref = new System.Windows.Forms.ComboBox();
            this.buttonSearchPref = new System.Windows.Forms.Button();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(700, 531);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(76, 24);
            this.buttonExit.TabIndex = 6;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxYear);
            this.groupBox4.Controls.Add(this.textBoxMonth);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(271, 72);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "審査年月を指定：";
            // 
            // textBoxYear
            // 
            this.textBoxYear.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxYear.Location = new System.Drawing.Point(86, 28);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(30, 26);
            this.textBoxYear.TabIndex = 1;
            this.textBoxYear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxMonth
            // 
            this.textBoxMonth.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxMonth.Location = new System.Drawing.Point(151, 28);
            this.textBoxMonth.Name = "textBoxMonth";
            this.textBoxMonth.Size = new System.Drawing.Size(30, 26);
            this.textBoxMonth.TabIndex = 3;
            this.textBoxMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(6, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "平成/令和";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(122, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "年";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(187, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "月 審査分";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 128);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(764, 397);
            this.dataGridView1.TabIndex = 3;
            // 
            // buttonSearchYM
            // 
            this.buttonSearchYM.Location = new System.Drawing.Point(12, 99);
            this.buttonSearchYM.Name = "buttonSearchYM";
            this.buttonSearchYM.Size = new System.Drawing.Size(197, 23);
            this.buttonSearchYM.TabIndex = 4;
            this.buttonSearchYM.Text = ">> 上記審査年月で都道府県を検索";
            this.buttonSearchYM.UseVisualStyleBackColor = true;
            this.buttonSearchYM.Click += new System.EventHandler(this.buttonSearchYM_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonSimpleSearch);
            this.groupBox1.Controls.Add(this.buttonDelete);
            this.groupBox1.Controls.Add(this.buttonOutput);
            this.groupBox1.Controls.Add(this.buttonMediVery);
            this.groupBox1.Controls.Add(this.buttonImport);
            this.groupBox1.Location = new System.Drawing.Point(299, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(477, 71);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "作業を選択";
            // 
            // buttonSimpleSearch
            // 
            this.buttonSimpleSearch.Enabled = false;
            this.buttonSimpleSearch.Location = new System.Drawing.Point(290, 18);
            this.buttonSimpleSearch.Name = "buttonSimpleSearch";
            this.buttonSimpleSearch.Size = new System.Drawing.Size(84, 42);
            this.buttonSimpleSearch.TabIndex = 15;
            this.buttonSimpleSearch.Text = "簡易検索";
            this.buttonSimpleSearch.UseVisualStyleBackColor = true;
            this.buttonSimpleSearch.Click += new System.EventHandler(this.buttonSimpleSearch_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(380, 18);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(84, 42);
            this.buttonDelete.TabIndex = 14;
            this.buttonDelete.Text = "削除";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonOutput
            // 
            this.buttonOutput.Location = new System.Drawing.Point(200, 18);
            this.buttonOutput.Name = "buttonOutput";
            this.buttonOutput.Size = new System.Drawing.Size(84, 42);
            this.buttonOutput.TabIndex = 13;
            this.buttonOutput.Text = "出力";
            this.buttonOutput.UseVisualStyleBackColor = true;
            this.buttonOutput.Click += new System.EventHandler(this.buttonOutput_Click);
            // 
            // buttonMediVery
            // 
            this.buttonMediVery.Location = new System.Drawing.Point(110, 18);
            this.buttonMediVery.Name = "buttonMediVery";
            this.buttonMediVery.Size = new System.Drawing.Size(84, 42);
            this.buttonMediVery.TabIndex = 12;
            this.buttonMediVery.Text = "Medi-very";
            this.buttonMediVery.UseVisualStyleBackColor = true;
            this.buttonMediVery.Click += new System.EventHandler(this.buttonMediVery_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.Location = new System.Drawing.Point(20, 18);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(84, 42);
            this.buttonImport.TabIndex = 11;
            this.buttonImport.Text = "データ取込";
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // comboBoxPref
            // 
            this.comboBoxPref.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPref.FormattingEnabled = true;
            this.comboBoxPref.Location = new System.Drawing.Point(462, 100);
            this.comboBoxPref.Name = "comboBoxPref";
            this.comboBoxPref.Size = new System.Drawing.Size(121, 20);
            this.comboBoxPref.TabIndex = 11;
            // 
            // buttonSearchPref
            // 
            this.buttonSearchPref.Location = new System.Drawing.Point(589, 99);
            this.buttonSearchPref.Name = "buttonSearchPref";
            this.buttonSearchPref.Size = new System.Drawing.Size(188, 23);
            this.buttonSearchPref.TabIndex = 12;
            this.buttonSearchPref.Text = ">> 上記審査年と都道府県で検索";
            this.buttonSearchPref.UseVisualStyleBackColor = true;
            this.buttonSearchPref.Click += new System.EventHandler(this.buttonSearchPref_Click);
            // 
            // InquiryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 566);
            this.Controls.Add(this.buttonSearchPref);
            this.Controls.Add(this.comboBoxPref);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonSearchYM);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.buttonExit);
            this.Name = "InquiryForm";
            this.Text = "MEPS -照会管理-";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.TextBox textBoxMonth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSearchYM;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonOutput;
        private System.Windows.Forms.Button buttonMediVery;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonSimpleSearch;
        private System.Windows.Forms.ComboBox comboBoxPref;
        private System.Windows.Forms.Button buttonSearchPref;
    }
}