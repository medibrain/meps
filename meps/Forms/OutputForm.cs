﻿using meps.Table;
using meps.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Forms
{
    public partial class OutputForm : Form
    {
        BindingSource bsPref = new BindingSource();
        int iym;

        public OutputForm(int iym)
        {
            InitializeComponent();
            this.iym = iym;

            var yy = DateTimeEx.GetHYYFromYYYYMM(iym).ToString("00");
            var MM = DateTimeEx.GetMMFromYYYYMM(iym).ToString("00");
            groupBoxOutput.Text = $"平成 {yy} 年 {MM} 月分";

            var list = PrefInfo.GetPrefInfo(iym);
            bsPref.DataSource = list;
            dataGridView1.DataSource = bsPref;

            foreach (DataGridViewColumn col in dataGridView1.Columns) col.Visible = false;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            DataGridViewColumn dgvc;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PID)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "番号";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PrefName)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "都道府県";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatusMaster)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "総合状況";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus1Str)];
            dgvc.HeaderText = "１回目";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus2Str)];
            dgvc.HeaderText = "ベリファイ";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.AllCount)];
            dgvc.HeaderText = "登録件数";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
        }

        /**
         * 出力先フォルダ選択ボタン
         */
        private void buttonFolderSelect_Click(object sender, EventArgs e)
        {
            string fn;
            var f = new DialogUtility.OpenFolderDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                fn = f.FolderPath;
            }
            else return;

            textBoxOutputFolderPath.Text = fn;
        }

        /**
         * 出力ボタン
         */
        private void buttonOutput_Click(object sender, EventArgs e)
        {
            //選択されている都道府県の取得
            var piList = new List<PrefInfo>();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                var pi = (PrefInfo)bsPref.List[row.Index];
                piList.Add(pi);
            }            
            if (piList.Count() == 0)
            {
                MessageBox.Show("出力する都道府県を選択してください", "警告");
                return;
            }

            //出力情報の確認
            List<OutputCheckForm.OutputListItem> outputList = null;
            using (var f = new OutputCheckForm(piList, iym))
            {
                var result = f.ShowDialog();
                if (result != DialogResult.OK) return;
                outputList = f.List;
            }

            //全体件数の取得
            int max = 0;
            outputList.ForEach(ol => max += ol.Count);

            //出力中、プログレスバーの表示
            var wf = new WaitForm();
            var allOutputResult = true;
            try
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.Max = max;
                System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
                while (!wf.Visible) System.Threading.Thread.Sleep(10);

                //コールバック
                Action<int> callback = maid =>
                {
                    wf.InvokeValue++;
                };

                //出力開始
                var errList = new List<PrefInfo>();
                bool opResult;
                foreach (var ol in outputList)
                {
                    if (System.IO.Directory.Exists(ol.OutputPath))
                    {
                        allOutputResult = false;
                        MessageBox.Show(
                            $"{ol.OutputPath}\r\n\r\n上記フォルダはすでに存在します。\r\n削除してから再度実行してください", 
                            $"【{ol.Name}】は出力できません", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }
                    opResult = Output.MediApp(iym, Pref.GetPref(ol.PID), ol.OutputPath, true, callback);
                    if (!opResult)
                    {
                        allOutputResult = false;
                        MessageBox.Show(
                            $"【{ol.Name}】の出力時にエラーが発生しました。",
                            $"【{ol.Name}】は出力できません", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        continue;
                    }
                }
            }
            finally
            {
                wf.InvokeCloseDispose();
            }

            if (allOutputResult)
            {
                MessageBox.Show($"{piList.Count} 件の都道府県の出力が完了しました。", "出力完了");
            }
            else
            {
                MessageBox.Show($"出力に失敗した都道府県が存在します。\r\n確認を行ってください。", "出力に失敗したものがあります");
            }            
        }

        /**
         * 総合状況「完了」で絞り込みボタン
         */
        private void button1_Click(object sender, EventArgs e)
        {
            var piList = (List<PrefInfo>)bsPref.DataSource;
            var newList = piList.Where(pi => pi.InputStatusMaster == "完了").ToList();
            bsPref.DataSource = newList;
        }

        /**
         * 全件表示ボタン
         */
        private void button2_Click(object sender, EventArgs e)
        {
            var list = PrefInfo.GetPrefInfo(iym);
            bsPref.DataSource = list;
        }
    }
}
