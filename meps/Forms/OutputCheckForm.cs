﻿using meps.Table;
using meps.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Forms
{
    public partial class OutputCheckForm : Form
    {
        public List<OutputListItem> List;

        private List<OutputListItem> oList;
        private BindingSource bsPI = new BindingSource();

        public class OutputListItem
        {
            public PrefInfo PrefInfo { get; set; }
            public int PID { get; set; }
            public string Name { get; set; }
            public int Count { get; set; }
            public string OutputPath { get; set; }
        }

        public OutputCheckForm(List<PrefInfo> maiList, int iym)
        {
            InitializeComponent();
            this.DialogResult = DialogResult.Cancel;

            var yy = DateTimeEx.GetHYYFromYYYYMM(iym);
            var MM = DateTimeEx.GetMMFromYYYYMM(iym);

            Func<string, string> toFullOutputPath = outputpath =>
            {
                if (string.IsNullOrWhiteSpace(outputpath)) return null;
                var exportFolderPath = outputpath;
                if (exportFolderPath.Contains("{eemm}"))
                {
                    exportFolderPath = exportFolderPath.Replace("{eemm}", yy.ToString("00") + MM.ToString("00"));
                }
                return exportFolderPath;
            };

            oList = new List<OutputListItem>();
            OutputListItem oli;
            foreach(var item in maiList)
            {
                oli = new OutputListItem();
                oli.PID = item.PID;
                oli.Name = item.PrefName;
                oli.Count = item.AllCount;
                oli.OutputPath = toFullOutputPath(Pref.GetPref(item.PID)?.OuputPath);
                oList.Add(oli);
            }

            bsPI.DataSource = oList;
            dataGridView1.DataSource = bsPI;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                item.Visible = false;
                item.HeaderCell.Style.WrapMode = DataGridViewTriState.False;
            }
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            DataGridViewColumn dgvc;
            dgvc = dataGridView1.Columns[nameof(OutputListItem.PID)];
            dgvc.Visible = true;
            dgvc.HeaderText = "都道府県番号";
            dgvc = dataGridView1.Columns[nameof(OutputListItem.Name)];
            dgvc.Visible = true;
            dgvc.HeaderText = "都道府県名";
            dgvc = dataGridView1.Columns[nameof(OutputListItem.Count)];
            dgvc.Visible = true;
            dgvc.HeaderText = "件数";
            dgvc = dataGridView1.Columns[nameof(OutputListItem.OutputPath)];
            dgvc.Visible = true;
            dgvc.HeaderText = "出力先";
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.List = null;
            this.Close();
        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.List = oList;
            this.Close();
        }
    }
}
