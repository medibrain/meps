﻿using meps.Controls;

namespace meps.Forms
{
    partial class RezeptInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonRegist = new System.Windows.Forms.Button();
            this.labelY = new System.Windows.Forms.Label();
            this.labelM = new System.Windows.Forms.Label();
            this.labelHnum = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelControlScroll = new System.Windows.Forms.Panel();
            this.panelControls = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.verifyBoxMediAppType = new meps.Controls.VerifyBox();
            this.panelInput = new System.Windows.Forms.Panel();
            this.groupBoxDRG = new System.Windows.Forms.GroupBox();
            this.label66 = new System.Windows.Forms.Label();
            this.verifyBoxDrgMediDate2D = new meps.Controls.VerifyBox();
            this.label61 = new System.Windows.Forms.Label();
            this.verifyBoxDrgMediDate2M = new meps.Controls.VerifyBox();
            this.verifyBoxDrgMediDate2Y = new meps.Controls.VerifyBox();
            this.verifyBoxDrgMediDate2G = new meps.Controls.VerifyBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.verifyBoxDrgMediDate1D = new meps.Controls.VerifyBox();
            this.label34 = new System.Windows.Forms.Label();
            this.verifyBoxDrgMediDate1M = new meps.Controls.VerifyBox();
            this.verifyBoxDrgMediDate1Y = new meps.Controls.VerifyBox();
            this.verifyBoxDrgMediDate1G = new meps.Controls.VerifyBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.verifyBoxDrgType = new meps.Controls.VerifyBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.verifyBoxBirthD = new meps.Controls.VerifyBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.verifyBoxInum = new meps.Controls.VerifyBox();
            this.verifyBoxM = new meps.Controls.VerifyBox();
            this.label27 = new System.Windows.Forms.Label();
            this.verifyBoxHospital = new meps.Controls.VerifyBox();
            this.label2 = new System.Windows.Forms.Label();
            this.verifyBoxPref = new meps.Controls.VerifyBox();
            this.label39 = new System.Windows.Forms.Label();
            this.verifyBoxY = new meps.Controls.VerifyBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.verifyBoxKouhi2Code = new meps.Controls.VerifyBox();
            this.label31 = new System.Windows.Forms.Label();
            this.verifyBoxHonninKazoku = new meps.Controls.VerifyBox();
            this.verifyBoxTuuchi = new meps.Controls.VerifyBox();
            this.verifyBoxMeisai = new meps.Controls.VerifyBox();
            this.label15 = new System.Windows.Forms.Label();
            this.verifyBoxHnum = new meps.Controls.VerifyBox();
            this.label52 = new System.Windows.Forms.Label();
            this.verifyBoxKouhi1Code = new meps.Controls.VerifyBox();
            this.label36 = new System.Windows.Forms.Label();
            this.verifyBoxTokki = new meps.Controls.VerifyBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.verifyBoxAgain = new meps.Controls.VerifyBox();
            this.verifyBoxSyokujiKouhi2HutanPrice = new meps.Controls.VerifyBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.verifyBoxSyokujiKouhi2MediPrice = new meps.Controls.VerifyBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.verifyBoxSyokujiKouhi2Days = new meps.Controls.VerifyBox();
            this.verifyBoxBirthM = new meps.Controls.VerifyBox();
            this.verifyBoxMediKouhi2Price = new meps.Controls.VerifyBox();
            this.verifyBoxBirthY = new meps.Controls.VerifyBox();
            this.verifyBoxKougaku = new meps.Controls.VerifyBox();
            this.verifyBoxBirthEra = new meps.Controls.VerifyBox();
            this.label14 = new System.Windows.Forms.Label();
            this.verifyBoxSex = new meps.Controls.VerifyBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.verifyBoxMediKouhi2Point = new meps.Controls.VerifyBox();
            this.verifyBoxMediKouhi2Days = new meps.Controls.VerifyBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.verifyBoxSyokujiKouhi1HutanPrice = new meps.Controls.VerifyBox();
            this.verifyBoxMediKouhi1Price = new meps.Controls.VerifyBox();
            this.verifyBoxSyokujiKouhi1MediPrice = new meps.Controls.VerifyBox();
            this.verifyBoxMediKouhi1Point = new meps.Controls.VerifyBox();
            this.verifyBoxSyokujiKouhi1Days = new meps.Controls.VerifyBox();
            this.labelSex = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.verifyBoxMediKouhi1Days = new meps.Controls.VerifyBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.verifyBoxSyokujiHutanPrice = new meps.Controls.VerifyBox();
            this.verifyBoxMediPrice = new meps.Controls.VerifyBox();
            this.verifyBoxSyokujiMediPrice = new meps.Controls.VerifyBox();
            this.verifyBoxMediPoint = new meps.Controls.VerifyBox();
            this.verifyBoxSyokujiDays = new meps.Controls.VerifyBox();
            this.verifyBoxMediDays = new meps.Controls.VerifyBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.verifyBoxNumbering = new meps.Controls.VerifyBox();
            this.labelNumberingOrBatch = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonBack = new System.Windows.Forms.Button();
            this.labelInputerName = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panelDatalist = new System.Windows.Forms.Panel();
            this.dataGridViewPlist = new System.Windows.Forms.DataGridView();
            this.panelUP = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.labelPref = new System.Windows.Forms.Label();
            this.labelSeikyu = new System.Windows.Forms.Label();
            this.buttonImageChange = new System.Windows.Forms.Button();
            this.buttonImageRotateL = new System.Windows.Forms.Button();
            this.buttonImageRotateR = new System.Windows.Forms.Button();
            this.buttonImageFill = new System.Windows.Forms.Button();
            this.scrollPictureControl1 = new meps.Controls.ScrollPictureControl();
            this.panelRight.SuspendLayout();
            this.panelControlScroll.SuspendLayout();
            this.panelControls.SuspendLayout();
            this.panelInput.SuspendLayout();
            this.groupBoxDRG.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelDatalist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).BeginInit();
            this.panelUP.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRegist
            // 
            this.buttonRegist.BackColor = System.Drawing.Color.Turquoise;
            this.buttonRegist.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonRegist.Location = new System.Drawing.Point(112, 3);
            this.buttonRegist.Name = "buttonRegist";
            this.buttonRegist.Size = new System.Drawing.Size(111, 25);
            this.buttonRegist.TabIndex = 1;
            this.buttonRegist.Text = "登録 (PageUp)";
            this.buttonRegist.UseVisualStyleBackColor = false;
            this.buttonRegist.Click += new System.EventHandler(this.buttonRegist_Click);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(74, 26);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(19, 13);
            this.labelY.TabIndex = 2;
            this.labelY.Text = "年";
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Location = new System.Drawing.Point(129, 23);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(31, 13);
            this.labelM.TabIndex = 4;
            this.labelM.Text = "月分";
            // 
            // labelHnum
            // 
            this.labelHnum.AutoSize = true;
            this.labelHnum.Location = new System.Drawing.Point(8, 132);
            this.labelHnum.Name = "labelHnum";
            this.labelHnum.Size = new System.Drawing.Size(31, 26);
            this.labelHnum.TabIndex = 15;
            this.labelHnum.Text = "被保\r\n番号";
            this.labelHnum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(344, 3);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 25);
            this.buttonExit.TabIndex = 3;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "和暦";
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.panelControlScroll);
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(923, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(421, 801);
            this.panelRight.TabIndex = 2;
            // 
            // panelControlScroll
            // 
            this.panelControlScroll.Controls.Add(this.panelControls);
            this.panelControlScroll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlScroll.Location = new System.Drawing.Point(0, 0);
            this.panelControlScroll.Name = "panelControlScroll";
            this.panelControlScroll.Size = new System.Drawing.Size(421, 771);
            this.panelControlScroll.TabIndex = 0;
            // 
            // panelControls
            // 
            this.panelControls.AutoScroll = true;
            this.panelControls.AutoScrollMinSize = new System.Drawing.Size(350, 800);
            this.panelControls.Controls.Add(this.label29);
            this.panelControls.Controls.Add(this.verifyBoxMediAppType);
            this.panelControls.Controls.Add(this.panelInput);
            this.panelControls.Controls.Add(this.verifyBoxNumbering);
            this.panelControls.Controls.Add(this.labelNumberingOrBatch);
            this.panelControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControls.Location = new System.Drawing.Point(0, 0);
            this.panelControls.Name = "panelControls";
            this.panelControls.Size = new System.Drawing.Size(421, 771);
            this.panelControls.TabIndex = 0;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label29.Location = new System.Drawing.Point(16, 8);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(108, 39);
            this.label29.TabIndex = 0;
            this.label29.Text = "ﾊﾞｯﾁ：--　不要：//\r\n付箋：++　再審査\r\n続紙：**　　 返戻：##";
            // 
            // verifyBoxMediAppType
            // 
            this.verifyBoxMediAppType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediAppType.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediAppType.IsVerifying = false;
            this.verifyBoxMediAppType.Location = new System.Drawing.Point(129, 12);
            this.verifyBoxMediAppType.MaxLength = 2;
            this.verifyBoxMediAppType.Name = "verifyBoxMediAppType";
            this.verifyBoxMediAppType.Required = false;
            this.verifyBoxMediAppType.Size = new System.Drawing.Size(31, 26);
            this.verifyBoxMediAppType.TabIndex = 1;
            this.verifyBoxMediAppType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.verifyBoxMediAppType.TextMaxLength = 2;
            this.verifyBoxMediAppType.TextMode = meps.Controls.VerifyBox.TEXT_MODE.ETC;
            this.verifyBoxMediAppType.VerifyLocationVertical = true;
            this.verifyBoxMediAppType.Leave += new System.EventHandler(this.verifyBoxMediAppType_Leave);
            // 
            // panelInput
            // 
            this.panelInput.Controls.Add(this.groupBoxDRG);
            this.panelInput.Controls.Add(this.label24);
            this.panelInput.Controls.Add(this.verifyBoxBirthD);
            this.panelInput.Controls.Add(this.label30);
            this.panelInput.Controls.Add(this.label28);
            this.panelInput.Controls.Add(this.label1);
            this.panelInput.Controls.Add(this.verifyBoxInum);
            this.panelInput.Controls.Add(this.verifyBoxM);
            this.panelInput.Controls.Add(this.labelM);
            this.panelInput.Controls.Add(this.label27);
            this.panelInput.Controls.Add(this.verifyBoxHospital);
            this.panelInput.Controls.Add(this.label2);
            this.panelInput.Controls.Add(this.verifyBoxPref);
            this.panelInput.Controls.Add(this.label39);
            this.panelInput.Controls.Add(this.verifyBoxY);
            this.panelInput.Controls.Add(this.label16);
            this.panelInput.Controls.Add(this.label33);
            this.panelInput.Controls.Add(this.verifyBoxKouhi2Code);
            this.panelInput.Controls.Add(this.label31);
            this.panelInput.Controls.Add(this.verifyBoxHonninKazoku);
            this.panelInput.Controls.Add(this.verifyBoxTuuchi);
            this.panelInput.Controls.Add(this.verifyBoxMeisai);
            this.panelInput.Controls.Add(this.label8);
            this.panelInput.Controls.Add(this.label15);
            this.panelInput.Controls.Add(this.labelY);
            this.panelInput.Controls.Add(this.verifyBoxHnum);
            this.panelInput.Controls.Add(this.label52);
            this.panelInput.Controls.Add(this.verifyBoxKouhi1Code);
            this.panelInput.Controls.Add(this.label36);
            this.panelInput.Controls.Add(this.verifyBoxTokki);
            this.panelInput.Controls.Add(this.label35);
            this.panelInput.Controls.Add(this.label6);
            this.panelInput.Controls.Add(this.verifyBoxAgain);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi2HutanPrice);
            this.panelInput.Controls.Add(this.label38);
            this.panelInput.Controls.Add(this.label53);
            this.panelInput.Controls.Add(this.label37);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi2MediPrice);
            this.panelInput.Controls.Add(this.label25);
            this.panelInput.Controls.Add(this.label54);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi2Days);
            this.panelInput.Controls.Add(this.verifyBoxBirthM);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi2Price);
            this.panelInput.Controls.Add(this.verifyBoxBirthY);
            this.panelInput.Controls.Add(this.verifyBoxKougaku);
            this.panelInput.Controls.Add(this.verifyBoxBirthEra);
            this.panelInput.Controls.Add(this.labelHnum);
            this.panelInput.Controls.Add(this.label14);
            this.panelInput.Controls.Add(this.verifyBoxSex);
            this.panelInput.Controls.Add(this.label56);
            this.panelInput.Controls.Add(this.label59);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi2Point);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi2Days);
            this.panelInput.Controls.Add(this.label7);
            this.panelInput.Controls.Add(this.label49);
            this.panelInput.Controls.Add(this.label9);
            this.panelInput.Controls.Add(this.label50);
            this.panelInput.Controls.Add(this.label10);
            this.panelInput.Controls.Add(this.label51);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi1HutanPrice);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi1Price);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi1MediPrice);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi1Point);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiKouhi1Days);
            this.panelInput.Controls.Add(this.labelSex);
            this.panelInput.Controls.Add(this.label4);
            this.panelInput.Controls.Add(this.verifyBoxMediKouhi1Days);
            this.panelInput.Controls.Add(this.labelBirthday);
            this.panelInput.Controls.Add(this.label11);
            this.panelInput.Controls.Add(this.label5);
            this.panelInput.Controls.Add(this.label48);
            this.panelInput.Controls.Add(this.label17);
            this.panelInput.Controls.Add(this.label12);
            this.panelInput.Controls.Add(this.label18);
            this.panelInput.Controls.Add(this.label47);
            this.panelInput.Controls.Add(this.label13);
            this.panelInput.Controls.Add(this.label46);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiHutanPrice);
            this.panelInput.Controls.Add(this.verifyBoxMediPrice);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiMediPrice);
            this.panelInput.Controls.Add(this.verifyBoxMediPoint);
            this.panelInput.Controls.Add(this.verifyBoxSyokujiDays);
            this.panelInput.Controls.Add(this.verifyBoxMediDays);
            this.panelInput.Controls.Add(this.label19);
            this.panelInput.Controls.Add(this.label45);
            this.panelInput.Controls.Add(this.label20);
            this.panelInput.Controls.Add(this.label44);
            this.panelInput.Controls.Add(this.label21);
            this.panelInput.Controls.Add(this.label43);
            this.panelInput.Controls.Add(this.label22);
            this.panelInput.Controls.Add(this.label42);
            this.panelInput.Controls.Add(this.label23);
            this.panelInput.Controls.Add(this.label41);
            this.panelInput.Controls.Add(this.label40);
            this.panelInput.Location = new System.Drawing.Point(4, 70);
            this.panelInput.Name = "panelInput";
            this.panelInput.Size = new System.Drawing.Size(398, 1018);
            this.panelInput.TabIndex = 4;
            // 
            // groupBoxDRG
            // 
            this.groupBoxDRG.Controls.Add(this.label66);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate2D);
            this.groupBoxDRG.Controls.Add(this.label61);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate2M);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate2Y);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate2G);
            this.groupBoxDRG.Controls.Add(this.label62);
            this.groupBoxDRG.Controls.Add(this.label63);
            this.groupBoxDRG.Controls.Add(this.label64);
            this.groupBoxDRG.Controls.Add(this.label65);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate1D);
            this.groupBoxDRG.Controls.Add(this.label34);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate1M);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate1Y);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgMediDate1G);
            this.groupBoxDRG.Controls.Add(this.label55);
            this.groupBoxDRG.Controls.Add(this.label57);
            this.groupBoxDRG.Controls.Add(this.label58);
            this.groupBoxDRG.Controls.Add(this.label60);
            this.groupBoxDRG.Controls.Add(this.verifyBoxDrgType);
            this.groupBoxDRG.Controls.Add(this.label32);
            this.groupBoxDRG.Location = new System.Drawing.Point(13, 775);
            this.groupBoxDRG.Name = "groupBoxDRG";
            this.groupBoxDRG.Size = new System.Drawing.Size(376, 208);
            this.groupBoxDRG.TabIndex = 92;
            this.groupBoxDRG.TabStop = false;
            this.groupBoxDRG.Text = "DRGが含まれている場合";
            this.groupBoxDRG.Visible = false;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label66.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label66.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label66.Location = new System.Drawing.Point(8, 28);
            this.label66.Name = "label66";
            this.label66.Padding = new System.Windows.Forms.Padding(2, 13, 2, 13);
            this.label66.Size = new System.Drawing.Size(128, 138);
            this.label66.TabIndex = 0;
            this.label66.Text = "DRG：\r\n　短期滞在出術等基本料\r\n　などが含まれている場合。\r\n\r\nDRG区分：\r\n　入院と退院が同じ月＝1\r\n　同じでない\r\n　　→\r\n　　　入院レセ＝0\r" +
    "\n　　　退院レセ＝2";
            // 
            // verifyBoxDrgMediDate2D
            // 
            this.verifyBoxDrgMediDate2D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate2D.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate2D.IsVerifying = false;
            this.verifyBoxDrgMediDate2D.Location = new System.Drawing.Point(329, 148);
            this.verifyBoxDrgMediDate2D.MaxLength = 2;
            this.verifyBoxDrgMediDate2D.Name = "verifyBoxDrgMediDate2D";
            this.verifyBoxDrgMediDate2D.Required = false;
            this.verifyBoxDrgMediDate2D.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate2D.TabIndex = 19;
            this.verifyBoxDrgMediDate2D.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate2D.TextMaxLength = 2;
            this.verifyBoxDrgMediDate2D.VerifyLocationVertical = true;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(355, 164);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(19, 13);
            this.label61.TabIndex = 20;
            this.label61.Text = "日";
            // 
            // verifyBoxDrgMediDate2M
            // 
            this.verifyBoxDrgMediDate2M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate2M.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate2M.IsVerifying = false;
            this.verifyBoxDrgMediDate2M.Location = new System.Drawing.Point(287, 148);
            this.verifyBoxDrgMediDate2M.MaxLength = 2;
            this.verifyBoxDrgMediDate2M.Name = "verifyBoxDrgMediDate2M";
            this.verifyBoxDrgMediDate2M.Required = false;
            this.verifyBoxDrgMediDate2M.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate2M.TabIndex = 17;
            this.verifyBoxDrgMediDate2M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate2M.TextMaxLength = 2;
            this.verifyBoxDrgMediDate2M.VerifyLocationVertical = true;
            // 
            // verifyBoxDrgMediDate2Y
            // 
            this.verifyBoxDrgMediDate2Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate2Y.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate2Y.IsVerifying = false;
            this.verifyBoxDrgMediDate2Y.Location = new System.Drawing.Point(242, 148);
            this.verifyBoxDrgMediDate2Y.MaxLength = 2;
            this.verifyBoxDrgMediDate2Y.Name = "verifyBoxDrgMediDate2Y";
            this.verifyBoxDrgMediDate2Y.Required = false;
            this.verifyBoxDrgMediDate2Y.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate2Y.TabIndex = 15;
            this.verifyBoxDrgMediDate2Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate2Y.TextMaxLength = 2;
            this.verifyBoxDrgMediDate2Y.VerifyLocationVertical = true;
            // 
            // verifyBoxDrgMediDate2G
            // 
            this.verifyBoxDrgMediDate2G.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate2G.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate2G.IsVerifying = false;
            this.verifyBoxDrgMediDate2G.Location = new System.Drawing.Point(186, 148);
            this.verifyBoxDrgMediDate2G.MaxLength = 1;
            this.verifyBoxDrgMediDate2G.Name = "verifyBoxDrgMediDate2G";
            this.verifyBoxDrgMediDate2G.Required = false;
            this.verifyBoxDrgMediDate2G.Size = new System.Drawing.Size(21, 26);
            this.verifyBoxDrgMediDate2G.TabIndex = 13;
            this.verifyBoxDrgMediDate2G.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate2G.TextMaxLength = 1;
            this.verifyBoxDrgMediDate2G.VerifyLocationVertical = true;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(141, 158);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(43, 13);
            this.label62.TabIndex = 12;
            this.label62.Text = "退院日";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label63.Location = new System.Drawing.Point(210, 146);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(31, 39);
            this.label63.TabIndex = 14;
            this.label63.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(313, 164);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(19, 13);
            this.label64.TabIndex = 18;
            this.label64.Text = "月";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(269, 164);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(19, 13);
            this.label65.TabIndex = 16;
            this.label65.Text = "年";
            // 
            // verifyBoxDrgMediDate1D
            // 
            this.verifyBoxDrgMediDate1D.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate1D.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate1D.IsVerifying = false;
            this.verifyBoxDrgMediDate1D.Location = new System.Drawing.Point(329, 89);
            this.verifyBoxDrgMediDate1D.MaxLength = 2;
            this.verifyBoxDrgMediDate1D.Name = "verifyBoxDrgMediDate1D";
            this.verifyBoxDrgMediDate1D.Required = false;
            this.verifyBoxDrgMediDate1D.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate1D.TabIndex = 10;
            this.verifyBoxDrgMediDate1D.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate1D.TextMaxLength = 2;
            this.verifyBoxDrgMediDate1D.VerifyLocationVertical = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(355, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(19, 13);
            this.label34.TabIndex = 11;
            this.label34.Text = "日";
            // 
            // verifyBoxDrgMediDate1M
            // 
            this.verifyBoxDrgMediDate1M.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate1M.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate1M.IsVerifying = false;
            this.verifyBoxDrgMediDate1M.Location = new System.Drawing.Point(287, 89);
            this.verifyBoxDrgMediDate1M.MaxLength = 2;
            this.verifyBoxDrgMediDate1M.Name = "verifyBoxDrgMediDate1M";
            this.verifyBoxDrgMediDate1M.Required = false;
            this.verifyBoxDrgMediDate1M.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate1M.TabIndex = 8;
            this.verifyBoxDrgMediDate1M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate1M.TextMaxLength = 2;
            this.verifyBoxDrgMediDate1M.VerifyLocationVertical = true;
            // 
            // verifyBoxDrgMediDate1Y
            // 
            this.verifyBoxDrgMediDate1Y.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate1Y.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate1Y.IsVerifying = false;
            this.verifyBoxDrgMediDate1Y.Location = new System.Drawing.Point(242, 89);
            this.verifyBoxDrgMediDate1Y.MaxLength = 2;
            this.verifyBoxDrgMediDate1Y.Name = "verifyBoxDrgMediDate1Y";
            this.verifyBoxDrgMediDate1Y.Required = false;
            this.verifyBoxDrgMediDate1Y.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxDrgMediDate1Y.TabIndex = 6;
            this.verifyBoxDrgMediDate1Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate1Y.TextMaxLength = 2;
            this.verifyBoxDrgMediDate1Y.VerifyLocationVertical = true;
            // 
            // verifyBoxDrgMediDate1G
            // 
            this.verifyBoxDrgMediDate1G.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgMediDate1G.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgMediDate1G.IsVerifying = false;
            this.verifyBoxDrgMediDate1G.Location = new System.Drawing.Point(186, 89);
            this.verifyBoxDrgMediDate1G.MaxLength = 1;
            this.verifyBoxDrgMediDate1G.Name = "verifyBoxDrgMediDate1G";
            this.verifyBoxDrgMediDate1G.Required = false;
            this.verifyBoxDrgMediDate1G.Size = new System.Drawing.Size(21, 26);
            this.verifyBoxDrgMediDate1G.TabIndex = 4;
            this.verifyBoxDrgMediDate1G.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgMediDate1G.TextMaxLength = 1;
            this.verifyBoxDrgMediDate1G.VerifyLocationVertical = true;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(141, 99);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(43, 13);
            this.label55.TabIndex = 3;
            this.label55.Text = "入院日";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label57.Location = new System.Drawing.Point(210, 87);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(31, 39);
            this.label57.TabIndex = 5;
            this.label57.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(313, 104);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(19, 13);
            this.label58.TabIndex = 9;
            this.label58.Text = "月";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(269, 104);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(19, 13);
            this.label60.TabIndex = 7;
            this.label60.Text = "年";
            // 
            // verifyBoxDrgType
            // 
            this.verifyBoxDrgType.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxDrgType.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxDrgType.IsVerifying = false;
            this.verifyBoxDrgType.Location = new System.Drawing.Point(205, 28);
            this.verifyBoxDrgType.MaxLength = 2;
            this.verifyBoxDrgType.Name = "verifyBoxDrgType";
            this.verifyBoxDrgType.Required = false;
            this.verifyBoxDrgType.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxDrgType.TabIndex = 2;
            this.verifyBoxDrgType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxDrgType.TextMaxLength = 2;
            this.verifyBoxDrgType.VerifyLocationVertical = true;
            this.verifyBoxDrgType.Leave += new System.EventHandler(this.verifyBoxDrgType_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(141, 38);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(55, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "DRG区分";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label24.Location = new System.Drawing.Point(321, 720);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 13);
            this.label24.TabIndex = 91;
            this.label24.Text = "有：1";
            // 
            // verifyBoxBirthD
            // 
            this.verifyBoxBirthD.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthD.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxBirthD.IsVerifying = false;
            this.verifyBoxBirthD.Location = new System.Drawing.Point(273, 248);
            this.verifyBoxBirthD.MaxLength = 2;
            this.verifyBoxBirthD.Name = "verifyBoxBirthD";
            this.verifyBoxBirthD.Required = false;
            this.verifyBoxBirthD.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxBirthD.TabIndex = 31;
            this.verifyBoxBirthD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBirthD.TextMaxLength = 2;
            this.verifyBoxBirthD.VerifyLocationVertical = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(299, 263);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(19, 13);
            this.label30.TabIndex = 90;
            this.label30.Text = "日";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("MS UI Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label28.Location = new System.Drawing.Point(202, 738);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 22);
            this.label28.TabIndex = 88;
            this.label28.Text = "※画像から判断\r\n　 できる場合";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(334, 685);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 81;
            this.label1.Text = "円";
            // 
            // verifyBoxInum
            // 
            this.verifyBoxInum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxInum.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxInum.IsVerifying = false;
            this.verifyBoxInum.Location = new System.Drawing.Point(217, 70);
            this.verifyBoxInum.MaxLength = 8;
            this.verifyBoxInum.Name = "verifyBoxInum";
            this.verifyBoxInum.Required = false;
            this.verifyBoxInum.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxInum.TabIndex = 14;
            this.verifyBoxInum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxInum.TextMaxLength = 8;
            this.verifyBoxInum.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxInum.VerifyLocationVertical = true;
            // 
            // verifyBoxM
            // 
            this.verifyBoxM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxM.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxM.IsVerifying = false;
            this.verifyBoxM.Location = new System.Drawing.Point(97, 11);
            this.verifyBoxM.MaxLength = 2;
            this.verifyBoxM.Name = "verifyBoxM";
            this.verifyBoxM.Required = false;
            this.verifyBoxM.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxM.TabIndex = 3;
            this.verifyBoxM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxM.TextMaxLength = 2;
            this.verifyBoxM.VerifyLocationVertical = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(173, 73);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 26);
            this.label27.TabIndex = 13;
            this.label27.Text = "保険者\r\n番号";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHospital
            // 
            this.verifyBoxHospital.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHospital.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxHospital.IsVerifying = false;
            this.verifyBoxHospital.Location = new System.Drawing.Point(300, 11);
            this.verifyBoxHospital.MaxLength = 7;
            this.verifyBoxHospital.Name = "verifyBoxHospital";
            this.verifyBoxHospital.Required = false;
            this.verifyBoxHospital.Size = new System.Drawing.Size(75, 26);
            this.verifyBoxHospital.TabIndex = 8;
            this.verifyBoxHospital.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHospital.TextMaxLength = 7;
            this.verifyBoxHospital.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxHospital.VerifyLocationVertical = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 685);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 79;
            this.label2.Text = "円";
            // 
            // verifyBoxPref
            // 
            this.verifyBoxPref.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxPref.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxPref.IsVerifying = false;
            this.verifyBoxPref.Location = new System.Drawing.Point(200, 11);
            this.verifyBoxPref.MaxLength = 2;
            this.verifyBoxPref.Name = "verifyBoxPref";
            this.verifyBoxPref.Required = false;
            this.verifyBoxPref.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxPref.TabIndex = 6;
            this.verifyBoxPref.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxPref.TextMaxLength = 2;
            this.verifyBoxPref.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxPref.VerifyLocationVertical = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(84, 192);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 26);
            this.label39.TabIndex = 22;
            this.label39.Text = "公②\r\n２桁";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxY
            // 
            this.verifyBoxY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxY.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxY.IsVerifying = false;
            this.verifyBoxY.Location = new System.Drawing.Point(41, 11);
            this.verifyBoxY.MaxLength = 2;
            this.verifyBoxY.Name = "verifyBoxY";
            this.verifyBoxY.Required = false;
            this.verifyBoxY.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxY.TabIndex = 1;
            this.verifyBoxY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxY.TextMaxLength = 2;
            this.verifyBoxY.VerifyLocationVertical = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(322, 258);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "特記\r\n";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(240, 12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(55, 26);
            this.label33.TabIndex = 7;
            this.label33.Text = "医療機関\r\nコード";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxKouhi2Code
            // 
            this.verifyBoxKouhi2Code.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKouhi2Code.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxKouhi2Code.IsVerifying = false;
            this.verifyBoxKouhi2Code.Location = new System.Drawing.Point(115, 190);
            this.verifyBoxKouhi2Code.MaxLength = 2;
            this.verifyBoxKouhi2Code.Name = "verifyBoxKouhi2Code";
            this.verifyBoxKouhi2Code.Required = false;
            this.verifyBoxKouhi2Code.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxKouhi2Code.TabIndex = 23;
            this.verifyBoxKouhi2Code.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKouhi2Code.TextMaxLength = 2;
            this.verifyBoxKouhi2Code.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxKouhi2Code.VerifyLocationVertical = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(166, 13);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 26);
            this.label31.TabIndex = 5;
            this.label31.Text = "都道\r\n府県";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHonninKazoku
            // 
            this.verifyBoxHonninKazoku.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHonninKazoku.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxHonninKazoku.IsVerifying = false;
            this.verifyBoxHonninKazoku.Location = new System.Drawing.Point(115, 70);
            this.verifyBoxHonninKazoku.MaxLength = 1;
            this.verifyBoxHonninKazoku.Name = "verifyBoxHonninKazoku";
            this.verifyBoxHonninKazoku.Required = false;
            this.verifyBoxHonninKazoku.Size = new System.Drawing.Size(27, 26);
            this.verifyBoxHonninKazoku.TabIndex = 12;
            this.verifyBoxHonninKazoku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHonninKazoku.TextMaxLength = 1;
            this.verifyBoxHonninKazoku.VerifyLocationVertical = true;
            // 
            // verifyBoxTuuchi
            // 
            this.verifyBoxTuuchi.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTuuchi.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxTuuchi.IsVerifying = false;
            this.verifyBoxTuuchi.Location = new System.Drawing.Point(207, 130);
            this.verifyBoxTuuchi.MaxLength = 1;
            this.verifyBoxTuuchi.Name = "verifyBoxTuuchi";
            this.verifyBoxTuuchi.Required = false;
            this.verifyBoxTuuchi.Size = new System.Drawing.Size(27, 26);
            this.verifyBoxTuuchi.TabIndex = 18;
            this.verifyBoxTuuchi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTuuchi.TextMaxLength = 1;
            this.verifyBoxTuuchi.VerifyLocationVertical = true;
            // 
            // verifyBoxMeisai
            // 
            this.verifyBoxMeisai.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMeisai.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMeisai.IsVerifying = false;
            this.verifyBoxMeisai.Location = new System.Drawing.Point(41, 70);
            this.verifyBoxMeisai.MaxLength = 1;
            this.verifyBoxMeisai.Name = "verifyBoxMeisai";
            this.verifyBoxMeisai.Required = false;
            this.verifyBoxMeisai.Size = new System.Drawing.Size(27, 26);
            this.verifyBoxMeisai.TabIndex = 10;
            this.verifyBoxMeisai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMeisai.TextMaxLength = 1;
            this.verifyBoxMeisai.VerifyLocationVertical = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(10, 192);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 26);
            this.label15.TabIndex = 20;
            this.label15.Text = "公①\r\n２桁";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxHnum
            // 
            this.verifyBoxHnum.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxHnum.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxHnum.IsVerifying = false;
            this.verifyBoxHnum.Location = new System.Drawing.Point(41, 130);
            this.verifyBoxHnum.MaxLength = 10;
            this.verifyBoxHnum.Name = "verifyBoxHnum";
            this.verifyBoxHnum.Required = false;
            this.verifyBoxHnum.Size = new System.Drawing.Size(100, 26);
            this.verifyBoxHnum.TabIndex = 16;
            this.verifyBoxHnum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxHnum.TextMaxLength = 10;
            this.verifyBoxHnum.VerifyLocationVertical = true;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(334, 465);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(19, 13);
            this.label52.TabIndex = 57;
            this.label52.Text = "円";
            // 
            // verifyBoxKouhi1Code
            // 
            this.verifyBoxKouhi1Code.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKouhi1Code.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxKouhi1Code.IsVerifying = false;
            this.verifyBoxKouhi1Code.Location = new System.Drawing.Point(41, 190);
            this.verifyBoxKouhi1Code.MaxLength = 2;
            this.verifyBoxKouhi1Code.Name = "verifyBoxKouhi1Code";
            this.verifyBoxKouhi1Code.Required = false;
            this.verifyBoxKouhi1Code.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxKouhi1Code.TabIndex = 21;
            this.verifyBoxKouhi1Code.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKouhi1Code.TextMaxLength = 2;
            this.verifyBoxKouhi1Code.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxKouhi1Code.VerifyLocationVertical = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(82, 73);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 26);
            this.label36.TabIndex = 11;
            this.label36.Text = "本人\r\n家族";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxTokki
            // 
            this.verifyBoxTokki.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxTokki.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxTokki.IsVerifying = false;
            this.verifyBoxTokki.Location = new System.Drawing.Point(356, 248);
            this.verifyBoxTokki.MaxLength = 2;
            this.verifyBoxTokki.Name = "verifyBoxTokki";
            this.verifyBoxTokki.Required = false;
            this.verifyBoxTokki.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxTokki.TabIndex = 33;
            this.verifyBoxTokki.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxTokki.TextMaxLength = 2;
            this.verifyBoxTokki.VerifyLocationVertical = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(10, 73);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(31, 26);
            this.label35.TabIndex = 9;
            this.label35.Text = "明細\r\n区分";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(116, 685);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 77;
            this.label6.Text = "日";
            // 
            // verifyBoxAgain
            // 
            this.verifyBoxAgain.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxAgain.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxAgain.IsVerifying = false;
            this.verifyBoxAgain.Location = new System.Drawing.Point(288, 711);
            this.verifyBoxAgain.MaxLength = 1;
            this.verifyBoxAgain.Name = "verifyBoxAgain";
            this.verifyBoxAgain.Required = false;
            this.verifyBoxAgain.Size = new System.Drawing.Size(27, 26);
            this.verifyBoxAgain.TabIndex = 86;
            this.verifyBoxAgain.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxAgain.TextMaxLength = 1;
            this.verifyBoxAgain.VerifyLocationVertical = true;
            // 
            // verifyBoxSyokujiKouhi2HutanPrice
            // 
            this.verifyBoxSyokujiKouhi2HutanPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi2HutanPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi2HutanPrice.IsVerifying = false;
            this.verifyBoxSyokujiKouhi2HutanPrice.Location = new System.Drawing.Point(249, 670);
            this.verifyBoxSyokujiKouhi2HutanPrice.MaxLength = 8;
            this.verifyBoxSyokujiKouhi2HutanPrice.Name = "verifyBoxSyokujiKouhi2HutanPrice";
            this.verifyBoxSyokujiKouhi2HutanPrice.Required = false;
            this.verifyBoxSyokujiKouhi2HutanPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiKouhi2HutanPrice.TabIndex = 80;
            this.verifyBoxSyokujiKouhi2HutanPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi2HutanPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiKouhi2HutanPrice.VerifyLocationVertical = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label38.Location = new System.Drawing.Point(240, 140);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(31, 13);
            this.label38.TabIndex = 19;
            this.label38.Text = "有：1";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(224, 465);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(19, 13);
            this.label53.TabIndex = 55;
            this.label53.Text = "点";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(176, 132);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(31, 26);
            this.label37.TabIndex = 17;
            this.label37.Text = "○通\r\n有無";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // verifyBoxSyokujiKouhi2MediPrice
            // 
            this.verifyBoxSyokujiKouhi2MediPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi2MediPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi2MediPrice.IsVerifying = false;
            this.verifyBoxSyokujiKouhi2MediPrice.Location = new System.Drawing.Point(140, 670);
            this.verifyBoxSyokujiKouhi2MediPrice.MaxLength = 8;
            this.verifyBoxSyokujiKouhi2MediPrice.Name = "verifyBoxSyokujiKouhi2MediPrice";
            this.verifyBoxSyokujiKouhi2MediPrice.Required = false;
            this.verifyBoxSyokujiKouhi2MediPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiKouhi2MediPrice.TabIndex = 78;
            this.verifyBoxSyokujiKouhi2MediPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi2MediPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiKouhi2MediPrice.VerifyLocationVertical = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(166, 726);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 13);
            this.label25.TabIndex = 84;
            this.label25.Text = "円";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(116, 465);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(19, 13);
            this.label54.TabIndex = 53;
            this.label54.Text = "日";
            // 
            // verifyBoxSyokujiKouhi2Days
            // 
            this.verifyBoxSyokujiKouhi2Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi2Days.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi2Days.IsVerifying = false;
            this.verifyBoxSyokujiKouhi2Days.Location = new System.Drawing.Point(82, 670);
            this.verifyBoxSyokujiKouhi2Days.MaxLength = 2;
            this.verifyBoxSyokujiKouhi2Days.Name = "verifyBoxSyokujiKouhi2Days";
            this.verifyBoxSyokujiKouhi2Days.Required = false;
            this.verifyBoxSyokujiKouhi2Days.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxSyokujiKouhi2Days.TabIndex = 76;
            this.verifyBoxSyokujiKouhi2Days.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi2Days.TextMaxLength = 2;
            this.verifyBoxSyokujiKouhi2Days.VerifyLocationVertical = true;
            // 
            // verifyBoxBirthM
            // 
            this.verifyBoxBirthM.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthM.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxBirthM.IsVerifying = false;
            this.verifyBoxBirthM.Location = new System.Drawing.Point(231, 248);
            this.verifyBoxBirthM.MaxLength = 2;
            this.verifyBoxBirthM.Name = "verifyBoxBirthM";
            this.verifyBoxBirthM.Required = false;
            this.verifyBoxBirthM.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxBirthM.TabIndex = 30;
            this.verifyBoxBirthM.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBirthM.TextMaxLength = 2;
            this.verifyBoxBirthM.VerifyLocationVertical = true;
            // 
            // verifyBoxMediKouhi2Price
            // 
            this.verifyBoxMediKouhi2Price.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi2Price.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi2Price.IsVerifying = false;
            this.verifyBoxMediKouhi2Price.Location = new System.Drawing.Point(249, 450);
            this.verifyBoxMediKouhi2Price.MaxLength = 8;
            this.verifyBoxMediKouhi2Price.Name = "verifyBoxMediKouhi2Price";
            this.verifyBoxMediKouhi2Price.Required = false;
            this.verifyBoxMediKouhi2Price.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediKouhi2Price.TabIndex = 56;
            this.verifyBoxMediKouhi2Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi2Price.TextMaxLength = 8;
            this.verifyBoxMediKouhi2Price.VerifyLocationVertical = true;
            // 
            // verifyBoxBirthY
            // 
            this.verifyBoxBirthY.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthY.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxBirthY.IsVerifying = false;
            this.verifyBoxBirthY.Location = new System.Drawing.Point(186, 248);
            this.verifyBoxBirthY.MaxLength = 2;
            this.verifyBoxBirthY.Name = "verifyBoxBirthY";
            this.verifyBoxBirthY.Required = false;
            this.verifyBoxBirthY.Size = new System.Drawing.Size(26, 26);
            this.verifyBoxBirthY.TabIndex = 28;
            this.verifyBoxBirthY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBirthY.TextMaxLength = 2;
            this.verifyBoxBirthY.VerifyLocationVertical = true;
            // 
            // verifyBoxKougaku
            // 
            this.verifyBoxKougaku.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxKougaku.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxKougaku.IsVerifying = false;
            this.verifyBoxKougaku.Location = new System.Drawing.Point(82, 711);
            this.verifyBoxKougaku.MaxLength = 8;
            this.verifyBoxKougaku.Name = "verifyBoxKougaku";
            this.verifyBoxKougaku.Required = false;
            this.verifyBoxKougaku.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxKougaku.TabIndex = 83;
            this.verifyBoxKougaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxKougaku.TextMaxLength = 8;
            this.verifyBoxKougaku.VerifyLocationVertical = true;
            // 
            // verifyBoxBirthEra
            // 
            this.verifyBoxBirthEra.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxBirthEra.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxBirthEra.IsVerifying = false;
            this.verifyBoxBirthEra.Location = new System.Drawing.Point(130, 248);
            this.verifyBoxBirthEra.MaxLength = 1;
            this.verifyBoxBirthEra.Name = "verifyBoxBirthEra";
            this.verifyBoxBirthEra.Required = false;
            this.verifyBoxBirthEra.Size = new System.Drawing.Size(21, 26);
            this.verifyBoxBirthEra.TabIndex = 27;
            this.verifyBoxBirthEra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxBirthEra.TextMaxLength = 1;
            this.verifyBoxBirthEra.VerifyLocationVertical = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 679);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 75;
            this.label14.Text = "食事公費②";
            // 
            // verifyBoxSex
            // 
            this.verifyBoxSex.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSex.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSex.IsVerifying = false;
            this.verifyBoxSex.Location = new System.Drawing.Point(41, 248);
            this.verifyBoxSex.MaxLength = 1;
            this.verifyBoxSex.Name = "verifyBoxSex";
            this.verifyBoxSex.Required = false;
            this.verifyBoxSex.Size = new System.Drawing.Size(21, 26);
            this.verifyBoxSex.TabIndex = 25;
            this.verifyBoxSex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSex.TextMaxLength = 1;
            this.verifyBoxSex.VerifyLocationVertical = true;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(12, 720);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 13);
            this.label56.TabIndex = 82;
            this.label56.Text = "高額療養費";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(201, 720);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(85, 13);
            this.label59.TabIndex = 85;
            this.label59.Text = "再審査・再入力";
            // 
            // verifyBoxMediKouhi2Point
            // 
            this.verifyBoxMediKouhi2Point.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi2Point.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi2Point.IsVerifying = false;
            this.verifyBoxMediKouhi2Point.Location = new System.Drawing.Point(140, 450);
            this.verifyBoxMediKouhi2Point.MaxLength = 8;
            this.verifyBoxMediKouhi2Point.Name = "verifyBoxMediKouhi2Point";
            this.verifyBoxMediKouhi2Point.Required = false;
            this.verifyBoxMediKouhi2Point.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediKouhi2Point.TabIndex = 54;
            this.verifyBoxMediKouhi2Point.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi2Point.TextMaxLength = 8;
            this.verifyBoxMediKouhi2Point.VerifyLocationVertical = true;
            // 
            // verifyBoxMediKouhi2Days
            // 
            this.verifyBoxMediKouhi2Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi2Days.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi2Days.IsVerifying = false;
            this.verifyBoxMediKouhi2Days.Location = new System.Drawing.Point(82, 450);
            this.verifyBoxMediKouhi2Days.MaxLength = 2;
            this.verifyBoxMediKouhi2Days.Name = "verifyBoxMediKouhi2Days";
            this.verifyBoxMediKouhi2Days.Required = false;
            this.verifyBoxMediKouhi2Days.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxMediKouhi2Days.TabIndex = 52;
            this.verifyBoxMediKouhi2Days.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi2Days.TextMaxLength = 2;
            this.verifyBoxMediKouhi2Days.VerifyLocationVertical = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(334, 616);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 74;
            this.label7.Text = "円";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(334, 405);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(19, 13);
            this.label49.TabIndex = 50;
            this.label49.Text = "円";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(224, 616);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 13);
            this.label9.TabIndex = 72;
            this.label9.Text = "円";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(224, 405);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(19, 13);
            this.label50.TabIndex = 48;
            this.label50.Text = "点";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(116, 616);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 70;
            this.label10.Text = "日";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(116, 405);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(19, 13);
            this.label51.TabIndex = 46;
            this.label51.Text = "日";
            // 
            // verifyBoxSyokujiKouhi1HutanPrice
            // 
            this.verifyBoxSyokujiKouhi1HutanPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi1HutanPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi1HutanPrice.IsVerifying = false;
            this.verifyBoxSyokujiKouhi1HutanPrice.Location = new System.Drawing.Point(249, 601);
            this.verifyBoxSyokujiKouhi1HutanPrice.MaxLength = 8;
            this.verifyBoxSyokujiKouhi1HutanPrice.Name = "verifyBoxSyokujiKouhi1HutanPrice";
            this.verifyBoxSyokujiKouhi1HutanPrice.Required = false;
            this.verifyBoxSyokujiKouhi1HutanPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiKouhi1HutanPrice.TabIndex = 73;
            this.verifyBoxSyokujiKouhi1HutanPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi1HutanPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiKouhi1HutanPrice.VerifyLocationVertical = true;
            // 
            // verifyBoxMediKouhi1Price
            // 
            this.verifyBoxMediKouhi1Price.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi1Price.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi1Price.IsVerifying = false;
            this.verifyBoxMediKouhi1Price.Location = new System.Drawing.Point(249, 390);
            this.verifyBoxMediKouhi1Price.MaxLength = 8;
            this.verifyBoxMediKouhi1Price.Name = "verifyBoxMediKouhi1Price";
            this.verifyBoxMediKouhi1Price.Required = false;
            this.verifyBoxMediKouhi1Price.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediKouhi1Price.TabIndex = 49;
            this.verifyBoxMediKouhi1Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi1Price.TextMaxLength = 8;
            this.verifyBoxMediKouhi1Price.VerifyLocationVertical = true;
            // 
            // verifyBoxSyokujiKouhi1MediPrice
            // 
            this.verifyBoxSyokujiKouhi1MediPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi1MediPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi1MediPrice.IsVerifying = false;
            this.verifyBoxSyokujiKouhi1MediPrice.Location = new System.Drawing.Point(140, 601);
            this.verifyBoxSyokujiKouhi1MediPrice.MaxLength = 8;
            this.verifyBoxSyokujiKouhi1MediPrice.Name = "verifyBoxSyokujiKouhi1MediPrice";
            this.verifyBoxSyokujiKouhi1MediPrice.Required = false;
            this.verifyBoxSyokujiKouhi1MediPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiKouhi1MediPrice.TabIndex = 71;
            this.verifyBoxSyokujiKouhi1MediPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi1MediPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiKouhi1MediPrice.VerifyLocationVertical = true;
            // 
            // verifyBoxMediKouhi1Point
            // 
            this.verifyBoxMediKouhi1Point.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi1Point.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi1Point.IsVerifying = false;
            this.verifyBoxMediKouhi1Point.Location = new System.Drawing.Point(140, 390);
            this.verifyBoxMediKouhi1Point.MaxLength = 8;
            this.verifyBoxMediKouhi1Point.Name = "verifyBoxMediKouhi1Point";
            this.verifyBoxMediKouhi1Point.Required = false;
            this.verifyBoxMediKouhi1Point.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediKouhi1Point.TabIndex = 47;
            this.verifyBoxMediKouhi1Point.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi1Point.TextMaxLength = 8;
            this.verifyBoxMediKouhi1Point.VerifyLocationVertical = true;
            // 
            // verifyBoxSyokujiKouhi1Days
            // 
            this.verifyBoxSyokujiKouhi1Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiKouhi1Days.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiKouhi1Days.IsVerifying = false;
            this.verifyBoxSyokujiKouhi1Days.Location = new System.Drawing.Point(82, 601);
            this.verifyBoxSyokujiKouhi1Days.MaxLength = 2;
            this.verifyBoxSyokujiKouhi1Days.Name = "verifyBoxSyokujiKouhi1Days";
            this.verifyBoxSyokujiKouhi1Days.Required = false;
            this.verifyBoxSyokujiKouhi1Days.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxSyokujiKouhi1Days.TabIndex = 69;
            this.verifyBoxSyokujiKouhi1Days.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiKouhi1Days.TextMaxLength = 2;
            this.verifyBoxSyokujiKouhi1Days.VerifyLocationVertical = true;
            // 
            // labelSex
            // 
            this.labelSex.AutoSize = true;
            this.labelSex.Location = new System.Drawing.Point(10, 258);
            this.labelSex.Name = "labelSex";
            this.labelSex.Size = new System.Drawing.Size(31, 13);
            this.labelSex.TabIndex = 24;
            this.labelSex.Text = "性別";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(65, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 26);
            this.label4.TabIndex = 26;
            this.label4.Text = "男：1\r\n女：2";
            // 
            // verifyBoxMediKouhi1Days
            // 
            this.verifyBoxMediKouhi1Days.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediKouhi1Days.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediKouhi1Days.IsVerifying = false;
            this.verifyBoxMediKouhi1Days.Location = new System.Drawing.Point(82, 390);
            this.verifyBoxMediKouhi1Days.MaxLength = 2;
            this.verifyBoxMediKouhi1Days.Name = "verifyBoxMediKouhi1Days";
            this.verifyBoxMediKouhi1Days.Required = false;
            this.verifyBoxMediKouhi1Days.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxMediKouhi1Days.TabIndex = 45;
            this.verifyBoxMediKouhi1Days.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediKouhi1Days.TextMaxLength = 2;
            this.verifyBoxMediKouhi1Days.VerifyLocationVertical = true;
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Location = new System.Drawing.Point(101, 250);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(31, 26);
            this.labelBirthday.TabIndex = 31;
            this.labelBirthday.Text = "生年\r\n月日";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(334, 557);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 67;
            this.label11.Text = "円";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label5.Location = new System.Drawing.Point(154, 243);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 39);
            this.label5.TabIndex = 33;
            this.label5.Text = "昭：3\r\n平：4\r\n令：5";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(334, 346);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(19, 13);
            this.label48.TabIndex = 43;
            this.label48.Text = "円";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(257, 263);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 13);
            this.label17.TabIndex = 31;
            this.label17.Text = "月";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(224, 557);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 13);
            this.label12.TabIndex = 65;
            this.label12.Text = "円";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(213, 263);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 13);
            this.label18.TabIndex = 29;
            this.label18.Text = "年";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(224, 346);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(19, 13);
            this.label47.TabIndex = 41;
            this.label47.Text = "点";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(116, 557);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 63;
            this.label13.Text = "日";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(116, 346);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(19, 13);
            this.label46.TabIndex = 39;
            this.label46.Text = "日";
            // 
            // verifyBoxSyokujiHutanPrice
            // 
            this.verifyBoxSyokujiHutanPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiHutanPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiHutanPrice.IsVerifying = false;
            this.verifyBoxSyokujiHutanPrice.Location = new System.Drawing.Point(249, 542);
            this.verifyBoxSyokujiHutanPrice.MaxLength = 8;
            this.verifyBoxSyokujiHutanPrice.Name = "verifyBoxSyokujiHutanPrice";
            this.verifyBoxSyokujiHutanPrice.Required = false;
            this.verifyBoxSyokujiHutanPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiHutanPrice.TabIndex = 66;
            this.verifyBoxSyokujiHutanPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiHutanPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiHutanPrice.VerifyLocationVertical = true;
            // 
            // verifyBoxMediPrice
            // 
            this.verifyBoxMediPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediPrice.IsVerifying = false;
            this.verifyBoxMediPrice.Location = new System.Drawing.Point(249, 330);
            this.verifyBoxMediPrice.MaxLength = 8;
            this.verifyBoxMediPrice.Name = "verifyBoxMediPrice";
            this.verifyBoxMediPrice.Required = false;
            this.verifyBoxMediPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediPrice.TabIndex = 42;
            this.verifyBoxMediPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediPrice.TextMaxLength = 8;
            this.verifyBoxMediPrice.VerifyLocationVertical = true;
            // 
            // verifyBoxSyokujiMediPrice
            // 
            this.verifyBoxSyokujiMediPrice.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiMediPrice.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiMediPrice.IsVerifying = false;
            this.verifyBoxSyokujiMediPrice.Location = new System.Drawing.Point(140, 542);
            this.verifyBoxSyokujiMediPrice.MaxLength = 8;
            this.verifyBoxSyokujiMediPrice.Name = "verifyBoxSyokujiMediPrice";
            this.verifyBoxSyokujiMediPrice.Required = false;
            this.verifyBoxSyokujiMediPrice.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxSyokujiMediPrice.TabIndex = 64;
            this.verifyBoxSyokujiMediPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiMediPrice.TextMaxLength = 8;
            this.verifyBoxSyokujiMediPrice.VerifyLocationVertical = true;
            // 
            // verifyBoxMediPoint
            // 
            this.verifyBoxMediPoint.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediPoint.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediPoint.IsVerifying = false;
            this.verifyBoxMediPoint.Location = new System.Drawing.Point(140, 330);
            this.verifyBoxMediPoint.MaxLength = 8;
            this.verifyBoxMediPoint.Name = "verifyBoxMediPoint";
            this.verifyBoxMediPoint.Required = false;
            this.verifyBoxMediPoint.Size = new System.Drawing.Size(83, 26);
            this.verifyBoxMediPoint.TabIndex = 40;
            this.verifyBoxMediPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediPoint.TextMaxLength = 8;
            this.verifyBoxMediPoint.VerifyLocationVertical = true;
            // 
            // verifyBoxSyokujiDays
            // 
            this.verifyBoxSyokujiDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxSyokujiDays.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxSyokujiDays.IsVerifying = false;
            this.verifyBoxSyokujiDays.Location = new System.Drawing.Point(82, 542);
            this.verifyBoxSyokujiDays.MaxLength = 2;
            this.verifyBoxSyokujiDays.Name = "verifyBoxSyokujiDays";
            this.verifyBoxSyokujiDays.Required = false;
            this.verifyBoxSyokujiDays.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxSyokujiDays.TabIndex = 62;
            this.verifyBoxSyokujiDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxSyokujiDays.TextMaxLength = 2;
            this.verifyBoxSyokujiDays.VerifyLocationVertical = true;
            // 
            // verifyBoxMediDays
            // 
            this.verifyBoxMediDays.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxMediDays.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxMediDays.IsVerifying = false;
            this.verifyBoxMediDays.Location = new System.Drawing.Point(82, 330);
            this.verifyBoxMediDays.MaxLength = 2;
            this.verifyBoxMediDays.Name = "verifyBoxMediDays";
            this.verifyBoxMediDays.Required = false;
            this.verifyBoxMediDays.Size = new System.Drawing.Size(32, 26);
            this.verifyBoxMediDays.TabIndex = 38;
            this.verifyBoxMediDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxMediDays.TextMaxLength = 2;
            this.verifyBoxMediDays.VerifyLocationVertical = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(12, 611);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 13);
            this.label19.TabIndex = 68;
            this.label19.Text = "食事公費①";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(12, 459);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 13);
            this.label45.TabIndex = 51;
            this.label45.Text = "療養公費②";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(12, 551);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 61;
            this.label20.Text = "食事保険";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 400);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 13);
            this.label44.TabIndex = 44;
            this.label44.Text = "療養公費①";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(247, 521);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 13);
            this.label21.TabIndex = 60;
            this.label21.Text = "負担金額";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 340);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(55, 13);
            this.label43.TabIndex = 37;
            this.label43.Text = "療養保険";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(138, 521);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 59;
            this.label22.Text = "療養金額";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(247, 312);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 36;
            this.label42.Text = "負担金額";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(80, 521);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 13);
            this.label23.TabIndex = 58;
            this.label23.Text = "日数";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(138, 312);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 35;
            this.label41.Text = "請求点数";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(80, 312);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 13);
            this.label40.TabIndex = 34;
            this.label40.Text = "日数";
            // 
            // verifyBoxNumbering
            // 
            this.verifyBoxNumbering.BackColor = System.Drawing.SystemColors.Info;
            this.verifyBoxNumbering.Font = new System.Drawing.Font("Meiryo UI", 11F);
            this.verifyBoxNumbering.IsVerifying = false;
            this.verifyBoxNumbering.Location = new System.Drawing.Point(256, 12);
            this.verifyBoxNumbering.MaxLength = 6;
            this.verifyBoxNumbering.Name = "verifyBoxNumbering";
            this.verifyBoxNumbering.Required = false;
            this.verifyBoxNumbering.Size = new System.Drawing.Size(63, 26);
            this.verifyBoxNumbering.TabIndex = 3;
            this.verifyBoxNumbering.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.verifyBoxNumbering.TextMaxLength = 6;
            this.verifyBoxNumbering.TextMode = meps.Controls.VerifyBox.TEXT_MODE.NUM;
            this.verifyBoxNumbering.VerifyLocationVertical = true;
            // 
            // labelNumberingOrBatch
            // 
            this.labelNumberingOrBatch.AutoSize = true;
            this.labelNumberingOrBatch.Location = new System.Drawing.Point(188, 22);
            this.labelNumberingOrBatch.Name = "labelNumberingOrBatch";
            this.labelNumberingOrBatch.Size = new System.Drawing.Size(61, 13);
            this.labelNumberingOrBatch.TabIndex = 2;
            this.labelNumberingOrBatch.Text = "ナンバリング";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonBack);
            this.panel2.Controls.Add(this.buttonExit);
            this.panel2.Controls.Add(this.buttonRegist);
            this.panel2.Controls.Add(this.labelInputerName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.panel2.Location = new System.Drawing.Point(0, 771);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(421, 30);
            this.panel2.TabIndex = 0;
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(229, 3);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(111, 25);
            this.buttonBack.TabIndex = 2;
            this.buttonBack.Text = "戻る (PageDown)";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // labelInputerName
            // 
            this.labelInputerName.Location = new System.Drawing.Point(8, 9);
            this.labelInputerName.Name = "labelInputerName";
            this.labelInputerName.Size = new System.Drawing.Size(203, 17);
            this.labelInputerName.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.splitter1.Location = new System.Drawing.Point(245, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 801);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // panelDatalist
            // 
            this.panelDatalist.Controls.Add(this.dataGridViewPlist);
            this.panelDatalist.Controls.Add(this.panelUP);
            this.panelDatalist.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelDatalist.Location = new System.Drawing.Point(0, 0);
            this.panelDatalist.Name = "panelDatalist";
            this.panelDatalist.Size = new System.Drawing.Size(245, 801);
            this.panelDatalist.TabIndex = 0;
            // 
            // dataGridViewPlist
            // 
            this.dataGridViewPlist.AllowUserToAddRows = false;
            this.dataGridViewPlist.AllowUserToDeleteRows = false;
            this.dataGridViewPlist.AllowUserToResizeRows = false;
            this.dataGridViewPlist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewPlist.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewPlist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPlist.Location = new System.Drawing.Point(0, 64);
            this.dataGridViewPlist.MultiSelect = false;
            this.dataGridViewPlist.Name = "dataGridViewPlist";
            this.dataGridViewPlist.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewPlist.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewPlist.RowHeadersVisible = false;
            this.dataGridViewPlist.RowTemplate.Height = 21;
            this.dataGridViewPlist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPlist.Size = new System.Drawing.Size(245, 737);
            this.dataGridViewPlist.TabIndex = 0;
            // 
            // panelUP
            // 
            this.panelUP.BackColor = System.Drawing.Color.MintCream;
            this.panelUP.Controls.Add(this.label26);
            this.panelUP.Controls.Add(this.labelPref);
            this.panelUP.Controls.Add(this.labelSeikyu);
            this.panelUP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUP.Location = new System.Drawing.Point(0, 0);
            this.panelUP.Name = "panelUP";
            this.panelUP.Size = new System.Drawing.Size(245, 64);
            this.panelUP.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 10);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "都道府県：";
            // 
            // labelPref
            // 
            this.labelPref.AutoSize = true;
            this.labelPref.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelPref.Location = new System.Drawing.Point(73, 8);
            this.labelPref.Name = "labelPref";
            this.labelPref.Size = new System.Drawing.Size(93, 16);
            this.labelPref.TabIndex = 0;
            this.labelPref.Text = "都道府県名";
            // 
            // labelSeikyu
            // 
            this.labelSeikyu.AutoSize = true;
            this.labelSeikyu.Location = new System.Drawing.Point(3, 37);
            this.labelSeikyu.Name = "labelSeikyu";
            this.labelSeikyu.Size = new System.Drawing.Size(43, 13);
            this.labelSeikyu.TabIndex = 5;
            this.labelSeikyu.Text = "請求月";
            // 
            // buttonImageChange
            // 
            this.buttonImageChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageChange.Location = new System.Drawing.Point(328, 770);
            this.buttonImageChange.Name = "buttonImageChange";
            this.buttonImageChange.Size = new System.Drawing.Size(40, 25);
            this.buttonImageChange.TabIndex = 61;
            this.buttonImageChange.Text = "差替";
            this.buttonImageChange.UseVisualStyleBackColor = true;
            this.buttonImageChange.Click += new System.EventHandler(this.buttonImageChange_Click);
            // 
            // buttonImageRotateL
            // 
            this.buttonImageRotateL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateL.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateL.Location = new System.Drawing.Point(256, 770);
            this.buttonImageRotateL.Name = "buttonImageRotateL";
            this.buttonImageRotateL.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateL.TabIndex = 60;
            this.buttonImageRotateL.Text = "↺";
            this.buttonImageRotateL.UseVisualStyleBackColor = true;
            this.buttonImageRotateL.Click += new System.EventHandler(this.buttonImageRotateL_Click);
            // 
            // buttonImageRotateR
            // 
            this.buttonImageRotateR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonImageRotateR.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImageRotateR.Location = new System.Drawing.Point(292, 770);
            this.buttonImageRotateR.Name = "buttonImageRotateR";
            this.buttonImageRotateR.Size = new System.Drawing.Size(35, 25);
            this.buttonImageRotateR.TabIndex = 59;
            this.buttonImageRotateR.Text = "↻";
            this.buttonImageRotateR.UseVisualStyleBackColor = true;
            this.buttonImageRotateR.Click += new System.EventHandler(this.buttonImageRotateR_Click);
            // 
            // buttonImageFill
            // 
            this.buttonImageFill.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImageFill.Location = new System.Drawing.Point(813, 770);
            this.buttonImageFill.Name = "buttonImageFill";
            this.buttonImageFill.Size = new System.Drawing.Size(75, 25);
            this.buttonImageFill.TabIndex = 58;
            this.buttonImageFill.Text = "全体表示";
            this.buttonImageFill.UseVisualStyleBackColor = true;
            this.buttonImageFill.Click += new System.EventHandler(this.buttonImageFill_Click);
            // 
            // scrollPictureControl1
            // 
            this.scrollPictureControl1.AutoScroll = true;
            this.scrollPictureControl1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.scrollPictureControl1.ButtonsVisible = false;
            this.scrollPictureControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scrollPictureControl1.Location = new System.Drawing.Point(248, 0);
            this.scrollPictureControl1.MinimumSize = new System.Drawing.Size(200, 137);
            this.scrollPictureControl1.Name = "scrollPictureControl1";
            this.scrollPictureControl1.Ratio = 1F;
            this.scrollPictureControl1.ScrollPosition = new System.Drawing.Point(0, 0);
            this.scrollPictureControl1.Size = new System.Drawing.Size(675, 801);
            this.scrollPictureControl1.TabIndex = 57;
            // 
            // RezeptInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1344, 801);
            this.Controls.Add(this.buttonImageChange);
            this.Controls.Add(this.buttonImageRotateL);
            this.Controls.Add(this.buttonImageRotateR);
            this.Controls.Add(this.buttonImageFill);
            this.Controls.Add(this.scrollPictureControl1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.panelDatalist);
            this.KeyPreview = true;
            this.Name = "RezeptInputForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MEPS -入力画面-";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.RezeptInputForm_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormOCRCheck_KeyUp);
            this.panelRight.ResumeLayout(false);
            this.panelControlScroll.ResumeLayout(false);
            this.panelControls.ResumeLayout(false);
            this.panelControls.PerformLayout();
            this.panelInput.ResumeLayout(false);
            this.panelInput.PerformLayout();
            this.groupBoxDRG.ResumeLayout(false);
            this.groupBoxDRG.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panelDatalist.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlist)).EndInit();
            this.panelUP.ResumeLayout(false);
            this.panelUP.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonRegist;
        private System.Windows.Forms.Label labelHnum;
        private System.Windows.Forms.Label labelM;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label labelSex;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelInputerName;
        private System.Windows.Forms.Panel panelDatalist;
        private System.Windows.Forms.DataGridView dataGridViewPlist;
        private System.Windows.Forms.Panel panelUP;
        private System.Windows.Forms.Label labelPref;
        private System.Windows.Forms.Label labelSeikyu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panelControlScroll;
        private System.Windows.Forms.Panel panelControls;
        private ScrollPictureControl scrollPictureControl1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label labelNumberingOrBatch;
        private VerifyBox verifyBoxNumbering;
        private VerifyBox verifyBoxY;
        private VerifyBox verifyBoxM;
        private VerifyBox verifyBoxHospital;
        private VerifyBox verifyBoxPref;
        private VerifyBox verifyBoxHnum;
        private VerifyBox verifyBoxHonninKazoku;
        private VerifyBox verifyBoxMeisai;
        private VerifyBox verifyBoxTuuchi;
        private VerifyBox verifyBoxSex;
        private VerifyBox verifyBoxBirthM;
        private VerifyBox verifyBoxBirthY;
        private VerifyBox verifyBoxBirthEra;
        private System.Windows.Forms.Label label39;
        private VerifyBox verifyBoxKouhi2Code;
        private System.Windows.Forms.Label label15;
        private VerifyBox verifyBoxKouhi1Code;
        private System.Windows.Forms.Panel panelInput;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private VerifyBox verifyBoxMediKouhi2Price;
        private VerifyBox verifyBoxMediKouhi2Point;
        private VerifyBox verifyBoxMediKouhi2Days;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private VerifyBox verifyBoxMediKouhi1Price;
        private VerifyBox verifyBoxMediKouhi1Point;
        private VerifyBox verifyBoxMediKouhi1Days;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private VerifyBox verifyBoxMediPrice;
        private VerifyBox verifyBoxMediPoint;
        private VerifyBox verifyBoxMediDays;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private VerifyBox verifyBoxAgain;
        private System.Windows.Forms.Label label25;
        private VerifyBox verifyBoxKougaku;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private VerifyBox verifyBoxSyokujiKouhi2HutanPrice;
        private VerifyBox verifyBoxSyokujiKouhi2MediPrice;
        private VerifyBox verifyBoxSyokujiKouhi2Days;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private VerifyBox verifyBoxSyokujiKouhi1HutanPrice;
        private VerifyBox verifyBoxSyokujiKouhi1MediPrice;
        private VerifyBox verifyBoxSyokujiKouhi1Days;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private VerifyBox verifyBoxSyokujiHutanPrice;
        private VerifyBox verifyBoxSyokujiMediPrice;
        private VerifyBox verifyBoxSyokujiDays;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label16;
        private VerifyBox verifyBoxTokki;
        private System.Windows.Forms.Label label26;
        private VerifyBox verifyBoxInum;
        private System.Windows.Forms.Label label27;
        private VerifyBox verifyBoxMediAppType;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Label label29;
        private VerifyBox verifyBoxBirthD;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button buttonImageChange;
        private System.Windows.Forms.Button buttonImageRotateL;
        private System.Windows.Forms.Button buttonImageRotateR;
        private System.Windows.Forms.Button buttonImageFill;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBoxDRG;
        private VerifyBox verifyBoxDrgType;
        private System.Windows.Forms.Label label32;
        private VerifyBox verifyBoxDrgMediDate2D;
        private System.Windows.Forms.Label label61;
        private VerifyBox verifyBoxDrgMediDate2M;
        private VerifyBox verifyBoxDrgMediDate2Y;
        private VerifyBox verifyBoxDrgMediDate2G;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private VerifyBox verifyBoxDrgMediDate1D;
        private System.Windows.Forms.Label label34;
        private VerifyBox verifyBoxDrgMediDate1M;
        private VerifyBox verifyBoxDrgMediDate1Y;
        private VerifyBox verifyBoxDrgMediDate1G;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label66;
    }
}