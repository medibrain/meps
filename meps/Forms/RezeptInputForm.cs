﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using meps.Controls;
using meps.Table;
using meps.Utils;
using System.Collections.Generic;

namespace meps.Forms
{
    public partial class RezeptInputForm : Form
    {
        private const string MEDIAPP_TYPE_NULL = "";
        private const string MEDIAPP_TYPE_APP = "";
        private const string MEDIAPP_TYPE_BATCH = "--";
        private const string MEDIAPP_TYPE_HUSEN = "++";
        private const string MEDIAPP_TYPE_NEXT = "**";
        private const string MEDIAPP_TYPE_NONE = "//";
        private const string MEDIAPP_TYPE_HENREI = "##";
        private const int FOCUS_TOP = 1;
        private const int FOCUS_NUMBERING = 2;
        private const int FOCUS_REGIST = 3;
        private const int FOCUS_FIRST_ERROR = 4;

        BindingSource bs = new BindingSource();
        int pid;
        string prefname;
        int iym;
        int ijy;
        int ijm;
        bool verify;

        // --------------------------------------------------------------------------------
        //
        // クラス定義部
        //
        // --------------------------------------------------------------------------------

        //画像ファイルの座標＝下記指定座標 / 倍率(0-1)
        //＝指定座標×倍率（％）÷100
        Point headerPos = new Point(0, 50);
        Point personPos = new Point(0, 150);
        Point daysPos = new Point(0, 500);
        Point medicalPos = new Point(0, 600);
        Point footterPos = new Point(0, 2000);

        Control[] headerControls, personControls, daysControls, medicalControls, footerControls;

        // --------------------------------------------------------------------------------
        //
        // 初期化部
        //
        // --------------------------------------------------------------------------------
        public RezeptInputForm(int iym, int ijy, int ijm, int pid, string prefname, bool verify = false)
        {
            InitializeComponent();

            this.pid = pid;
            this.prefname = prefname;
            this.iym = iym;
            this.ijy = ijy;
            this.ijm = ijm;
            this.verify = verify;

            //表示位置の登録
            headerControls = new Control[] 
            {
                verifyBoxMediAppType, verifyBoxNumbering, verifyBoxY, verifyBoxM, verifyBoxPref, verifyBoxHospital,
                verifyBoxMeisai, verifyBoxHonninKazoku, verifyBoxInum, verifyBoxHnum, verifyBoxTuuchi, 
                verifyBoxKouhi1Code, verifyBoxKouhi2Code
            };
            personControls = new Control[] 
            {
                verifyBoxSex, verifyBoxBirthEra, verifyBoxBirthY, verifyBoxBirthM
            };
            daysControls = new Control[] 
            {
                verifyBoxMediDays, verifyBoxMediKouhi1Days, verifyBoxMediKouhi2Days
            };
            medicalControls = new Control[]
            {
                verifyBoxDrgType,
                verifyBoxDrgMediDate1G, verifyBoxDrgMediDate1Y, verifyBoxDrgMediDate1M, verifyBoxDrgMediDate1D,
                verifyBoxDrgMediDate2G, verifyBoxDrgMediDate2Y, verifyBoxDrgMediDate2M, verifyBoxDrgMediDate2D,
            };
            footerControls = new Control[]
            {
                verifyBoxMediPoint, verifyBoxMediPrice,
                verifyBoxMediKouhi1Point, verifyBoxMediKouhi1Price,
                verifyBoxMediKouhi2Point, verifyBoxMediKouhi2Price,
                verifyBoxSyokujiDays, verifyBoxSyokujiMediPrice, verifyBoxSyokujiHutanPrice,
                verifyBoxSyokujiKouhi1Days, verifyBoxSyokujiKouhi1MediPrice, verifyBoxSyokujiKouhi1HutanPrice,
                verifyBoxSyokujiKouhi2Days, verifyBoxSyokujiKouhi2MediPrice, verifyBoxSyokujiKouhi2HutanPrice,
                verifyBoxKougaku, verifyBoxAgain,
            };

            //イベントハンドラの登録
            Action<Control> func = null;
            func = new Action<Control>(c =>
            {
                foreach (Control item in c.Controls)
                {
                    if(item is VerifyBox)
                    {
                        ((VerifyBox)item).Enter += verifyBoxs_Enter;
                        //((VerifyBox)item).box.Enter += textBoxs_Enter;
                    }
                    func(item);
                }
            });
            func(panelControls);

            //審査年月＆都道府県で入力対象データ取得
            var list = MediApp.GetMediAppsByPIDIYM(iym, pid);

            //データバインド
            bs.CurrentChanged += Bs_CurrentChanged;
            bs.DataSource = list;
            dataGridViewPlist.DataSource = bs;

            foreach (DataGridViewColumn col in dataGridViewPlist.Columns) col.Visible = false;

            var icnt = 0;
            DataGridViewColumn dgvc;
            if (verify)
            {
                dgvc = dataGridViewPlist.Columns[nameof(MediApp.InputStatusView2)];
                dgvc.HeaderText = "状況";
                dgvc.DisplayIndex = icnt++;
                dgvc.Visible = true;
                dgvc.Width = 55;
            }
            else
            {
                dgvc = dataGridViewPlist.Columns[nameof(MediApp.InputStatusView1)];
                dgvc.HeaderText = "状況";
                dgvc.DisplayIndex = icnt++;
                dgvc.Visible = true;
                dgvc.Width = 55;
            }
            dgvc = dataGridViewPlist.Columns[nameof(MediApp.MediAppTypeView)];
            dgvc.HeaderText = "種類";
            dgvc.DisplayIndex = icnt++;
            dgvc.Visible = true;
            dgvc.Width = 70;
            dgvc = dataGridViewPlist.Columns[nameof(MediApp.NumberingView)];
            dgvc.HeaderText = "ﾅﾝﾊﾞ";
            dgvc.DisplayIndex = icnt++;
            dgvc.Visible = true;
            dgvc.Width = 45;
            dgvc = dataGridViewPlist.Columns[nameof(MediApp.HNum)];
            dgvc.HeaderText = "被保番";
            dgvc.DisplayIndex = icnt++;
            dgvc.Visible = true;
            dgvc.Width = 70;
            

            //左上の設定
            labelPref.Text = prefname;

            //20190724150326 furukawa st ////////////////////////
            //令和対応
            
            labelSeikyu.Text = $"平成/令和{ijy.ToString()}年 {ijm.ToString()}月 請求分";
            //labelSeikyu.Text = $"平成{ijy.ToString()}年 {ijm.ToString()}月 請求分";
            //20190724150326 furukawa ed ////////////////////////


            if (verify) this.Text += "（ベリファイ入力モード）";

            //最大最小値の設定
            verifyBoxIntValueInit();

            //大阪支部だけ特別
            if (pid == 27)
            {
                groupBoxDRG.Visible = true;
            }

            //20190725112000 furukawa st ////////////////////////
            //試験サーバの場合色変える            
            CommonTool.setFormColor(this);
            //20190725112000 furukawa ed ////////////////////////
        }

        private void verifyBoxIntValueInit()
        {
            verifyBoxNumbering.Required = true;
            verifyBoxNumbering.SetErrorAndWarningRange(-1, -1, 1, -1);
            verifyBoxY.Required = true;

            //20190724145118 furukawa st ////////////////////////
            //引数変更により月も入れる
            
            verifyBoxY.SetErrorAndWarningRange(-1, -1, 1, -1);
            //verifyBoxY.SetErrorAndWarningRange(-1, -1, 1, DateTimeEx.GetHsYearFromAd(DateTime.Now.Year));
            //20190724145118 furukawa ed ////////////////////////


            verifyBoxM.Required = true;
            verifyBoxM.SetErrorAndWarningRange(-1, -1, 1, 12);

            verifyBoxPref.Required = true;
            verifyBoxPref.SetErrorAndWarningRange(-1, -1, 1, Pref.GetMaxValue());

            verifyBoxHospital.Required = true;
            verifyBoxHospital.SetErrorAndWarningRange(-1, -1, 0, -1);

            verifyBoxMeisai.SetErrorAndWarningRange(-1, -1, 1, 6);
            verifyBoxHonninKazoku.SetErrorAndWarningRange(-1, -1, 0, 9);
            verifyBoxInum.Required = true;
            verifyBoxInum.SetErrorAndWarningRange(-1, -1, 1, -1);
            verifyBoxHnum.Required = true;
            verifyBoxHnum.SetErrorAndWarningLength(-1, -1, 1, 10);

            verifyBoxTuuchi.SetErrorAndWarningRange(-1, -1, 0, 1);
            verifyBoxKouhi1Code.SetErrorAndWarningRange(-1, -1, 1, -1);
            verifyBoxKouhi2Code.SetErrorAndWarningRange(-1, -1, 1, -1);

            verifyBoxSex.Required = true;
            verifyBoxSex.SetErrorAndWarningRange(-1, -1, 1, 2);

            verifyBoxBirthEra.Required = true;

            //20190724150510 furukawa st ////////////////////////
            //令和対応
            
            verifyBoxBirthEra.SetErrorAndWarningRange(-1, -1, 1, 5);
            //verifyBoxBirthEra.SetErrorAndWarningRange(-1, -1, 1, 4);//平成(4)が最大
            //20190724150510 furukawa ed ////////////////////////


            verifyBoxBirthY.Required = true;
            verifyBoxBirthY.SetErrorAndWarningRange(-1, -1, 1, 64);//昭和64年が最大
            verifyBoxBirthM.Required = true;
            verifyBoxBirthM.SetErrorAndWarningRange(-1, -1, 1, 12);
            verifyBoxBirthD.Required = true;
            verifyBoxBirthD.SetErrorAndWarningRange(-1, -1, 1, 31);            
            verifyBoxTokki.SetErrorAndWarningRange(-1, -1, Tokki.Min, Tokki.Max);

            verifyBoxMediDays.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxMediPoint.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxMediPrice.SetErrorAndWarningRange(-1, -1, 0, -1);

            verifyBoxMediKouhi1Days.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxMediKouhi1Point.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxMediKouhi1Price.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxMediKouhi2Days.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxMediKouhi2Point.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxMediKouhi2Price.SetErrorAndWarningRange(-1, -1, 0, -1);

            verifyBoxSyokujiDays.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxSyokujiMediPrice.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxSyokujiHutanPrice.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxSyokujiKouhi1Days.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxSyokujiKouhi1MediPrice.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxSyokujiKouhi1HutanPrice.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxSyokujiKouhi2Days.SetErrorAndWarningRange(-1, -1, 1, 31);
            verifyBoxSyokujiKouhi2MediPrice.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxSyokujiKouhi2HutanPrice.SetErrorAndWarningRange(-1, -1, 0, -1);

            verifyBoxKougaku.SetErrorAndWarningRange(-1, -1, 0, -1);
            verifyBoxAgain.SetErrorAndWarningRange(-1, -1, 0, 1);
            verifyBoxDrgType.SetErrorAndWarningRange(-1, -1, 0, 2);


            //20190724150839 furukawa st ////////////////////////
            //令和対応
            
            verifyBoxDrgMediDate1G.SetErrorAndWarningRange(-1, -1, 1, 5);
            //verifyBoxDrgMediDate1G.SetErrorAndWarningRange(-1, -1, 1, 4);//平成(4)が最大
            //20190724150839 furukawa ed ////////////////////////


            verifyBoxDrgMediDate1Y.SetErrorAndWarningRange(-1, -1, 1, 64);//昭和(64)が最大
            verifyBoxDrgMediDate1M.SetErrorAndWarningRange(-1, -1, 1, 12);
            verifyBoxDrgMediDate1D.SetErrorAndWarningRange(-1, -1, 1, 31);

            //20190724150904 furukawa st ////////////////////////
            //令和対応
            
            verifyBoxDrgMediDate2G.SetErrorAndWarningRange(-1, -1, 1, 5);
            //verifyBoxDrgMediDate2G.SetErrorAndWarningRange(-1, -1, 1, 4);//平成(4)が最大
            //20190724150904 furukawa ed ////////////////////////


            verifyBoxDrgMediDate2Y.SetErrorAndWarningRange(-1, -1, 1, 64);//昭和(64)が最大
            verifyBoxDrgMediDate2M.SetErrorAndWarningRange(-1, -1, 1, 12);
            verifyBoxDrgMediDate2D.SetErrorAndWarningRange(-1, -1, 1, 31);
        }


        // --------------------------------------------------------------------------------
        //
        // イベントハンドラ部
        //
        // --------------------------------------------------------------------------------

        /**
         * フォーム表示時
         */
        private void RezeptInputForm_Shown(object sender, EventArgs e)
        {
            //scrollPictureControl1.AdjustImageWidth();
            scrollPictureControl1.SetPictureBoxFill();
            moveFocus(FOCUS_TOP);
        }

        /**
         * 特殊機能キー（MediApp関連）
         */
        private void FormOCRCheck_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.PageUp) buttonRegist_Click(null, null);
            if (e.KeyCode == Keys.PageDown) moveData(false);
        }

        /**
         * 特殊機能キー（コントロール関連）
         */
        private void FormOCRCheck_KeyDown(object sender, KeyEventArgs e)
        {
            //EnterキーでTABキーの動作をさせる
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{TAB}");
        }

        /**
         * データ選択時
         */
        private void Bs_CurrentChanged(object sender, EventArgs e)
        {
            verifyBoxClear();//クリア
            var ma = (MediApp)bs.Current;
            setMediApp(ma);
        }

        /**
         * VerifyBox侵入時
         */
        private void verifyBoxs_Enter(object sender, EventArgs e)
        {
            var t = (TextBox)sender;//VerifyBox#box対応
            if (headerControls.Contains(t)) scrollPictureControl1.ScrollPosition = headerPos;
            else if (headerControls.Contains(t)) scrollPictureControl1.ScrollPosition = headerPos;
            else if (personControls.Contains(t)) scrollPictureControl1.ScrollPosition = personPos;
            else if (daysControls.Contains(t)) scrollPictureControl1.ScrollPosition = daysPos;
            else if (medicalControls.Contains(t)) scrollPictureControl1.ScrollPosition = medicalPos;
            else if (footerControls.Contains(t)) scrollPictureControl1.ScrollPosition = footterPos;
        }

        /**
         * 申請書タイプ入力完了時
         */
        private void verifyBoxMediAppType_Leave(object sender, EventArgs e)
        {
            //申請書以外は入力項目を表示しない
            var currMAT = getCurerntMediAppType();
            if (setAndGetMediAppType(currMAT, true) == null)
            {
                MessageBox.Show("値が不正です。\r\n通常の申請書の場合は空白である必要があります。", "エラー");
                verifyBoxMediAppType.SetTextValue(MEDIAPP_TYPE_APP);
                verifyBoxMediAppType.Focus();
                return;
            }
        }

        /**
         * 登録ボタン
         */
        private void buttonRegist_Click(object sender, EventArgs e)
        {
            var ma = (MediApp)bs.Current;

            var maType = getCurerntMediAppType();
            if (!checkInputtedDatas(maType))
            {
                MessageBox.Show(
                    "入力値が不正です。\r\n赤い項目を確認してください。",
                    "入力値エラー");
                return;
            }

            if (verify && !checkVerify())//ベリファイモードなら
            {
                MessageBox.Show(
                    "ベリファイに失敗しました。\r\n赤い項目を確認し、両方の値が一致するように修正してください。",
                    "入力値エラー");
                getVisibleVerifyBoxList().ForEach(vb => vb.IsVerifying = true);
                moveFocus(FOCUS_FIRST_ERROR);
                return;
            }

            if (!regist(ma, maType, verify))
            {
                MessageBox.Show(
                    "データ登録に失敗しました。", 
                    "登録エラー");
                return;
            }

            verifyBoxClear();//クリア

            updateList();//リスト更新

            if (!moveData(true))
            {
                var mbResult = MessageBox.Show(
                    $"最終行まで到達しました。この都道府県（{prefname}）の入力を終了しますか？", 
                    "退室確認", MessageBoxButtons.YesNo);
                if(mbResult == DialogResult.Yes)
                {
                    this.Close();
                }
            }            
        }

        /**
         * 戻るボタン
         */
        private void buttonBack_Click(object sender, EventArgs e)
        {
            moveData(false);
        }

        /**
         * 終了ボタン
         */
        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /**
         * 時計と逆回転ボタン
         */
        private void buttonImageRotateL_Click(object sender, EventArgs e)
        {
            var ma = (MediApp)bs.Current;
            var fn = ma.GetImagePath(Settings.ImageHost, DB.GetDBName());
            if (!ImageController.ImageRotate(fn, false))
            {
                MessageBox.Show("画像の回転に失敗しました。", "エラー");
                return;
            }
            scrollPictureControl1.SetPictureFile(fn);
        }

        /**
         * 時計回りボタン
         */
        private void buttonImageRotateR_Click(object sender, EventArgs e)
        {
            var ma = (MediApp)bs.Current;
            var fn = ma.GetImagePath(Settings.ImageHost, DB.GetDBName());
            if (!ImageController.ImageRotate(fn, true))
            {
                MessageBox.Show("画像の回転に失敗しました。", "エラー");
                return;
            }
            scrollPictureControl1.SetPictureFile(fn);
        }

        /**
         * 差替ボタン
         */
        private void buttonImageChange_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "TIFファイル(*.tif;*.tiff)|*.tif;*.tiff";
            ofd.Title = "差し替える画像を選択してください";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var newImageDistPath = ofd.FileName;
                var ma = (MediApp)bs.Current;
                string showPath;
                var cResult = imageChange(ma, newImageDistPath, out showPath);
                if (cResult)
                {
                    scrollPictureControl1.SetPictureFile(showPath);
                    MessageBox.Show("画像を差替えました。", "確認");
                }
                else
                {
                    scrollPictureControl1.SetPictureFile(showPath);
                    MessageBox.Show("画像の差替えに失敗しました。", "エラー");
                }
            }
        }

        /**
         * 全体表示ボタン
         */
        private void buttonImageFill_Click(object sender, EventArgs e)
        {
            scrollPictureControl1.SetPictureBoxFill();
        }

        /**
         * DRG区分Leave
         */
        private void verifyBoxDrgType_Leave(object sender, EventArgs e)
        {
            var required = verifyBoxDrgType.Text != "";
            verifyBoxDrgMediDate1G.Required = required;            
            verifyBoxDrgMediDate1Y.Required = required;
            verifyBoxDrgMediDate1M.Required = required;
            verifyBoxDrgMediDate1D.Required = required;
            verifyBoxDrgMediDate2G.Required = required;
            verifyBoxDrgMediDate2Y.Required = required;
            verifyBoxDrgMediDate2M.Required = required;
            verifyBoxDrgMediDate2D.Required = required;

            verifyBoxDrgMediDate1G.DoCheck(verifyBoxDrgMediDate1G);
            verifyBoxDrgMediDate1Y.DoCheck(verifyBoxDrgMediDate1Y);
            verifyBoxDrgMediDate1M.DoCheck(verifyBoxDrgMediDate1M);
            verifyBoxDrgMediDate1D.DoCheck(verifyBoxDrgMediDate1D);
            verifyBoxDrgMediDate2G.DoCheck(verifyBoxDrgMediDate2G);
            verifyBoxDrgMediDate2Y.DoCheck(verifyBoxDrgMediDate2Y);
            verifyBoxDrgMediDate2M.DoCheck(verifyBoxDrgMediDate2M);
            verifyBoxDrgMediDate2D.DoCheck(verifyBoxDrgMediDate2D);
        }


        // --------------------------------------------------------------------------------
        //
        // 入力関連部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 必須制御
        /// </summary>
        /// <param name="ma"></param>
        private void initControlRequired(MediApp ma)
        {
            var required = ma.DrgMediDate1 != DateTime.MinValue;
            verifyBoxDrgMediDate1G.Required = required;
            verifyBoxDrgMediDate1Y.Required = required;
            verifyBoxDrgMediDate1M.Required = required;
            verifyBoxDrgMediDate1D.Required = required;
            verifyBoxDrgMediDate2G.Required = required;
            verifyBoxDrgMediDate2Y.Required = required;
            verifyBoxDrgMediDate2M.Required = required;
            verifyBoxDrgMediDate2D.Required = required;
        }

        /// <summary>
        /// 入力データを反映します。
        /// </summary>
        /// <param name="ma"></param>
        private void setMediApp(MediApp ma)
        {
            //画像貼付
            scrollPictureControl1.SetPictureFile(ma.GetImagePath(Settings.ImageHost, DB.GetDBName()));
            //scrollPictureControl1.AdjustImageWidth();
            scrollPictureControl1.SetPictureBoxFill();

            //表示の初期化
            var maType = getMediAppType(ma.MAType);
            var matStr = setAndGetMediAppType(maType);

            //フォーカスセット
            moveFocus(FOCUS_TOP);

            //ベリファイなら１回目入力結果も反映
            if (verify) setMediAppToVerify(ma, maType);

            //必須制御
            initControlRequired(ma);

            //再編集か確認
            if (!verify && ma.InputStatus1 != (int)Consts.INPUT_STATUS.入力済) return;
            if (verify && ma.InputStatus2 != (int)Consts.INPUT_STATUS.入力済) return;

            //値貼付
            verifyBoxMediAppType.SetTextValue(matStr);
            if (maType == Consts.MEDIAPP_TYPE.バッチ) verifyBoxNumbering.SetTextValue(ma.Batch);
            else verifyBoxNumbering.SetIntValue(ma.Numbering);
            verifyBoxY.SetIntValue(DateTimeEx.GetHYYFromYYYYMM(ma.MYM));
            verifyBoxM.SetIntValue(DateTimeEx.GetMMFromYYYYMM(ma.MYM));
            verifyBoxPref.SetTextValue(ma.PrefNum);
            verifyBoxHospital.SetTextValue(ma.HospitalCode);
            verifyBoxMeisai.SetIntValue(ma.MeisaiType);
            verifyBoxHonninKazoku.SetIntValue(ma.HonninKazoku);
            verifyBoxPref.SetTextValue(ma.PrefNum);
            verifyBoxInum.SetTextValue(ma.INum);
            verifyBoxHnum.SetTextValue(ma.HNum);
            verifyBoxTuuchi.SetIntValue(ma.TuuchiType);
            verifyBoxKouhi1Code.SetTextValue(ma.Kouhi1Code);
            verifyBoxKouhi2Code.SetTextValue(ma.Kouhi2Code);
            verifyBoxSex.SetIntValue(ma.Sex);
            var JYYMMDD = DateTimeEx.GetIntJpDateWithEraNumber(ma.Birthday).ToString();
            if (JYYMMDD.Length == 7)
            {
                verifyBoxBirthEra.SetIntValue(Converter.ToInt(JYYMMDD.Substring(0, 1)));
                verifyBoxBirthY.SetIntValue(Converter.ToInt(JYYMMDD.Substring(1, 2)));
                verifyBoxBirthM.SetIntValue(Converter.ToInt(JYYMMDD.Substring(3, 2)));
                verifyBoxBirthD.SetIntValue(Converter.ToInt(JYYMMDD.Substring(5, 2)));
            }
            verifyBoxTokki.SetIntValue(ma.Tokki);

            verifyBoxMediDays.SetIntValue(ma.IhoDays);
            verifyBoxMediPoint.SetIntValue(ma.IhoPoint);
            verifyBoxMediPrice.SetIntValue(ma.IhoPrice);
            verifyBoxMediKouhi1Days.SetIntValue(ma.Kouhi1Days);
            verifyBoxMediKouhi1Point.SetIntValue(ma.Kouhi1Point);
            verifyBoxMediKouhi1Price.SetIntValue(ma.Kouhi1Price);
            verifyBoxMediKouhi2Days.SetIntValue(ma.Kouhi2Days);
            verifyBoxMediKouhi2Point.SetIntValue(ma.Kouhi2Point);
            verifyBoxMediKouhi2Price.SetIntValue(ma.Kouhi2Price);

            verifyBoxSyokujiDays.SetIntValue(ma.SyokujiIhoDays);
            verifyBoxSyokujiMediPrice.SetIntValue(ma.SyokujiIhoMediPrice);
            verifyBoxSyokujiHutanPrice.SetIntValue(ma.SyokujiIhoHutanPrice);
            verifyBoxSyokujiKouhi1Days.SetIntValue(ma.SyokujiKouhi1Days);
            verifyBoxSyokujiKouhi1MediPrice.SetIntValue(ma.SyokujiKouhi1MediPrice);
            verifyBoxSyokujiKouhi1HutanPrice.SetIntValue(ma.SyokujiKouhi1HutanPrice);
            verifyBoxSyokujiKouhi2Days.SetIntValue(ma.SyokujiKouhi2Days);
            verifyBoxSyokujiKouhi2MediPrice.SetIntValue(ma.SyokujiKouhi2MediPrice);
            verifyBoxSyokujiKouhi2HutanPrice.SetIntValue(ma.SyokujiKouhi2HutanPrice);

            verifyBoxKougaku.SetIntValue(ma.KougakuPrice);
            verifyBoxAgain.SetIntValue(ma.Again);

            verifyBoxDrgType.SetIntValue(ma.DrgType);
            var drgMediDate1 = DateTimeEx.GetIntJpDateWithEraNumber(ma.DrgMediDate1).ToString();
            if (drgMediDate1.Length == 7)
            {
                verifyBoxDrgMediDate1G.SetIntValue(Converter.ToInt(drgMediDate1.Substring(0, 1)));
                verifyBoxDrgMediDate1Y.SetIntValue(Converter.ToInt(drgMediDate1.Substring(1, 2)));
                verifyBoxDrgMediDate1M.SetIntValue(Converter.ToInt(drgMediDate1.Substring(3, 2)));
                verifyBoxDrgMediDate1D.SetIntValue(Converter.ToInt(drgMediDate1.Substring(5, 2)));
            }
            var drgMediDate2 = DateTimeEx.GetIntJpDateWithEraNumber(ma.DrgMediDate2).ToString();
            if (drgMediDate2.Length == 7)
            {
                verifyBoxDrgMediDate2G.SetIntValue(Converter.ToInt(drgMediDate2.Substring(0, 1)));
                verifyBoxDrgMediDate2Y.SetIntValue(Converter.ToInt(drgMediDate2.Substring(1, 2)));
                verifyBoxDrgMediDate2M.SetIntValue(Converter.ToInt(drgMediDate2.Substring(3, 2)));
                verifyBoxDrgMediDate2D.SetIntValue(Converter.ToInt(drgMediDate2.Substring(5, 2)));
            }

            //一度全チェック
            getVisibleVerifyBoxList().ForEach(vb => vb.DoCheck(vb));
        }

        /// <summary>
        /// VerifyBoxの１回目入力結果用ボックスに値を反映する。
        /// </summary>
        /// <param name="ma"></param>
        /// <param name="maTypeStr"></param>
        private void setMediAppToVerify(MediApp ma, Consts.MEDIAPP_TYPE maType)
        {
            //値貼付
            verifyBoxMediAppType.SetTextVerifyValue(toMediAppTypeString(maType));
            if (maType == Consts.MEDIAPP_TYPE.バッチ) verifyBoxNumbering.SetTextVerifyValue(ma.Batch);
            else verifyBoxNumbering.SetIntVerifyValue(ma.Numbering);
            verifyBoxY.SetIntVerifyValue(DateTimeEx.GetHYYFromYYYYMM(ma.MYM));
            verifyBoxM.SetIntVerifyValue(DateTimeEx.GetMMFromYYYYMM(ma.MYM));
            verifyBoxPref.SetTextVerifyValue(ma.PrefNum);
            verifyBoxHospital.SetTextVerifyValue(ma.HospitalCode);
            verifyBoxMeisai.SetIntVerifyValue(ma.MeisaiType);
            verifyBoxHonninKazoku.SetIntVerifyValue(ma.HonninKazoku);
            verifyBoxInum.SetTextVerifyValue(ma.INum);
            verifyBoxHnum.SetTextVerifyValue(ma.HNum);
            verifyBoxTuuchi.SetIntVerifyValue(ma.TuuchiType);
            verifyBoxKouhi1Code.SetTextVerifyValue(ma.Kouhi1Code);
            verifyBoxKouhi2Code.SetTextVerifyValue(ma.Kouhi2Code);
            verifyBoxSex.SetIntVerifyValue(ma.Sex);
            var JYYMMDD = DateTimeEx.GetIntJpDateWithEraNumber(ma.Birthday).ToString();
            if (JYYMMDD.Length == 7)
            {
                verifyBoxBirthEra.SetIntVerifyValue(Converter.ToInt(JYYMMDD.Substring(0, 1)));
                verifyBoxBirthY.SetIntVerifyValue(Converter.ToInt(JYYMMDD.Substring(1, 2)));
                verifyBoxBirthM.SetIntVerifyValue(Converter.ToInt(JYYMMDD.Substring(3, 2)));
                verifyBoxBirthD.SetIntVerifyValue(Converter.ToInt(JYYMMDD.Substring(5, 2)));
            }
            verifyBoxTokki.SetIntVerifyValue(ma.Tokki);

            verifyBoxMediDays.SetIntVerifyValue(ma.IhoDays);
            verifyBoxMediPoint.SetIntVerifyValue(ma.IhoPoint);
            verifyBoxMediPrice.SetIntVerifyValue(ma.IhoPrice);
            verifyBoxMediKouhi1Days.SetIntVerifyValue(ma.Kouhi1Days);
            verifyBoxMediKouhi1Point.SetIntVerifyValue(ma.Kouhi1Point);
            verifyBoxMediKouhi1Price.SetIntVerifyValue(ma.Kouhi1Price);
            verifyBoxMediKouhi2Days.SetIntVerifyValue(ma.Kouhi2Days);
            verifyBoxMediKouhi2Point.SetIntVerifyValue(ma.Kouhi2Point);
            verifyBoxMediKouhi2Price.SetIntVerifyValue(ma.Kouhi2Price);

            verifyBoxSyokujiDays.SetIntVerifyValue(ma.SyokujiIhoDays);
            verifyBoxSyokujiMediPrice.SetIntVerifyValue(ma.SyokujiIhoMediPrice);
            verifyBoxSyokujiHutanPrice.SetIntVerifyValue(ma.SyokujiIhoHutanPrice);
            verifyBoxSyokujiKouhi1Days.SetIntVerifyValue(ma.SyokujiKouhi1Days);
            verifyBoxSyokujiKouhi1MediPrice.SetIntVerifyValue(ma.SyokujiKouhi1MediPrice);
            verifyBoxSyokujiKouhi1HutanPrice.SetIntVerifyValue(ma.SyokujiKouhi1HutanPrice);
            verifyBoxSyokujiKouhi2Days.SetIntVerifyValue(ma.SyokujiKouhi2Days);
            verifyBoxSyokujiKouhi2MediPrice.SetIntVerifyValue(ma.SyokujiKouhi2MediPrice);
            verifyBoxSyokujiKouhi2HutanPrice.SetIntVerifyValue(ma.SyokujiKouhi2HutanPrice);

            verifyBoxKougaku.SetIntVerifyValue(ma.KougakuPrice);
            verifyBoxAgain.SetIntVerifyValue(ma.Again);

            var drgMediDate1 = DateTimeEx.GetIntJpDateWithEraNumber(ma.DrgMediDate1).ToString();
            if (drgMediDate1.Length == 7)
            {
                verifyBoxDrgMediDate1G.SetIntVerifyValue(Converter.ToInt(drgMediDate1.Substring(0, 1)));
                verifyBoxDrgMediDate1Y.SetIntVerifyValue(Converter.ToInt(drgMediDate1.Substring(1, 2)));
                verifyBoxDrgMediDate1M.SetIntVerifyValue(Converter.ToInt(drgMediDate1.Substring(3, 2)));
                verifyBoxDrgMediDate1D.SetIntVerifyValue(Converter.ToInt(drgMediDate1.Substring(5, 2)));
            }
            var drgMediDate2 = DateTimeEx.GetIntJpDateWithEraNumber(ma.DrgMediDate2).ToString();
            if (drgMediDate2.Length == 7)
            {
                verifyBoxDrgMediDate2G.SetIntVerifyValue(Converter.ToInt(drgMediDate2.Substring(0, 1)));
                verifyBoxDrgMediDate2Y.SetIntVerifyValue(Converter.ToInt(drgMediDate2.Substring(1, 2)));
                verifyBoxDrgMediDate2M.SetIntVerifyValue(Converter.ToInt(drgMediDate2.Substring(3, 2)));
                verifyBoxDrgMediDate2D.SetIntVerifyValue(Converter.ToInt(drgMediDate2.Substring(5, 2)));
            }
        }

        /// <summary>
        /// 入力されたデータの正当性を確認します。
        /// </summary>
        /// <param name="maType"></param>
        /// <returns></returns>
        private bool checkInputtedDatas(Consts.MEDIAPP_TYPE maType)
        {
            switch (maType)
            {
                case Consts.MEDIAPP_TYPE.申請書:
                    var vbList = getVisibleVerifyBoxList();
                    foreach (var vb in vbList)
                    {
                        if (vb.IsError)
                        {
                            Console.WriteLine(vb.Name);
                            return false;
                        }
                    }
                    //以下特殊処理
                    var tokki = verifyBoxTokki.GetIntValue();
                    if (tokki != -1 && !verifyBoxTokki.IsError && !Tokki.Match(tokki)) return false;
                    break;
                case Consts.MEDIAPP_TYPE.バッチ:
                    var numbering = verifyBoxNumbering.GetIntValue();
                    if (numbering == -1) return false;
                    if (numbering.ToString().Length != 4) return false;
                    break;
                case Consts.MEDIAPP_TYPE.続紙:
                    break;
                case Consts.MEDIAPP_TYPE.付箋:
                    break;
                case Consts.MEDIAPP_TYPE.不要:
                    break;
                case Consts.MEDIAPP_TYPE.再審査返戻:
                    break;
            }

            return true;
        }

        /// <summary>
        /// ベリファイ結果を確認します。
        /// </summary>
        /// <returns></returns>
        private bool checkVerify()
        {
            var vbList = getVisibleVerifyBoxList();

            var result = true;
            foreach (var vb in vbList)
            {
                result &= vb.CheckVerify();
                if (!result)
                {
                    Console.WriteLine(vb.Name);
                }
            }

            return result;
        }


        // --------------------------------------------------------------------------------
        //
        // MediApp操作部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// MediAppへ登録を行います。
        /// </summary>
        /// <param name="cma"></param>
        /// <param name="maType"></param>
        /// <param name="verify"></param>
        /// <returns></returns>
        private bool regist(MediApp cma, Consts.MEDIAPP_TYPE maType, bool verify)
        {
            try
            {
                //ユーザ関連
                if (verify)
                {
                    cma.InputStatus2 = (int)Consts.INPUT_STATUS.入力済;
                    cma.InputUser2 = User.CurrentUser.UserID;
                }
                else
                {
                    cma.InputStatus1 = (int)Consts.INPUT_STATUS.入力済;
                    cma.InputUser1 = User.CurrentUser.UserID;
                }

                var matInt = (int)maType;

                //バッチ
                if (maType == Consts.MEDIAPP_TYPE.バッチ)
                {
                    cma.MAType = matInt;
                    cma.Batch = verifyBoxNumbering.GetTextValue();
                    return cma.Update();
                }

                //付箋
                if (maType == Consts.MEDIAPP_TYPE.付箋)
                {
                    cma.MAType = matInt;
                    cma.Again = 1;
                    return cma.Update();
                }

                //続紙
                if (maType == Consts.MEDIAPP_TYPE.続紙)
                {
                    cma.MAType = matInt;
                    return cma.Update();
                }

                //不要
                if (maType == Consts.MEDIAPP_TYPE.不要)
                {
                    cma.MAType = matInt;
                    return cma.Update();
                }

                //再審査返戻
                if (maType == Consts.MEDIAPP_TYPE.再審査返戻)
                {
                    cma.MAType = matInt;
                    cma.Numbering = verifyBoxNumbering.GetIntValue();
                    return cma.Update();
                }

                //申請書
                if (maType == Consts.MEDIAPP_TYPE.申請書)
                {
                    cma.MAType = matInt;
                    cma.Numbering = verifyBoxNumbering.GetIntValue();
                    var my = verifyBoxY.GetIntValue();
                    var mm = verifyBoxM.GetIntValue();
                    cma.MYM = DateTimeEx.GetYYYYMMFromHYYandMM(my, mm);
                    cma.MeisaiType = verifyBoxMeisai.GetIntValue();
                    var lead2 = verifyBoxInum.GetTextValue().Substring(0, 2);
                    if (IppanRouken.IsRouken(lead2)) cma.IppanRouken = IppanRouken.Rouken;
                    else cma.IppanRouken = IppanRouken.Ippan;
                    var kc1 = verifyBoxKouhi1Code.GetTextValue();
                    var kc2 = verifyBoxKouhi2Code.GetTextValue();
                    cma.SyahoKouhi = SyahoKouhi.GetCode(kc1, kc2);
                    cma.HonninKazoku = verifyBoxHonninKazoku.GetIntValue();
                    cma.PrefNum = verifyBoxPref.GetTextValue();
                    cma.HospitalCode = verifyBoxHospital.GetTextValue();
                    cma.Kouhi1Code = verifyBoxKouhi1Code.GetTextValue();
                    cma.INum = verifyBoxInum.GetTextValue();
                    cma.HNum = verifyBoxHnum.GetTextValue();
                    cma.Sex = verifyBoxSex.GetIntValue();
                    var j = verifyBoxBirthEra.GetIntValue();
                    var yy = verifyBoxBirthY.GetIntValue().ToString("00");
                    var MM = verifyBoxBirthM.GetIntValue().ToString("00");
                    var dd = verifyBoxBirthD.GetIntValue().ToString("00");
                    cma.Birthday = DateTimeEx.GetDateFromJstr7($"{j}{yy}{MM}{dd}");
                    cma.Tokki = verifyBoxTokki.GetIntValue();
                    cma.TuuchiType = verifyBoxTuuchi.GetIntValue();
                    cma.IhoDays = verifyBoxMediDays.GetIntValue();
                    cma.IhoPoint = verifyBoxMediPoint.GetIntValue();
                    cma.IhoPrice = verifyBoxMediPrice.GetIntValue();
                    cma.Kouhi1Days = verifyBoxMediKouhi1Days.GetIntValue();
                    cma.Kouhi1Point = verifyBoxMediKouhi1Point.GetIntValue();
                    cma.Kouhi1Price = verifyBoxMediKouhi1Price.GetIntValue();
                    cma.Kouhi2Days = verifyBoxMediKouhi2Days.GetIntValue();
                    cma.Kouhi2Point = verifyBoxMediKouhi2Point.GetIntValue();
                    cma.Kouhi2Price = verifyBoxMediKouhi2Price.GetIntValue();
                    cma.SyokujiIhoDays = verifyBoxSyokujiDays.GetIntValue();
                    cma.SyokujiIhoMediPrice = verifyBoxSyokujiMediPrice.GetIntValue();
                    cma.SyokujiIhoHutanPrice = verifyBoxSyokujiHutanPrice.GetIntValue();
                    cma.SyokujiKouhi1Days = verifyBoxSyokujiKouhi1Days.GetIntValue();
                    cma.SyokujiKouhi1MediPrice = verifyBoxSyokujiKouhi1MediPrice.GetIntValue();
                    cma.SyokujiKouhi1HutanPrice = verifyBoxSyokujiKouhi1HutanPrice.GetIntValue();
                    cma.SyokujiKouhi2Days = verifyBoxSyokujiKouhi2Days.GetIntValue();
                    cma.SyokujiKouhi2MediPrice = verifyBoxSyokujiKouhi2MediPrice.GetIntValue();
                    cma.SyokujiKouhi2HutanPrice = verifyBoxSyokujiKouhi2HutanPrice.GetIntValue();
                    cma.KougakuPrice = verifyBoxKougaku.GetIntValue();
                    cma.Again = verifyBoxAgain.GetIntValue();
                    cma.DrgType = verifyBoxDrgType.GetIntValue();
                    var drg1G = verifyBoxDrgMediDate1G.GetIntValue();
                    var drg1YY = verifyBoxDrgMediDate1Y.GetIntValue().ToString("00");
                    var drg1MM = verifyBoxDrgMediDate1M.GetIntValue().ToString("00");
                    var drg1DD = verifyBoxDrgMediDate1D.GetIntValue().ToString("00");
                    cma.DrgMediDate1 = DateTimeEx.GetDateFromJstr7($"{drg1G}{drg1YY}{drg1MM}{drg1DD}");
                    var drg2G = verifyBoxDrgMediDate2G.GetIntValue();
                    var drg2YY = verifyBoxDrgMediDate2Y.GetIntValue().ToString("00");
                    var drg2MM = verifyBoxDrgMediDate2M.GetIntValue().ToString("00");
                    var drg2DD = verifyBoxDrgMediDate2D.GetIntValue().ToString("00");
                    cma.DrgMediDate2 = DateTimeEx.GetDateFromJstr7($"{drg2G}{drg2YY}{drg2MM}{drg2DD}");
                    cma.Kouhi2Code = verifyBoxKouhi2Code.GetTextValue();

                    return cma.Update();
                }
            }
            catch(Exception ex)
            {
                Log.ErrorWrite(ex);
            }
            return false;
        }

        // --------------------------------------------------------------------------------
        //
        // Utility部
        //
        // --------------------------------------------------------------------------------

        /// <summary>
        /// 現在入力中の申請書タイプを取得します。
        /// </summary>
        /// <returns></returns>
        private Consts.MEDIAPP_TYPE getCurerntMediAppType()
        {
            var currMAT = verifyBoxMediAppType.GetTextValue();
            switch (currMAT)
            {
                case MEDIAPP_TYPE_APP:
                    return Consts.MEDIAPP_TYPE.申請書;
                case MEDIAPP_TYPE_BATCH:
                    return Consts.MEDIAPP_TYPE.バッチ;
                case MEDIAPP_TYPE_NEXT:
                    return Consts.MEDIAPP_TYPE.続紙;
                case MEDIAPP_TYPE_HUSEN:
                    return Consts.MEDIAPP_TYPE.付箋;
                case MEDIAPP_TYPE_NONE:
                    return  Consts.MEDIAPP_TYPE.不要;
                case MEDIAPP_TYPE_HENREI:
                    return Consts.MEDIAPP_TYPE.再審査返戻;
                default:
                    return Consts.MEDIAPP_TYPE.NULL;
            }
        }

        /// <summary>
        /// 申請書のタイプを取得します。
        /// </summary>
        /// <param name="maType"></param>
        /// <returns></returns>
        private static Consts.MEDIAPP_TYPE getMediAppType(int maType)
        {
            if(Enum.IsDefined(typeof(Consts.MEDIAPP_TYPE), maType))
            {
                return (Consts.MEDIAPP_TYPE)maType;
            }
            return Consts.MEDIAPP_TYPE.NULL;
        }

        /// <summary>
        /// Consts.MEDIAPP_TYPE値に該当する表示用文字列を取得します。
        /// </summary>
        /// <param name="maType"></param>
        /// <returns></returns>
        private string toMediAppTypeString(Consts.MEDIAPP_TYPE maType)
        {
            switch (maType)
            {
                case Consts.MEDIAPP_TYPE.申請書:
                    return MEDIAPP_TYPE_APP;
                case Consts.MEDIAPP_TYPE.バッチ:
                    return MEDIAPP_TYPE_BATCH;
                case Consts.MEDIAPP_TYPE.続紙:
                    return MEDIAPP_TYPE_NEXT;
                case Consts.MEDIAPP_TYPE.付箋:
                    return MEDIAPP_TYPE_HUSEN;
                case Consts.MEDIAPP_TYPE.不要:
                    return MEDIAPP_TYPE_NONE;
                case Consts.MEDIAPP_TYPE.再審査返戻:
                    return MEDIAPP_TYPE_HENREI;
                case Consts.MEDIAPP_TYPE.NULL:
                    return MEDIAPP_TYPE_NULL;
            }
            return null;
        }

        /// <summary>
        /// 申請書のタイプに応じて見た目を変更します。
        /// inputting=trueにした場合はフォーカスと自動的に変更します。
        /// </summary>
        /// <param name="maType"></param>
        /// <param name="inputting"></param>
        /// <returns></returns>
        private string setAndGetMediAppType(Consts.MEDIAPP_TYPE maType, bool inputting = false)
        {
            switch (maType)
            {
                //申請書
                case Consts.MEDIAPP_TYPE.申請書:
                    panelInput.Visible = true;
                    labelNumberingOrBatch.Visible = true;
                    verifyBoxNumbering.Visible = true;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if(inputting) moveFocus(FOCUS_NUMBERING);
                    break;
                //バッチ
                case Consts.MEDIAPP_TYPE.バッチ:
                    panelInput.Visible = false;
                    labelNumberingOrBatch.Visible = true;
                    verifyBoxNumbering.Visible = true;
                    verifyBoxNumbering.TextMaxLength = 4;
                    labelNumberingOrBatch.Text = "バッチ番号";
                    if (inputting) moveFocus(FOCUS_NUMBERING);
                    break;
                //続紙
                case Consts.MEDIAPP_TYPE.続紙:
                    panelInput.Visible = false;
                    labelNumberingOrBatch.Visible = false;
                    verifyBoxNumbering.Visible = false;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if (inputting) moveFocus(FOCUS_REGIST);
                    break;
                //付箋
                case Consts.MEDIAPP_TYPE.付箋:
                    panelInput.Visible = false;
                    labelNumberingOrBatch.Visible = false;
                    verifyBoxNumbering.Visible = false;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if (inputting) moveFocus(FOCUS_REGIST);
                    break;
                //不要
                case Consts.MEDIAPP_TYPE.不要:
                    panelInput.Visible = false;
                    labelNumberingOrBatch.Visible = false;
                    verifyBoxNumbering.Visible = false;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if (inputting) moveFocus(FOCUS_REGIST);
                    break;
                //再審査返戻
                case Consts.MEDIAPP_TYPE.再審査返戻:
                    panelInput.Visible = false;
                    labelNumberingOrBatch.Visible = true;
                    verifyBoxNumbering.Visible = true;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if (inputting) moveFocus(FOCUS_NUMBERING);
                    break;
                //初期値
                case Consts.MEDIAPP_TYPE.NULL:
                    panelInput.Visible = true;
                    labelNumberingOrBatch.Visible = true;
                    verifyBoxNumbering.Visible = true;
                    verifyBoxNumbering.TextMaxLength = 6;
                    labelNumberingOrBatch.Text = "ナンバリング";
                    if (inputting) moveFocus(FOCUS_TOP);
                    break;
            }
            return toMediAppTypeString(maType);
        }

        /// <summary>
        /// フォーカスを移動します。引数には定数を指定します。
        /// </summary>
        /// <param name="constFocusValue"></param>
        private void moveFocus(int constFocusValue)
        {
            switch (constFocusValue)
            {
                case FOCUS_TOP:
                    verifyBoxMediAppType.Focus();
                    break;
                case FOCUS_NUMBERING:
                    verifyBoxNumbering.Focus();
                    break;
                case FOCUS_REGIST:
                    buttonRegist.Focus();
                    break;
                case FOCUS_FIRST_ERROR:
                    var vbList = getVisibleVerifyBoxList();
                    foreach(var vb in vbList)
                    {
                        if (vb.IsVerifyError)
                        {
                            vb.Focus();
                            return;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// 選択されているデータから移動します。true:次のデータ、false:一つ前のデータ。
        /// 戻り値falseはそれ以上進めない場合に返されます。
        /// </summary>
        /// <param name="nextOrBack"></param>
        private bool moveData(bool nextOrBack)
        {
            var ri = dataGridViewPlist.CurrentCell.RowIndex;
            var batchIndex = dataGridViewPlist.Columns[nameof(MediApp.MediAppTypeView)].Index;
            if (nextOrBack)
            {
                if (ri >= bs.Count - 1) return false;
                dataGridViewPlist.CurrentCell = dataGridViewPlist[batchIndex, ri + 1];
            }
            else
            {
                if (ri <= 0) return false;
                dataGridViewPlist.CurrentCell = dataGridViewPlist[batchIndex, ri - 1];
            }
            return true;
        }        

        /// <summary>
        /// 表示中のVerifyBoxをすべて取得します。true:不可視も含めたすべて
        /// </summary>
        /// <param name="all"></param>
        /// <returns></returns>
        private List<VerifyBox> getVisibleVerifyBoxList(bool all=false)
        {
            var list = new List<VerifyBox>();
            Action<Control> get = null;
            get = cs =>
            {
                VerifyBox vb;
                foreach (Control c in cs.Controls)
                {
                    if (!all && !c.Visible) continue;
                    if (c is VerifyBox)
                    {
                        vb = (VerifyBox)c;
                        list.Add(vb);
                    }
                    if(c is Panel || c is GroupBox)
                    {
                        get(c);
                    }
                }
            };
            get(panelControls);
            list.Sort((x, y) => x.TabIndex.CompareTo(y.TabIndex));//タブインデックス昇順
            return list;
        }

        /// <summary>
        /// VerifyBoxの値をすべてクリアします。
        /// </summary>
        private void verifyBoxClear()
        {
            getVisibleVerifyBoxList(true).ForEach(vb => vb.AllClear());
            panelInput.Visible = true;
        }

        /// <summary>
        /// リストの情報を更新します。
        /// </summary>
        private void updateList()
        {
            bs.ResetBindings(false);
        }

        /// <summary>
        /// 画像の差替えを行います。
        /// </summary>
        /// <param name="ma"></param>
        /// <param name="newFilePath"></param>
        /// <param name="showPath"></param>
        /// <returns></returns>
        private bool imageChange(MediApp ma, string newFilePath, out string showPath)
        {
            var fPath = ScanImage.GetImageFolderPathToIYMPID(ma.IYM, ma.PID);
            var currentFilePath = $"{fPath}\\{ma.FileName}";
            var currentFilePath2 = $"{fPath}\\{FileUtility.GetFileName(currentFilePath)}(Copy){FileUtility.GetExtension(currentFilePath)}";
            showPath = currentFilePath;
            try
            {
                //復元用にリネーム退避
                System.IO.File.Move(currentFilePath, currentFilePath2);
            }
            catch (Exception ex)
            {
                Log.ErrorWrite($"画像ファイル（{currentFilePath}）のリネームに失敗しました。");
                Log.ErrorWrite(ex);
                return false;
            }
            try
            {
                //コピー
                System.IO.File.Copy(newFilePath, currentFilePath);
            }
            catch (Exception ex)
            {
                Log.ErrorWrite($"画像ファイル（{newFilePath}）のコピーに失敗しました。");
                Log.ErrorWrite(ex);                
                try
                {
                    //リネーム分をもとに戻す
                    System.IO.File.Move(currentFilePath2, currentFilePath);
                }
                catch (Exception ex2)
                {
                    //元に戻すこともできない
                    Log.ErrorWrite($"リネームファイル（{currentFilePath2}）を元に戻せませんでした");
                    Log.ErrorWrite(ex2);
                }
                return false;
            }
            try
            {
                //リネーム分を削除
                System.IO.File.Delete(currentFilePath2);
            }
            catch (Exception ex)
            {
                //リネーム分が消せていないが一応成功とする
                Log.ErrorWrite($"リネームファイル（{currentFilePath2}）を削除できませんでした");
                Log.ErrorWrite(ex);
                return true;
            }

            return true;
        }

    }
}
