﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using meps.Utils;
using meps.Table;
using System.Collections.Generic;

namespace meps.Forms
{
    public partial class ScanForm : Form
    {
        string imageFolderPath;
        string[] imageSubFolderPaths;
        int imageFileCount;
        int imageFolderCount;
        bool allFolder;

        BindingSource bsPref = new BindingSource();

        public ScanForm()
        {
            InitializeComponent();
        }

        private void ScanForm_Shown(object sender, EventArgs e)
        {
            //現在年を入れてあげる
            textBoxYear.Text = DateTimeEx.GetJpYear(DateTime.Today).ToString();
            textBoxMonth.Focus();
        }

        //終了
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// フォルダ個別画像登録
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonFolderSelect_Click(object sender, EventArgs e)
        {
            int y, m;
            int todayJY = DateTimeEx.GetJpYear(DateTime.Today);

            string fn = "";

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("審査年を指定してください");
                return;
            }
            else if (y < todayJY - 3 || y > todayJY + 3)
            {
                MessageBox.Show($"審査年度の指定が不正です\r\n({DateTimeEx.GetJpYear(DateTime.Today)}±3)");
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("審査月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            //フォルダ選択ダイアログ表示
            var f = new DialogUtility.OpenFolderDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                fn = f.FolderPath;
            }
            else return;

            //指定されたフォルダが存在するかチェック
            if (!System.IO.Directory.Exists(fn))
            {
                MessageBox.Show("フォルダが存在しません");
                return;
            }

            //審査年月がパスに含まれるか確認（18/04後藤伸さん）
            var yymm = y * 100 + m;
            if (!fn.Contains(yymm.ToString()))
            {
                var r = MessageBox.Show($"審査年月「{yymm}」がパスの中に含まれていませんがよろしいですか？","確認", 
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (r != DialogResult.OK) return;
            }

            //ファイル数を取得
            var fcount = FileUtility.GetFilesInFolder(fn).Count(s =>
            {
                var ex = System.IO.Path.GetExtension(s).ToLower();
                return ex == ".tiff" || ex == ".tif";
            });
            //指定されたフォルダにファイルが存在するかチェック
            if (fcount == 0)
            {
                MessageBox.Show("画像ファイル（.tif）が存在しません");
                return;
            }

            imageFolderPath = fn;
            imageFileCount = fcount;
            imageSubFolderPaths = null;
            imageFolderCount = 1;
            allFolder = false;

            textBoxImageFolderPath.Text = fn;

            MessageBox.Show(
                $"{fcount} 件の画像ファイルが見付かりました。\r\n「取込開始」ボタンを押してください。", 
                "準備完了");
        }

        /// <summary>
        /// フォルダ一括登録
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAllFolder_Click(object sender, EventArgs e)
        {
            int y, m;
            int todayJY = DateTimeEx.GetJpYear(DateTime.Today);

            string fn;
            string[] subFolders;

            int count;

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("審査年を指定してください");
                return;
            }
            else if (y < todayJY - 1 || y > todayJY + 1)
            {
                MessageBox.Show("審査年度の指定が不正です\r\n(" + DateTimeEx.GetJpYear(DateTime.Today) + "±1)");
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("審査月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            //フォルダ選択ダイアログ表示
            var f = new DialogUtility.OpenFolderDialog();
            if (f.ShowDialog() == DialogResult.OK)
            {
                fn = f.FolderPath;
            }
            else return;

            //審査年月がパスに含まれるか確認（18/04後藤伸さん）
            var yymm = y * 100 + m;
            if (!fn.Contains(yymm.ToString()))
            {
                var r = MessageBox.Show($"審査年月「{yymm}」がパスの中に含まれていませんがよろしいですか？", "確認",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                if (r != DialogResult.OK) return;
            }

            try
            {
                subFolders = FileUtility.GetFoldersInFolder(fn);
                count = FileUtility.GetFilesInFolder(fn).Count(s =>
                {
                    var ex = System.IO.Path.GetExtension(s).ToLower();
                    return ex == ".tiff" || ex == ".tif";
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show($"アクセス権のないフォルダが検出されました\r\n\r\n{ex}",
                    "画像取り込み", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            imageFolderPath = fn;
            imageFileCount = count;
            imageSubFolderPaths = subFolders;
            imageFolderCount = subFolders.Count();
            allFolder = true;

            textBoxImageFolderPath.Text = fn;

            MessageBox.Show(
                $"{imageFolderCount} 個のフォルダと {imageFileCount} 件の画像ファイルが見付かりました。\r\n「取込開始」ボタンを押してください。",
                "準備完了");
        }

        /// <summary>
        /// 取込開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImportStart_Click(object sender, EventArgs e)
        {
            var y = Converter.ToInt(textBoxYear.Text);
            var M = Converter.ToInt(textBoxMonth.Text);

            //20190724145503 furukawa st ////////////////////////
            //令和対応
            
            var iym = DateTimeEx.GetAdYearFromWareki(y * 100 + M) * 100 + M;
            //var iym = DateTimeEx.GetAdYearFromHs(y) * 100 + M;
            //20190724145503 furukawa ed ////////////////////////




            if (imageFileCount == 0)
            {
                MessageBox.Show("1件以上の画像が入ったフォルダを指定してください");
                return;
            }

            bool result;
            string errormessage;
            if (allFolder)
            {
                //複数フォルダ
                var res = MessageBox.Show(imageFolderCount + " 個のフォルダ内に合計 " +
                imageFileCount + " 個のtiff画像ファイルが検出されました。\r\n" +
                "画像の一括取り込みを開始しますか?",
                "画像取り込み", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res == System.Windows.Forms.DialogResult.Cancel) return;

                result = doImageImport(iym, true, out errormessage);
            }
            else
            {
                //単一フォルダ
                var res = MessageBox.Show(
                imageFileCount.ToString() +
                " 件の画像の取り込みを開始します。よろしいですか？", "画像取込",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (res != System.Windows.Forms.DialogResult.OK) return;

                result = doImageImport(iym, false, out errormessage);
            }

            if (result)
            {
                MessageBox.Show("取り込みが終了しました。", "完了");
            }
            else
            {
                if (allFolder)
                {
                    MessageBox.Show(
                        $"取り込みに失敗したフォルダがあります。下記のエラー内容を確認してください。\r\n--------\r\n{errormessage}", 
                        "確認してください");
                }
                else
                {
                    MessageBox.Show(
                        $"取り込みに失敗しました。エラー内容は以下の通りです。\r\n--------\r\n{errormessage}", 
                        "エラー");
                }                
            }
        }

        private bool doImageImport(int iym, bool allFolder, out string errormessage)
        {
            bool result;
            errormessage = "";

            //取り込み中、プログレスバーの表示
            var wf = new WaitForm();
            try
            {
                wf.BarStyle = ProgressBarStyle.Continuous;
                wf.Max = imageFileCount;
                System.Threading.Tasks.Task.Factory.StartNew(() => wf.ShowDialog());
                while (!wf.Visible) System.Threading.Thread.Sleep(10);

                wf.LogPrint("画像取り込み処理を開始しました  フォルダ数は" + imageFolderCount + "です");

                //進捗度更新用
                Action<string> callback = filename =>
                {
                    wf.InvokeValue++;
                };

                //画像取込開始
                var si = new ScanImage();
                if (allFolder)
                {
                    //複数フォルダ
                    result = si.DoImagesImportInFolders(imageSubFolderPaths, iym, callback);
                }
                else
                {
                    //単一フォルダ
                    result = si.DoImagesImport(imageFolderPath, iym, callback);
                }
                if (!result)
                {
                    foreach (var em in si.ErrorMessages)
                    {
                        errormessage += em + "\r\n";
                    }
                    Log.ErrorWrite(errormessage);
                }
            }
            finally
            {
                wf.InvokeCloseDispose();
            }
            return result;
        }

        /// <summary>
        /// 処理年月でScanを抽出、表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            int y, m;
            int todayJY = DateTimeEx.GetJpYear(DateTime.Today);

            //データチェック
            if (!int.TryParse(textBoxYear.Text, out y))
            {
                MessageBox.Show("請求年を指定してください");
                return;
            }
            else if (y < todayJY - 1 || y > todayJY + 1)
            {
                MessageBox.Show("請求年度の指定が不正です\r\n(" + DateTimeEx.GetJpYear(DateTime.Today) + "±1)");
                return;
            }

            if (!int.TryParse(textBoxMonth.Text, out m))
            {
                MessageBox.Show("請求月を指定してください");
                return;
            }
            else if (m < 1 || m > 12)
            {
                MessageBox.Show("月の指定が不正です\r\n(1-12)");
                return;
            }

            //20190724145603 furukawa st ////////////////////////
            //令和対応
            
            var iym = DateTimeEx.GetAdYearFromWareki(y * 100 + m) * 100 + m;
            //var iym = DateTimeEx.GetAdYearFromHs(y) * 100 + m;
            //20190724145603 furukawa ed ////////////////////////



            updatePrefInfo(iym);

            foreach (DataGridViewColumn col in dataGridView1.Columns) col.Visible = false;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            DataGridViewColumn dgvc;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PID)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "番号";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PrefName)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "都道府県";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatusMaster)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "総合状況";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus1Str)];
            dgvc.HeaderText = "１回目";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus2Str)];
            dgvc.HeaderText = "ベリファイ";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.AllCount)];
            dgvc.HeaderText = "登録件数";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
        }

        private void updatePrefInfo(int iym)
        {
            var list = PrefInfo.GetPrefInfo(iym);

            bsPref.DataSource = list;
            dataGridView1.DataSource = bsPref;

            bsPref.ResetBindings(false);
        }

        /// <summary>
        /// ScanID単位でデータを削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int iym = 0;
            var delList = new List<int>();
            var message = "";
            foreach(DataGridViewRow row in dataGridView1.SelectedRows)
            {
                var pi = (PrefInfo)bsPref.List[row.Index];
                delList.Add(pi.PID);
                message += $"平成{pi.JY}年{pi.JM}月{pi.PrefName}\r\n";
                iym = pi.IYM;
            }


            var r = MessageBox.Show("選択された都道府県のデータを削除します。\r\n\r\n" +
                "この操作は現在作業している入力者に致命的な影響を及ぼします。\r\n" +
                "他の入力者がこの都道府県の作業をしていないことを必ず確認してください。\r\n\r\n" +
                $"以下ののデータを全て削除しますか？\r\n{message}", "重大な警告",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if(r == DialogResult.OK)
            {
                var tran = new DB.Transaction();

                //MediAppから削除
                try
                {
                    var maResult = MediApp.DeleteByIYMPID(iym, delList.ToArray(), tran);
                    if (!maResult)
                    {
                        throw new Exception("mediapp delete failed.");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    tran.Rollback();
                    MessageBox.Show("削除に失敗しました。\r\n状態を削除する前に戻しました。", "エラー");
                    return;
                }

                //MediAppInfoから削除
                try
                {
                    var maiResult = MediAppInfo.DeleteByIYMPID(iym, delList.ToArray(), tran);
                    if (!maiResult)
                    {
                        throw new Exception("mediappinfo delete failed.");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    tran.Rollback();
                    MessageBox.Show("削除に失敗しました。\r\n状態を削除する前に戻しました。", "エラー");
                    return;
                }

                //PrefInfoから削除
                try
                {
                    var piResult = PrefInfo.DeleteByIYMPref(iym, delList.ToArray(), tran);
                    if (!piResult)
                    {
                        throw new Exception("prefinfo delete failed.");
                    }
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    tran.Rollback();
                    MessageBox.Show("削除に失敗しました。\r\n状態を削除する前に戻しました。", "エラー");
                    return;
                }

                //画像削除
                foreach (var pn in delList)
                {
                    var delResult = FileUtility.FolderDelete(ScanImage.GetImageFolderPathToIYMPID(iym, pn));
                    if (!delResult)
                    {
                        tran.Rollback();
                        MessageBox.Show(
                            $"削除に失敗しました（都道府県番号：{pn.ToString("00")}）\r\n状態を削除する前に戻しました。", "エラー");
                        return;
                    }
                }                
                
                tran.Commit();
                MessageBox.Show($"以下のデータを全て削除しました。\r\n{message}", "削除完了");
                updatePrefInfo(iym);
            }
        }

    }
}
