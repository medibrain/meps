﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NpgsqlTypes;
using meps.Table;

namespace meps.Forms
{
    public partial class PrefSelectForm : Form
    {
        int iym;
        int ijy;
        int ijm;
        BindingSource bsPref = new BindingSource();
        string currentSelectColmunName;
        bool sortOver = true;

        public PrefSelectForm(int iym, int ijy, int ijm)
        {
            InitializeComponent();

            this.iym = iym;
            this.ijy = ijy;
            this.ijm = ijm;

            //20190724150232 furukawa st ////////////////////////
            //令和対応
            
            label1.Text = "平成/令和 " + ijy.ToString() + " 年 " + ijm.ToString() + " 月請求分";
            //label1.Text = "平成 " + ijy.ToString() + " 年 " + ijm.ToString() + " 月請求分";
            //20190724150232 furukawa ed ////////////////////////


            //リスト更新
            updateAllPrefInfo();

            //ヘッダ初期化
            foreach (DataGridViewColumn col in dataGridView1.Columns) col.Visible = false;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            DataGridViewColumn dgvc;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PID)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "番号";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.PrefName)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "都道府県";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatusMaster)];
            dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgvc.HeaderText = "総合状況";
            dgvc.Visible = true;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus1Str)];
            dgvc.HeaderText = "１回目";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputStatus2Str)];
            dgvc.HeaderText = "ベリファイ";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputUserName1)];
            dgvc.HeaderText = "１回目入力者";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.InputUserName2)];
            dgvc.HeaderText = "ﾍﾞﾘﾌｧｲ入力者";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.AllCount)];
            dgvc.HeaderText = "登録件数";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.Inputted1Count)];
            dgvc.HeaderText = "入力済";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;
            dgvc = dataGridView1.Columns[nameof(PrefInfo.Inputted2Count)];
            dgvc.HeaderText = "ベリファイ済";
            dgvc.Visible = true;
            dgvc.FillWeight = 1;

            //20190725112910 furukawa st ////////////////////////
            //試験サーバの場合色変える            
            Utils.CommonTool.setFormColor(this);
            //20190725112910 furukawa ed ////////////////////////
        }

        /// <summary>
        /// リストを全件更新します
        /// </summary>
        private void updateAllPrefInfo()
        {
            var piList = PrefInfo.GetPrefInfo(iym);

            bsPref.DataSource = piList;
            dataGridView1.DataSource = bsPref;

            updateProgressPercent(piList);

            bsPref.ResetBindings(false);
        }

        /// <summary>
        /// 現在の入力状況をパーセント表示します
        /// </summary>
        private void updateProgressPercent(List<PrefInfo> piList)
        {
            int inputtedCount = 0;
            int verifiedCount = 0;
            int allCount = 0;

            foreach (var pi in piList)
            {
                allCount += pi.AllCount;
                inputtedCount += pi.Inputted1Count;
                verifiedCount += pi.Inputted2Count;
            }

            float percentInputted = (float)inputtedCount / (float)allCount * (float)100;
            float percentVerified = (float)verifiedCount / (float)allCount * (float)100;

            var inputtedStr = $"入力済：{percentInputted.ToString("0.00")}%";
            var verifiedStr = $"ベリファイ済：{percentVerified.ToString("0.00")}%";

            labelProgressPercent.Text = $"{inputtedStr} / {verifiedStr}";
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// ソート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var piList = (List<PrefInfo>)bsPref.DataSource;
            var cName = dataGridView1.Columns[e.ColumnIndex].Name;

            var sort = currentSelectColmunName == cName;
            if (sort && sortOver) sort = false;
            sortOver = sort;
                switch (cName)
            {
                case nameof(PrefInfo.PID):
                    if (sort) piList.Sort((x, y) => { return x.PID.CompareTo(y.PID); });
                    else piList.Sort((x, y) => { return y.PID.CompareTo(x.PID); });
                    break;
                case nameof(PrefInfo.PrefName):
                    if (sort) piList.Sort((x, y) => { return x.PrefName.CompareTo(y.PrefName); });
                    else piList.Sort((x, y) => { return y.PrefName.CompareTo(x.PrefName); });
                    break;
                case nameof(PrefInfo.InputStatusMaster):
                    if (sort) piList.Sort((x, y) => { return x.InputStatusMaster.CompareTo(y.InputStatusMaster); });
                    else piList.Sort((x, y) => { return y.InputStatusMaster.CompareTo(x.InputStatusMaster); });
                    break;
                case nameof(PrefInfo.InputStatus1Str):
                    if (sort) piList.Sort((x, y) => { return x.InputStatus1Str.CompareTo(y.InputStatus1Str); });
                    else piList.Sort((x, y) => { return y.InputStatus1Str.CompareTo(x.InputStatus1Str); });
                    break;
                case nameof(PrefInfo.InputStatus2Str):
                    if (sort) piList.Sort((x, y) => { return x.InputStatus2Str.CompareTo(y.InputStatus2Str); });
                    else piList.Sort((x, y) => { return y.InputStatus2Str.CompareTo(x.InputStatus2Str); });
                    break;
                case nameof(PrefInfo.InputUserName1):
                    if (sort) piList.Sort((x, y) => { return x.InputUserName1.CompareTo(y.InputUserName1); });
                    else piList.Sort((x, y) => { return y.InputUserName1.CompareTo(x.InputUserName1); });
                    break;
                case nameof(PrefInfo.InputUserName2):
                    if (sort) piList.Sort((x, y) => { return x.InputUserName2.CompareTo(y.InputUserName2); });
                    else piList.Sort((x, y) => { return y.InputUserName2.CompareTo(x.InputUserName2); });
                    break;
                case nameof(PrefInfo.AllCount):
                    if (sort) piList.Sort((x, y) => { return x.AllCount.CompareTo(y.AllCount); });
                    else piList.Sort((x, y) => { return y.AllCount.CompareTo(x.AllCount); });
                    break;
                case nameof(PrefInfo.Inputted1Count):
                    if (sort) piList.Sort((x, y) => { return x.Inputted1Count.CompareTo(y.Inputted1Count); });
                    else piList.Sort((x, y) => { return y.Inputted1Count.CompareTo(x.Inputted1Count); });
                    break;
                case nameof(PrefInfo.Inputted2Count):
                    if (sort) piList.Sort((x, y) => { return x.Inputted2Count.CompareTo(y.Inputted2Count); });
                    else piList.Sort((x, y) => { return y.Inputted2Count.CompareTo(x.Inputted2Count); });
                    break;
            }

            currentSelectColmunName = cName;
            bsPref.ResetBindings(false);
        }

        /// <summary>
        /// 入力開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInputStart_Click(object sender, EventArgs e)
        {
            var pi = (PrefInfo)bsPref.Current;
            if (pi.InputtingStatus==(int)Consts.INPUTTING_STATUS.入力中)
            {
                MessageBox.Show($"このデータは現在 {User.GetUser(pi.LastInputUser).Name} が入力中です。", "１回目入力");

                //20190724153338 furukawa st ////////////////////////
                //入力中解除
                
                if (MessageBox.Show("入力中を解除しますか？", "1回目入力", 
                    MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    pi.CheckOut();
                }
                else
                {
                    return;
                }

                //                return;

                //20190724153338 furukawa ed ////////////////////////

            }

            try
            {
                //入力開始
                pi.CheckIn(User.CurrentUser.UserID);
                inputStart(pi, false);
            }
            finally
            {
                //入力終了
                pi.CheckOut();
            }
            updateAllPrefInfo();
        }

        /// <summary>
        /// ベリファイ開始
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonVerify_Click(object sender, EventArgs e)
        {
            var pi = (PrefInfo)bsPref.Current;
            if (pi.InputStatus1 != Consts.INPUT_STATUS.入力済)
            {
                MessageBox.Show("１回目の入力がまだ完了していません。", "ベリファイ入力");
                return;
            }
            if (pi.InputtingStatus == (int)Consts.INPUTTING_STATUS.入力中)
            {
                MessageBox.Show($"このデータは現在 {User.GetUser(pi.LastInputUser).Name} が入力中です。", "ベリファイ入力");
                return;
            }

            try
            {
                //ベリファイ開始
                pi.CheckIn(User.CurrentUser.UserID);
                inputStart(pi, true);
            }
            finally
            {
                //ベリファイ終了
                pi.CheckOut();
            }
            updateAllPrefInfo();
        }

        /// <summary>
        /// 入力開始（true=ベリファイモード）
        /// </summary>
        /// <param name="prefinfo"></param>
        /// <param name="verify"></param>
        private void inputStart(PrefInfo prefinfo, bool verify)
        {
            //表示位置を記憶
            var firstIndex = dataGridView1.FirstDisplayedScrollingRowIndex;

            var pnColIndex = dataGridView1.Columns[nameof(PrefInfo.PID)].Index;

            //選択行を記録
            int row = dataGridView1.CurrentCellAddress.Y;
            int cellValue = (int)(dataGridView1[pnColIndex, row].Value);

            //入力開始
            var pi = (PrefInfo)bsPref.Current;
            var piIndex = bsPref.Position;
            using (var f = new RezeptInputForm(iym, ijy, ijm, pi.PID, pi.PrefName, verify))
            {
                f.ShowDialog();
            };

            //パフォーマンス向上：ロジック停止
            dataGridView1.SuspendLayout();

            //選択行を、直前まで作業していた行に移す
            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if((int)dataGridView1[pnColIndex, i].Value == cellValue)
                {
                    dataGridView1[pnColIndex, i].Selected = true;
                    break;
                }
            }

            //表示位置を元に戻す
            dataGridView1.FirstDisplayedScrollingRowIndex = firstIndex;

            //パフォーマンス向上：ロジック再開
            dataGridView1.ResumeLayout();
        }

        /// <summary>
        /// リスト更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            updateAllPrefInfo();
        }

        /// <summary>
        /// リスト更新(F5)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrefSelectForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5) updateAllPrefInfo();
        }
    }
}