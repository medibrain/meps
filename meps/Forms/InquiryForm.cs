﻿using meps.Table;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace meps.Forms
{
    public partial class InquiryForm : Form
    {
        BindingSource bsInquiry = new BindingSource();

        public InquiryForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// データ取込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonImport_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Medi-very
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonMediVery_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOutput_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 簡易検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSimpleSearch_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int iym = 0;
            var delList = new List<int>();
            var message = "";
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                var pi = (Inquiry)bsInquiry.List[row.Index];
                delList.Add(pi.PID);
                //message += $"平成{pi.JY}年{pi.JM}月{pi.PrefName}\r\n";
                //iym = pi.IYM;
            }


            var r = MessageBox.Show("選択された都道府県のデータを削除します。\r\n\r\n" +
                "この操作は現在作業している入力者に致命的な影響を及ぼします。\r\n" +
                "他の入力者がこの都道府県の作業をしていないことを必ず確認してください。\r\n\r\n" +
                $"以下ののデータを全て削除しますか？\r\n{message}", "重大な警告",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
        }

        /// <summary>
        /// 審査年月で再検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchYM_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 都道府県で検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSearchPref_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 終了
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonExit_Click(object sender, EventArgs e)
        {

        }
    }
}
