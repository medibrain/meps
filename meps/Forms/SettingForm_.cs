﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Printing;
using meps.Table;
using meps.Utils;

namespace meps.Forms
{
    public partial class SettingForm : Form
    {
        bool hostSetting;
        BindingSource bs = new BindingSource();

        public SettingForm(bool hostSetting = false)
        {
            InitializeComponent();

            if (hostSetting) initHostSetting();
            else initNormal();

            this.hostSetting = hostSetting;
        }

        /// <summary>
        /// 初期設定させる
        /// </summary>
        private void initHostSetting()
        {
            textBoxDataIP.Text = Settings.DataBaseHost;
            textBoxImageIP.Text = Settings.ImageHost;
            textBoxTimeout.Text = Settings.CommandTimeout;

            string defPrinterName;
            using (var pd = new PrintDocument())
            {
                defPrinterName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == defPrinterName)
                    comboBox1.SelectedIndex = i;
            }

            var pn = Settings.DefaultPrinterName;
            if (comboBox1.Items.Contains(pn)) comboBox1.SelectedItem = pn;

            dataGridViewUser.Enabled = false;
            textBoxUserID.Enabled = false;
            textBoxuserName.Enabled = false;
            textBoxUserPass.Enabled = false;
            textBoxUserRoman.Enabled = false;
            checkBoxEnable.Enabled = false;
            checkBoxAdmin.Enabled = false;
            buttonAddUser.Enabled = false;
        }

        /// <summary>
        /// 通常処理
        /// </summary>
        private void initNormal()
        {
            User.GetUsers();

            textBoxDataIP.Text = Settings.DataBaseHost;
            textBoxImageIP.Text = Settings.ImageHost;
            textBoxTimeout.Text = Settings.CommandTimeout;

            dataGridViewUser.DataSource = bs;
            UserListUpdate();
            dataGridViewUser.Columns[nameof(User.Pass)].Visible = false;
            bs.CurrentItemChanged += Bs_CurrentItemChanged;
            setUser();

            string defPrinterName;
            using (var pd = new PrintDocument())
            {
                defPrinterName = pd.PrinterSettings.PrinterName;
            }

            for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            {
                comboBox1.Items.Add(PrinterSettings.InstalledPrinters[i]);
                if (PrinterSettings.InstalledPrinters[i] == defPrinterName)
                    comboBox1.SelectedIndex = i;
            }

            var pn = Settings.DefaultPrinterName;
            if (comboBox1.Items.Contains(pn)) comboBox1.SelectedItem = pn;
        }

        private void Bs_CurrentItemChanged(object sender, EventArgs e)
        {
            setUser();
        }

        private void setUser()
        {
            var u =(User)bs.Current;

            if (u.UserID == 0)
            {
                textBoxUserID.Clear();
                textBoxUserID.ReadOnly = false;
                textBoxuserName.Clear();
                textBoxUserPass.Clear();
                textBoxUserRoman.Clear();
                checkBoxEnable.Checked = true;
                checkBoxAdmin.Checked = false;
                buttonAddUser.Text = "追加";
            }
            else
            {
                textBoxUserID.Text = u.UserID.ToString();
                textBoxUserID.ReadOnly = true;
                textBoxuserName.Text = u.Name;
                textBoxUserPass.Text = u.Pass;
                textBoxUserRoman.Text = u.LoginName;
                checkBoxEnable.Checked = u.Enable;
                checkBoxAdmin.Checked = u.Admin;
                buttonAddUser.Text = "変更";
            }


            buttonAddUser.Enabled = true;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (hostSetting)
            {
                if (!DB.ConnectTest(textBoxDataIP.Text))
                {
                    MessageBox.Show("指定された【Server IP Address】は利用できません", "エラー");
                    textBoxDataIP.Focus();
                    return;
                }

                var imagefolder = $"{textBoxImageIP.Text}\\scan\\";
                if (!FileUtility.FolderExist(imagefolder))
                {
                    MessageBox.Show("指定された【Image IP Address】は利用できません", "エラー");
                    textBoxImageIP.Focus();
                    return;
                }

                DB.UpdateDatabaseHost(textBoxDataIP.Text);
            }

            Settings.DataBaseHost = textBoxDataIP.Text;
            Settings.ImageHost = textBoxImageIP.Text;
            Settings.CommandTimeout = Converter.ToInt(textBoxTimeout.Text).ToString();
            Settings.DefaultPrinterName = (string)comboBox1.SelectedItem;

            Properties.Settings.Default.Save();

            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// User管理機能(追加・変更のみ)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddUser_Click(object sender, EventArgs e)
        {
            //データチェック
            if (textBoxUserID.Text == "" || textBoxuserName.Text == ""
                || textBoxUserRoman.Text == "" || textBoxUserPass.Text == "")
            {
                MessageBox.Show("空白の欄があります");
                return;
            }
            
            int userid;
            userid = int.TryParse(textBoxUserID.Text, out userid) ? userid : 0;
            if (userid == 0)
            {
                MessageBox.Show("UserIDには数字のみ入力してください");
                return;
            }

            bool update = true;
            if (((User)bs.Current).UserID == 0)
            {
                if (User.GetList().FirstOrDefault(item => item.UserID == userid) != null)
                {
                    MessageBox.Show("指定されたIDは既に使用されています");
                    return;
                }
                update = false;
            }

            var msg = (update ? "選択中のユーザーを更新します。" : "新規ユーザーを登録します。") + "よろしいですか？";
            if (MessageBox.Show(msg, "ユーザー確認", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) !=
                 DialogResult.OK) return;

            if (update)
            {
                var u = User.GetUser(userid);
                u.Name = textBoxuserName.Text;
                u.LoginName = textBoxUserRoman.Text;
                u.Pass = textBoxUserPass.Text;
                u.Enable = checkBoxEnable.Checked;
                u.Admin = checkBoxAdmin.Checked;
                u.Update();
            }
            else
            {
                var u = new User(userid, textBoxuserName.Text);
                u.LoginName = textBoxUserRoman.Text;
                u.Pass = textBoxUserPass.Text;
                u.Enable = checkBoxEnable.Checked;
                u.Admin = checkBoxAdmin.Checked;
                u.Insert();
            }

            //表示を更新
            UserListUpdate();
            MessageBox.Show(update ? "更新しました" : "登録しました");
        }

        //Userリスト表示の更新
        private void UserListUpdate()
        {
            User.GetUsers();
            var l = User.GetList();

            l.Add(new User(0, "新規"));
            l.Sort((x, y) => x.UserID.CompareTo(y.UserID));
            bs.DataSource = l;
        }

        private void FormSetting_Shown(object sender, EventArgs e)
        {
            if(!hostSetting)
            {
                if (User.CurrentUser.Admin) return;
                MessageBox.Show("現在ログインしているユーザーには管理権限がありません", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.Close();
            }
        }
    }
}
