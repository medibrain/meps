﻿using meps.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace meps.Controls
{
    public class VerifyBoxCore : TextBox
    {
        [DllImport("imm32.dll")]
        static extern int ImmGetVirtualKey(int hwnd);

        const int WM_KEYDOWN = 0x100;
        const int VK_PROCESSKEY = 0xE5;

        //event デリゲート型 イベント ハンドラー名;
        public delegate void ProcessKeyDownEventHandrer(Keys key);
        public event ProcessKeyDownEventHandrer ProcessCmdKeyDown;

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (msg.Msg.Equals(WM_KEYDOWN))
            {
                if (msg.WParam.ToInt32().Equals(VK_PROCESSKEY))
                {
                    int key = ImmGetVirtualKey((int)this.Handle);
                    if (ProcessCmdKeyDown != null) ProcessCmdKeyDown((Keys)key);
                }
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }

    public partial class VerifyBox : VerifyBoxCore
    {
        public VerifyBoxCore box = new VerifyBoxCore();
        bool _verifyLocationVertical = false;
        bool _required = false;
        int _maxLength = 0;
        TEXT_MODE _textMode = TEXT_MODE.INT;
        DATE_MODE _dateMode = DATE_MODE.NotDate;
        TEXTALIGN_LOCATION _textAlign = TEXTALIGN_LOCATION.RIGHT;
        public enum TEXT_MODE { INT, NUM, DATE, MULTI, TEXT, ETC }//NUM:数値だがint化は不可, MULTI:複数回答, ETC:その他
        public enum DATE_MODE { NotDate, AD, JP, BOTH }
        public enum TEXTALIGN_LOCATION { CENTER, LEFT, RIGHT }


        /// <summary>
        /// ベリファイ用テキストボックスの表示位置が垂直位置かを指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        [System.ComponentModel.Description("ベリファイ用テキストボックスの表示位置が垂直位置かを指定または取得します")]
        public bool VerifyLocationVertical
        {
            get { return _verifyLocationVertical; }
            set
            {
                _verifyLocationVertical = value;
                boxLocationChange();
            }
        }

        /// <summary>
        /// テキストボックスの日付表示方法を指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(DATE_MODE.NotDate)]
        [System.ComponentModel.Description("テキストボックスの日付表示方法を指定または取得します")]
        public virtual DATE_MODE DateMode
        {
            get { return _dateMode; }
            set
            {
                _dateMode = value;
                boxLocationChange();
            }
        }

        /// <summary>
        /// 入力必須項目かどうかを指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(true)]
        [System.ComponentModel.Description("入力必須項目かどうかを指定または取得します")]
        public bool Required
        {
            get { return _required; }
            set
            {
                _required = value;
            }
        }

        /// <summary>
        /// 入力モードを指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(TEXT_MODE.INT)]
        [System.ComponentModel.Description("入力のタイプを指定または取得します")]
        public TEXT_MODE TextMode
        {
            get { return _textMode; }
            set
            {
                _textMode = value;
            }
        }

        /// <summary>
        /// ベリファイ用TextBoxのVisibleを操作します
        /// ※開発GUI環境から操作できるように外に出しています
        /// </summary>
        [System.ComponentModel.DefaultValue(false)]
        [System.ComponentModel.Description("開発GUI環境上での確認用に使用します")]
        public bool BoxVisible
        {
            get { return box.Visible; }
            set
            {
                box.Visible = value;
            }
        }

        /// <summary>
        /// 横方向における文字の表示位置を指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(TEXTALIGN_LOCATION.RIGHT)]
        [System.ComponentModel.Description("横方向における文字の表示位置を指定または取得します")]
        public TEXTALIGN_LOCATION TextAlignLocation
        {
            get { return _textAlign; }

            set
            {
                switch (value)
                {
                    case TEXTALIGN_LOCATION.CENTER:
                        _textAlign = value;
                        this.TextAlign = HorizontalAlignment.Center;
                        box.TextAlign = HorizontalAlignment.Center;
                        break;
                    case TEXTALIGN_LOCATION.LEFT:
                        _textAlign = value;
                        this.TextAlign = HorizontalAlignment.Left;
                        box.TextAlign = HorizontalAlignment.Left;
                        break;
                    case TEXTALIGN_LOCATION.RIGHT:
                        _textAlign = value;
                        this.TextAlign = HorizontalAlignment.Right;
                        box.TextAlign = HorizontalAlignment.Right;
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 最大文字数を指定または取得します
        /// </summary>
        [System.ComponentModel.DefaultValue(0)]
        [System.ComponentModel.Description("最大文字数を指定または取得します")]
        public int TextMaxLength
        {
            get { return _maxLength; }
            set
            {
                _maxLength = value;
                this.MaxLength = value;
                box.MaxLength = value;
            }
        }

        /// <summary>
        /// 現在ベリファイ中であることを示します。
        /// </summary>
        public bool IsVerifying { get; set; } = false;





        public VerifyBox()
        {
            InitializeComponent(); this.BackColor = SystemColors.Info;
            BoxVisible = false;
            box.BackColor = Color.MistyRose;
            this.LocationChanged += VerifyBox_LocationChanged;
            this.ParentChanged += VerifyBox_ParentChanged;
            this.TabIndexChanged += VerifyBox_TabIndexChanged;
            this.FontChanged += VerifyBox_FontChanged;
            this.SizeChanged += VerifyBox_SizeChanged;
            this.Leave += Box_Leave;
            this.Enter += Box_Enter;
            this.KeyDown += Box_KeyDown;
            box.Leave += Box_Leave;
            box.Enter += Box_Enter;
            box.KeyDown += Box_KeyDown;
            this.Font = new Font("Meiryo UI", 11);
            this.IsError = false;
        }

        /// <summary>
        /// 現在設定されているデータがエラーかどうかを取得・設定します
        /// </summary>
        public bool IsError { get; private set; }
        /// <summary>
        /// ベリファイエラーかどうかを取得・設定します
        /// </summary>
        public bool IsVerifyError { get; private set; }

        void Box_KeyDown(object sender, KeyEventArgs e)
        {
            var key = e.KeyCode;
            switch (key)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");//戻る
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");//進む
                    break;
            }
        }

        void Box_Enter(object sender, EventArgs e)
        {
            //this.BackColor = SystemColors.ControlDark;
            //this.ForeColor = Color.White;
            var t = (TextBox)sender;
            t.SelectAll();
            if (DateMode == DATE_MODE.NotDate) return;

            if (DateMode == DATE_MODE.AD)
            {
                if (t.Text.Length == 10 && t.Text.IndexOf('/') == 4 && t.Text.LastIndexOf('/') == 7)
                {
                    t.Text = t.Text.Replace("/", "");
                }
            }
            else if (DateMode == DATE_MODE.JP)
            {
                if (t.Text.Length == 9 && t.Text.IndexOf('/') == 3 && t.Text.LastIndexOf('/') == 6)
                {
                    t.Text = t.Text.Replace("/", "");
                }
            }
        }

        public void Box_Leave(object sender, EventArgs e)
        {
            DoCheck((TextBox)sender);
        }

        /// <summary>
        /// 入力値のチェックを行います。
        /// </summary>
        public void DoCheck(TextBox t)
        {
            //this.BackColor = SystemColors.Window;
            //this.ForeColor = SystemColors.WindowText;

            if (t == this)
            {
                this.IsError = false;
                SetRight();

                if (Required && t.Text.Trim() == string.Empty)
                {
                    SetError();
                    return;
                }
                else if (!Required && t.Text.Trim() == string.Empty)
                {
                    this.BackColor = SystemColors.Window;
                    return;
                }
            }

            if (DateMode == DATE_MODE.NotDate && t == this)
            {
                //警告範囲、エラー範囲が指定されていれば表示変更
                decimal v;
                int l = t.Text.Trim().Length;
                var intDcm = t.Text.Trim().Split('.');
                decimal.TryParse(t.Text, out v);
                if (!Required && t.Text.Trim() == string.Empty) this.BackColor = SystemColors.Window;
                else
                {
                    if (warningMax != -1 && warningMax < v) SetWarning();
                    if (warningMin != -1 && warningMin > v) SetWarning();
                    if (warningMaxLength != -1 && warningMaxLength < l) SetWarning();
                    if (warningMinLength != -1 && warningMinLength > l) SetWarning();
                    if (errorMax != -1 && errorMax < v) SetError();
                    if (errorMin != -1 && errorMin > v) SetError();
                    if (errorMaxLength != -1 && errorMaxLength < l) SetError();
                    if (errorMinLength != -1 && errorMinLength > l) SetError();
                    if (warningIntMaxLength != -1 && intDcm.Length > 0 && warningIntMaxLength < intDcm[0].Length) SetWarning();
                    //if (warningDecimalMaxLength != -1 && intDcm.Length > 1 && warningDecimalMaxLength < intDcm[1].Length) SetWarning();
                    if (errorIntMaxLength != -1 && intDcm.Length > 0 && errorIntMaxLength < intDcm[0].Length) SetError();
                    //if (errorDecimalMaxLength != -1 && intDcm.Length > 1 && errorDecimalMaxLength < intDcm[1].Length) SetError();

                    //NUMであれば最長文字数まで０埋め
                    if (TextMaxLength > 0 && TextMode == TEXT_MODE.NUM && this.Text != "")
                    {
                        SetIntValue(GetIntValue());
                    }
                }                
            }
            else
            {
                DateTime dt = DateTime.MinValue;
                if (DateMode == DATE_MODE.AD)
                {
                    if (!convertAD(t, out dt) && t == this)
                    {
                        SetError();
                        return;
                    }
                }
                else if (DateMode == DATE_MODE.BOTH)
                {
                    if (!convertBOTH(t, out dt) && t == this)
                    {
                        SetError();
                        return;
                    }
                }
                else if (DateMode == DATE_MODE.JP)
                {
                    if (!convertJP(t, out dt) && t == this)
                    {
                        SetError();
                        return;
                    }
                }

                if (t == this)
                {
                    //警告/エラーチェック
                    if (warningMaxDt != DateTime.MinValue && warningMaxDt < dt) SetWarning();
                    if (warningMinDt != DateTime.MinValue && warningMinDt > dt) SetWarning();
                    if (errorMaxDt != DateTime.MinValue && errorMaxDt < dt) SetError();
                    if (errorMinDt != DateTime.MinValue && errorMinDt > dt) SetError();
                }
            }

            //ベリファイ中ならベリファイも行う
            if (IsVerifying)
            {
                if (t == box)
                {
                    //boxも最長文字数まで０埋め
                    if (TextMaxLength > 0 && TextMode == TEXT_MODE.NUM && box.Text != "")
                    {
                        int val;
                        int.TryParse(box.Text, out val);
                        SetIntVerifyValue(val);
                    }
                }                
                //ベリファイ
                CheckVerify();
            }
        }

        private bool convertAD(TextBox t, out DateTime dt)
        {
            dt = DateTime.MinValue;
            if (t.Text.Trim().Length != 8)
            {
                return false;
            }
            int y, m, d;
            int.TryParse(t.Text.Substring(0, 4), out y);
            int.TryParse(t.Text.Substring(4, 2), out m);
            int.TryParse(t.Text.Substring(6, 2), out d);
            if (!DateTimeEx.IsDate(y, m, d))
            {
                return false;
            }
            dt = new DateTime(y, m, d);
            t.Text = dt.ToString("yyyy/MM/dd");
            return true;
        }

        private bool convertBOTH(TextBox t, out DateTime dt)
        {
            dt = DateTime.MinValue;
            string d;
            d = t.Text.Trim().Replace("/", "");
            //文字列が7桁の場合は和暦と判断する
            if (d.Count() == 7)
            {
                //先頭文字を判定
                string dh = d.Substring(0, 1).ToUpper();
                if (dh == "H") d.Replace("H", "4");
                else if (dh == "S") d.Replace("S", "3");
                else if (dh == "T") d.Replace("T", "2");
                else if ("1234567890".Contains(dh)) { }
                else return false;

                int d1 = 0;
                if (!int.TryParse(d, out d1)) { };

                if (d1 >= 2 || d1 <= 4)
                {
                    dt = DateTimeEx.GetDateFromJstr7(d);
                    t.Text = dt.ToString("yyyy/MM/dd");
                }
                else
                {
                    return false;
                }
            }
            //文字列が8桁の場合は西暦と判断する
            else if (d.Count() == 8)
            {
                dt = d.ToDateTime();
                t.Text = dt.ToString("yyyy/MM/dd");
            }
            else
            {
                return false;
            }
            return true;
        }

        private bool convertJP(TextBox t, out DateTime dt)
        {
            dt = DateTime.MinValue;
            if (t.Text.Trim().Length != 7)
            {
                if (t.Text.Length == 9 && t.Text.IndexOf('/') == 3 && t.Text.LastIndexOf('/') == 6)
                {
                    t.Text = t.Text.Replace("/", "");
                    if (convertJP(t, out dt))
                    {
                        return true;
                    }
                }
                return false;
            }
            var c1 = t.Text[0].ToString().ToUpper();

            //20190724153239 furukawa st ////////////////////////
            //令和対応
            
            if (!"1234567890".Contains(c1) && c1 != "R" && c1 != "H" && c1 != "S" && c1 != "T") return false;
            //if (!"1234567890".Contains(c1) && c1 != "H" && c1 != "S" && c1 != "T") return false;
            //20190724153239 furukawa ed ////////////////////////


            dt = DateTimeEx.GetDateFromJstr7(t.Text.ToUpper());
            if (dt == DateTime.MinValue)
            {
                return false;
            }
            t.Text = DateTimeEx.GetShortJpDateStr(dt);
            return true;
        }

        public void SetNormal()
        {
            this.BackColor = SystemColors.Window;
            this.IsError = false;
        }

        public void SetRight()
        {
            this.BackColor = Color.LightCyan;
            this.IsError = false;
        }

        public void SetError()
        {
            this.BackColor = Color.MistyRose;
            this.IsError = true;
        }

        public void SetWarning()
        {
            this.BackColor = Color.LightYellow;
            this.IsError = false;
        }

        decimal warningMin = -1, warningMax = -1, errorMin = -1, errorMax = -1;
        /// <summary>
        /// 警告、およびエラー範囲を指定します　-1で設定なしとなります
        /// </summary>
        /// <param name="wMin"></param>
        /// <param name="wMax"></param>
        /// <param name="eMin"></param>
        /// <param name="eMax"></param>
        public void SetErrorAndWarningRange(decimal wMin, decimal wMax, decimal eMin, decimal eMax)
        {
            warningMin = wMin;
            warningMax = wMax;
            errorMin = eMin;
            errorMax = eMax;
        }

        int warningMinLength = -1, warningMaxLength = -1, errorMinLength = -1, errorMaxLength = -1;
        /// <summary>
        /// 警告、およびエラー文字数を指定します　-1で設定なしとなります
        /// </summary>
        /// <param name="wMin"></param>
        /// <param name="wMax"></param>
        /// <param name="eMin"></param>
        /// <param name="eMax"></param>
        public void SetErrorAndWarningLength(int wMin, int wMax, int eMin, int eMax)
        {
            warningMinLength = wMin;
            warningMaxLength = wMax;
            errorMinLength = eMin;
            errorMaxLength = eMax;
        }

        int warningIntMaxLength = -1, warningDecimalMaxLength = -1, errorIntMaxLength = -1, errorDecimalMaxLength = -1;
        /// <summary>
        /// 警告、エラーとなる整数部分の長さ、少数部分の長さを指定します(ドットは含めません)　-1で指定なしとなります
        /// </summary>
        /// <param name="eIntMax"></param>
        /// <param name="eDcmMax"></param>
        /// <param name="wIntMax"></param>
        /// <param name="wDcmMax"></param>
        public void SetErrorAndWarningIntDecimalLength(int wIntMax, int wDcmMax, int eIntMax, int eDcmMax)
        {
            warningIntMaxLength = wIntMax;
            warningDecimalMaxLength = wDcmMax;
            errorIntMaxLength = eIntMax;
            errorDecimalMaxLength = eDcmMax;
        }

        DateTime warningMinDt = DateTime.MinValue, warningMaxDt = DateTime.MinValue, errorMinDt = DateTime.MinValue, errorMaxDt = DateTime.MinValue;
        /// <summary>
        /// 日付の警告、およびエラー範囲を指定します　DateTime.MinValueで設定なしとなります
        /// </summary>
        /// <param name="wMin"></param>
        /// <param name="wMax"></param>
        /// <param name="eMin"></param>
        /// <param name="eMax"></param>
        public void SetErrorAndWarningDt(DateTime wMin, DateTime wMax, DateTime eMin, DateTime eMax)
        {
            warningMinDt = wMin;
            warningMaxDt = wMax;
            errorMinDt = eMin;
            errorMaxDt = eMax;
        }


        void VerifyBox_SizeChanged(object sender, EventArgs e)
        {
            box.Size = this.Size;
            boxLocationAdjust();
        }

        void VerifyBox_FontChanged(object sender, EventArgs e)
        {
            box.Font = this.Font;
        }

        void VerifyBox_TabIndexChanged(object sender, EventArgs e)
        {
            box.TabIndex = this.TabIndex;
        }

        void VerifyBox_ParentChanged(object sender, EventArgs e)
        {
            box.Parent = this.Parent;
        }

        void VerifyBox_LocationChanged(object sender, EventArgs e)
        {
            boxLocationChange();
        }

        /// <summary>
        /// ベリファイ用テキストボックスの表示位置を調整します
        /// </summary>
        private void boxLocationChange()
        {
            boxLocationAdjust();
        }

        private void boxLocationAdjust()
        {
            if (VerifyLocationVertical)
            {
                box.Location = new Point(this.Location.X, this.Location.Y + this.Height);
            }
            else
            {
                box.Location = new Point(this.Location.X + this.Width, this.Location.Y);
            }
        }

        /// <summary>
        /// テキストボックス、およびベリファイの表示を元に戻します
        /// </summary>
        public void AllClear()
        {
            this.Clear();
            box.Clear();
            BoxVisible = false;
            this.BackColor = SystemColors.Window;
            IsError = false;
            IsVerifyError = false;
            IsVerifying = false;
        }

        /// <summary>
        /// int型の範囲チェックをします
        /// </summary>
        /// <param name="minValue">最小値</param>
        /// <param name="maxValue">最大値</param>
        /// <returns>エラーの場合、false</returns>
        public bool CheckInt(int minValue, int maxValue)
        {
            int temp;
            if (!int.TryParse(this.Text, out temp) ||
                temp < minValue || maxValue < temp)
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
            this.BackColor = SystemColors.Window;
            return true;
        }

        /// <summary>
        /// decimal型の範囲チェックをします
        /// </summary>
        /// <param name="minValue">最小値</param>
        /// <param name="maxValue">最大値</param>
        /// <returns>エラーの場合、false</returns>
        public bool CheckDecimal(decimal minValue, decimal maxValue)
        {
            decimal temp;
            if (!decimal.TryParse(this.Text, out temp) ||
                temp < minValue || maxValue < temp)
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
            this.BackColor = SystemColors.Window;
            return true;
        }


        /// <summary>
        /// 文字数をチェックします
        /// </summary>
        /// <param name="minLength"></param>
        /// <returns>エラーの場合、false</returns>
        public bool CheckLength(int minLength)
        {
            if (minLength <= this.Text.Trim().Length)
            {
                this.BackColor = SystemColors.Window;
                return true;
            }
            else
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
        }

        /// <summary>
        /// 文字数をチェックします
        /// </summary>
        /// <param name="minLength"></param>
        /// <returns>エラーの場合、false</returns>
        public bool CheckLength(int minLength, int maxLength)
        {
            var length = this.Text.Trim().Length;
            if (minLength <= length || length <= maxLength)
            {
                this.BackColor = SystemColors.Window;
                return true;
            }
            else
            {
                this.BackColor = Color.MistyRose;
                return false;
            }
        }


        /// <summary>
        /// ベリファイチェックをします
        /// </summary>
        /// <returns>エラーの場合、false</returns>
        public virtual bool CheckVerify()
        {
            if (this.Text.Trim() == box.Text.Trim())
            {
                BoxVisible = false;
                IsVerifyError = false;
                return true;
            }
            else
            {
                ////小数点以下の０有無判定
                //decimal tTxt, bTxt;
                //if (!decimal.TryParse(this.Text.Trim(), out tTxt))
                //{
                //    BoxVisible = true;
                //    return false;
                //}
                //if (!decimal.TryParse(box.Text.Trim(), out bTxt))
                //{
                //    BoxVisible = true;
                //    return false;
                //}
                //if (tTxt == bTxt)
                //{
                //    BoxVisible = false;
                //    return true;
                //}
                //else
                //{
                    BoxVisible = true;
                    IsVerifyError = true;
                return false;
                //}
            }
        }

        /// <summary>
        /// int型のデータを取得します。
        /// 失敗した場合、-1が返ります。
        /// </summary>
        /// <returns></returns>
        public int GetIntValue()
        {
            int temp;
            if (!int.TryParse(this.Text, out temp)) return -1;
            return temp;
        }

        /// <summary>
        /// 1000倍したint型のデータが返ります。
        /// 失敗した場合、-1が返ります。
        /// </summary>
        /// <returns></returns>
        public int GetInt1000Value()
        {
            decimal temp;
            if (!decimal.TryParse(this.Text, out temp)) return -1;
            return (int)(temp * 1000);
        }

        /// <summary>
        /// string型のデータを取得します。
        /// </summary>
        /// <returns></returns>
        public string GetTextValue()
        {
            return this.Text.Trim();
        }

        /// <summary>
        /// DateTime型の日付を取得します
        /// DateModeが日付でない場合、また日付に変換できない場合、DateTime.MinValueを返します
        /// </summary>
        /// <returns></returns>
        public DateTime GetDateTimeValue()
        {
            if (DateMode == DATE_MODE.JP)
            {
                return DateTimeEx.GetDateFromJstr7(this.Text.Trim().Replace("/", ""));
            }
            else if (DateMode == DATE_MODE.AD)
            {
                return this.Text.Replace("/", "").Trim().ToDateTime();
            }
            //2016/5/20 下記、伊藤追加
            else if (DateMode == DATE_MODE.BOTH)
            {
                DateTime dt;
                string d;
                d = this.Text.Trim().Replace("/", "");
                //文字列が7桁の場合は和暦と判断する
                if (d.Count() == 7)
                {
                    //先頭文字を判定
                    string dh = d.Substring(0, 1).ToUpper();
                    if (dh == "H") d.Replace("H", "4");
                    else if (dh == "S") d.Replace("S", "3");
                    else if (dh == "T") d.Replace("T", "2");

                    int d1 = 0;
                    if (!int.TryParse(d, out d1)) return DateTime.MinValue;

                    if (d1 >= 2 || d1 <= 4)
                    {
                        dt = DateTimeEx.GetDateFromJstr7(d);
                        return dt;
                    }
                }
                //文字列が8桁の場合は西暦と判断する
                else if (d.Count() == 8)
                {
                    dt = d.ToDateTime();
                    return dt;
                }
            }

            return DateTime.MinValue;
        }

        public void SetIntValue(int value)
        {
            if (value == -1)
            {
                this.Text = "";
                return;
            }
            string need0 = "";
            for (var i = 0; i < TextMaxLength; i++) need0 += "0";
            if (TextMode == TEXT_MODE.NUM) this.Text = value.ToString(need0);
            else this.Text = value.ToString();
        }

        public void SetInt1000Value(int value)
        {
            if (value == -1)
            {
                this.Text = "";
                return;
            }
            string need0 = "";
            for (var i = 0; i < TextMaxLength; i++) need0 += "0";
            if (TextMode == TEXT_MODE.NUM) this.Text = (value / 1000m).ToString(need0);
            else this.Text = (value / 1000m).ToString();
        }

        public void SetIntVerifyValue(int value)
        {
            if (value == -1)
            {
                box.Text = "";
                return;
            }
            string need0 = "";
            for (var i = 0; i < TextMaxLength; i++) need0 += "0";
            if (TextMode == TEXT_MODE.NUM) box.Text = value.ToString(need0);
            else box.Text = value.ToString();
        }

        public void SetInt1000VerifyValue(int value)
        {
            if (value == -1)
            {
                box.Text = "";
                return;
            }
            string need0 = "";
            for (var i = 0; i < TextMaxLength; i++) need0 += "0";
            if (TextMode == TEXT_MODE.NUM) box.Text = (value / 1000m).ToString(need0);
            else box.Text = (value / 1000m).ToString();
        }

        public void SetTextValue(string value)
        {
            if (value == null)
            {
                this.Text = "";
                return;
            }
            this.Text = value;
        }

        public void SetTextVerifyValue(string value)
        {
            if (value == null)
            {
                box.Text = "";
                return;
            }
            box.Text = value;
        }

        public void SetDateValue(DateTime dt)
        {
            if (DateMode == DATE_MODE.NotDate)
            {
                this.Clear();
            }
            else if (DateMode == DATE_MODE.JP)
            {
                this.Text = DateTimeEx.GetShortJpDateStr(dt);
            }
            else if (DateMode == DATE_MODE.AD)
            {
                this.Text = dt.ToString("yyyy/MM/dd");
            }
            else if (DateMode == DATE_MODE.BOTH)
            {
                this.Text = dt.ToString("yyyy/MM/dd");
            }
        }

        public void SetDateVerifyValue(DateTime dt)
        {
            if (DateMode == DATE_MODE.NotDate)
            {
                box.Clear();
            }
            else if (DateMode == DATE_MODE.JP)
            {
                box.Text = DateTimeEx.GetShortJpDateStr(dt);
            }
            else if (DateMode == DATE_MODE.AD)
            {
                box.Text = dt.ToString("yyyy/MM/dd");
            }
            else if (DateMode == DATE_MODE.BOTH)
            {
                box.Text = dt.ToString("yyyy/MM/dd");
            }
        }

    }
}
