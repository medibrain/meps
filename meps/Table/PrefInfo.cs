﻿using meps.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    public class PrefInfo
    {
        /// <summary>
        /// 審査年月 [Input Year Month](yyyyMM)
        /// </summary>
        public int IYM { get; set; }
        /// <summary>
        /// 支部番号 [Prefectures ID]
        /// </summary>
        public int PID { get; set; }
        /// <summary>
        /// 取り込み日時 [Import Datetime]
        /// </summary>
        public DateTime ImportDatetime { get; set; }
        /// <summary>
        /// 取り込みユーザ [Import User ID]
        /// </summary>
        public int ImportUser { get; set; }
        /// <summary>
        /// 現在の入力状況 [Inputting Status] (0=待機中、1=入力中)
        /// </summary>
        public int InputtingStatus { get; set; }
        /// <summary>
        /// 最終入力ユーザ [Last Input User ID]
        /// </summary>
        public int LastInputUser { get; set; }



        /// <summary>
        /// (Not In Table) 平成年 [Japan Year]
        /// </summary>
        /// 
        //20190724145413 furukawa st ////////////////////////
        //引数変更により月も入れる
        
        public int JY => DateTimeEx.GetHsYearFromAd(IYM / 100,IYM%100);
        //public int JY => DateTimeEx.GetHsYearFromAd(IYM / 100);
        //20190724145413 furukawa ed ////////////////////////


        /// <summary>
        /// (Not In Table) 審査月 [Japan Month]
        /// </summary>
        public int JM => IYM - ((IYM / 100) * 100);
        /// <summary>
        /// (Not In Table) 都道府県名
        /// </summary>
        public string PrefName => Pref.GetPref(PID).Name;
        /// <summary>
        /// (Not In Table) 入力状況（全体）
        /// </summary>
        public string InputStatusMaster { get; set; }
        /// <summary>
        /// (Not In Table) 入力状況表示用（１回目）
        /// </summary>
        public string InputStatus1Str { get; set; }
        /// <summary>
        /// (Not In Table) 入力状況（１回目）
        /// </summary>
        public Consts.INPUT_STATUS InputStatus1 { get; set; }
        /// <summary>
        /// (Not In Table) 入力状況表示用（２回目）
        /// </summary>
        public string InputStatus2Str { get; set; }
        /// <summary>
        /// (Not In Table) 入力状況（２回目）
        /// </summary>
        public Consts.INPUT_STATUS InputStatus2 { get; set; }
        /// <summary>
        /// (Not In Table) 入力ユーザ（１回目）
        /// </summary>
        public int InputUserID1 { get; set; }
        /// <summary>
        /// (Not In Table) 入力ユーザ名（１回目）
        /// </summary>
        public string InputUserName1 { get; set; }
        /// <summary>
        /// (Not In Table) 入力ユーザ（２回目）
        /// </summary>
        public int InputUserID2 { get; set; }
        /// <summary>
        /// (Not In Table) 入力ユーザ名（２回目）
        /// </summary>
        public string InputUserName2 { get; set; }
        /// <summary>
        /// (Not In Table) 登録件数
        /// </summary>
        public int AllCount { get; set; }
        /// <summary>
        /// (Not In Table) 入力完了件数（１回目）
        /// </summary>
        public int Inputted1Count { get; set; }
        /// <summary>
        /// (Not In Table) 入力完了件数（２回目）
        /// </summary>
        public int Inputted2Count { get; set; }


        private static Dictionary<int, PrefInfo> GetPrefInfos(int iym)
        {
            var sql = $"SELECT * FROM prefinfo WHERE iym={iym} ORDER BY pid";
            var list = DB.Query<PrefInfo>(sql);
            if (list != null)
            {
                var piList = new Dictionary<int, PrefInfo>();
                list.ToList().ForEach(pi => piList.Add(pi.PID, pi));
                return piList;
            }
            return null;
        }

        /// <summary>
        /// 審査年月に該当する各都道府県の情報リストを取得します。
        /// 失敗した場合はnullを返します。
        /// </summary>
        /// <param name="iym"></param>
        /// <returns></returns>
        public static List<PrefInfo> GetPrefInfo(int iym)
        {
            var piDic = GetPrefInfos(iym);
            var maList = MediApp.GetMediAppsByIYM(iym);

            if (piDic == null || maList == null) return null;

            List<MediApp> mapList = null;
            int cpn = 0;
            foreach(var ma in maList)
            {
                if (cpn == 0)
                {
                    mapList = new List<MediApp>();
                    cpn = ma.PID;
                }
                if(cpn != ma.PID)
                {
                    //更新
                    updatePrefInfo(piDic[cpn], mapList);

                    //次の都道府県へ
                    cpn = ma.PID;
                    mapList = new List<MediApp>();
                }

                mapList.Add(ma);
            }

            //最後の都道府県を更新
            if (mapList != null) updatePrefInfo(piDic[cpn], mapList);

            return piDic.Values.ToList();
        }

        /// <summary>
        /// 指定の都道府県の入力情報を更新します。
        /// 第二引数のmapListの都道府県は統一されている必要があります
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="mapList"></param>
        /// <returns></returns>
        private static PrefInfo updatePrefInfo(PrefInfo pi, List<MediApp> mapList)
        {
            pi.InputUserName1 = "----";
            pi.InputUserName2 = "----";

            var inputted1 = true;
            var inputted2 = true;
            var iuList1 = new List<int>();
            var iuList2 = new List<int>();
            foreach (var map in mapList)
            {
                //件数（全体）
                pi.AllCount++;
                //１回目入力者
                var uid1 = map.InputUser1;
                if (uid1 != 0)
                {
                    if (pi.InputUserID1 == 0)
                    {
                        pi.InputUserID1 = uid1;
                        pi.InputUserName1 = User.GetUser(uid1).Name;
                        iuList1.Add(uid1);
                    }
                    else if (pi.InputUserID1 != 0 && pi.InputUserID1 != uid1) iuList1.Add(uid1);
                }
                //２回目入力者
                var uid2 = map.InputUser2;
                if (uid2 != 0)
                {
                    if (pi.InputUserID2 == 0)
                    {
                        pi.InputUserID2 = uid2;
                        pi.InputUserName2 = User.GetUser(uid2).Name;
                        iuList2.Add(uid2);
                    }
                    else if (pi.InputUserID2 != 0 && pi.InputUserID2 != uid2) iuList2.Add(uid2);
                }
                //１回目入力状況（内訳）
                switch (map.InputStatus1)
                {
                    case (int)Consts.INPUT_STATUS.スキャン済:
                        inputted1 = false;
                        break;
                    case (int)Consts.INPUT_STATUS.入力済:
                        pi.Inputted1Count++;
                        break;
                }
                //２回目入力状況（内訳）
                switch (map.InputStatus2)
                {
                    case (int)Consts.INPUT_STATUS.スキャン済:
                        inputted2 = false;
                        break;
                    case (int)Consts.INPUT_STATUS.入力済:
                        pi.Inputted2Count++;
                        break;
                }
            }
            //入力状況（全体）
            if(pi.InputtingStatus == (int)Consts.INPUTTING_STATUS.入力中) pi.InputStatusMaster = "入力中";
            else if (!inputted1) pi.InputStatusMaster = "未入力有";
            else if (!inputted2) pi.InputStatusMaster = "未ベリファイ有";
            else pi.InputStatusMaster = "完了";
            //入力状況（１回目、２回目）
            if (pi.Inputted1Count == pi.AllCount) pi.InputStatus1 = Consts.INPUT_STATUS.入力済;
            else pi.InputStatus1 = Consts.INPUT_STATUS.スキャン済;
            if (pi.Inputted2Count == pi.AllCount) pi.InputStatus2 = Consts.INPUT_STATUS.入力済;
            else pi.InputStatus2 = Consts.INPUT_STATUS.スキャン済;
            float percentInputted1 = (float)pi.Inputted1Count / (float)pi.AllCount * (float)100;
            pi.InputStatus1Str = $"{percentInputted1.ToString("0.0")}%";
            float percentInputted2 = (float)pi.Inputted2Count / (float)pi.AllCount * (float)100;
            pi.InputStatus2Str = $"{percentInputted2.ToString("0.0")}%";
            //入力者（複数人いる場合）
            if (iuList1.Count > 1)
            {
                pi.InputUserName1 = "";
                iuList1.ForEach(iu => { pi.InputUserName1 += $"{iu}, "; });
            }
            if (iuList2.Count > 1)
            {
                pi.InputUserName2 = "";
                iuList2.ForEach(iu => { pi.InputUserName2 += $"{iu}, "; });
            }
            return pi;
        }

        /// <summary>
        /// 審査年月にかかわる都道府県情報を追加します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert(DB.Transaction tran)
        {
            var sql = "INSERT INTO prefinfo " +
                "(iym, pid, importdatetime, importuser) " +
                "VALUES (@iym, @pid, @importdatetime, @importuser)";
            return DB.Execute(sql, this, tran);
        }

        /// <summary>
        /// 状態を入力中に変更します。
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool CheckIn(int userid)
        {
            var sql = 
                "UPDATE prefinfo " +
                $"SET inputtingstatus={(int)Consts.INPUTTING_STATUS.入力中}, lastinputuser={userid} " +
                "WHERE iym=@iym AND pid=@pid";
            var result = DB.Execute(sql, this);
            if (result)
            {
                InputtingStatus = (int)Consts.INPUTTING_STATUS.入力中;
                LastInputUser = userid;
            }
            return result;
        }

        /// <summary>
        /// 状態を待機中に変更します。
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool CheckOut()
        {
            var sql =
                "UPDATE prefinfo " +
                $"SET inputtingstatus={(int)Consts.INPUTTING_STATUS.待機中} " +
                "WHERE iym=@iym AND pid=@pid";
            var result = DB.Execute(sql, this);
            if (result)
            {
                InputtingStatus = (int)Consts.INPUTTING_STATUS.待機中;
            }
            return result;
        }

        /// <summary>
        /// 指定審査年月内の指定都道府県に該当するすべてのデータを削除します。
        /// </summary>
        /// <param name="iym"></param>
        /// <param name="pids"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public static bool DeleteByIYMPref(int iym, int[] pids, DB.Transaction tran)
        {
            var pidWhere = "";
            foreach (var pid in pids)
            {
                pidWhere += $"pid={pid} OR ";
            }
            var orIndex = pidWhere.LastIndexOf("OR");
            pidWhere = pidWhere.Substring(0, orIndex);
            var sql = $"DELETE FROM prefinfo WHERE iym={iym} AND ({pidWhere})";
            return DB.Execute(sql, null, tran);
        }
    }
}
