﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    class IppanRouken
    {
        static int[] CODE =
        {
            1,3
        };

        /// <summary>
        /// 一般
        /// </summary>
        public static int Ippan => CODE[0];
        /// <summary>
        /// 老健
        /// </summary>
        public static int Rouken => CODE[1];

        /// <summary>
        /// コードの最小値
        /// </summary>
        public static int Min => CODE[0];
        /// <summary>
        /// コードの最大値
        /// </summary>
        public static int Max => CODE[CODE.Length - 1];

        /// <summary>
        /// 老健か確認します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool IsRouken(string code)
        {
            return code == "39";
        }

        /// <summary>
        /// 指定したコードが有効なものか確認します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Match(int code)
        {
            return CODE.Contains(code);
        }
    }
}
