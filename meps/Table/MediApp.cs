﻿using meps.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    public class MediApp
    {
        /// <summary>
        /// ID [MediApp ID]
        /// </summary>
        public int MAID { get; set; }
        /// <summary>
        /// 申請書タイプ  [MediApp Type] Consts#MEDIAPP_TYPE参照
        /// </summary>
        public int MAType { get; set; }
        /// <summary>
        /// 支部番号 [Prefectures ID] Pref#PID
        /// </summary>
        public int PID { get; set; }
        /// <summary>
        /// 入力年月 [Input Year Month](yyyyMM)
        /// </summary>
        public int IYM { get; set; }
        /// <summary>
        /// バッチ [Batch]
        /// </summary>
        public string Batch { get; set; } = string.Empty;
        /// <summary>
        /// ナンバリング [Numbering]
        /// </summary>
        public int Numbering { get; set; } = -1;
        /// <summary>
        /// 診療年月 [Medical Year Month](yyyyMM)
        /// </summary>
        public int MYM { get; set; } = -1;
        /// <summary>
        /// 明細区分 [Meisai Type]
        /// </summary>
        public int MeisaiType { get; set; } = -1;
        /// <summary>
        /// 一般／老健 [Ippan Rouken]
        /// </summary>
        public int IppanRouken { get; set; } = -1;
        /// <summary>
        /// 社保／公費 [Syaho Kouhi]
        /// </summary>
        public int SyahoKouhi { get; set; } = -1;
        /// <summary>
        /// 本人／家族 [Honnin Kazoku]
        /// </summary>
        public int HonninKazoku { get; set; } = -1;
        /// <summary>
        /// 都道府県番号 [Prefectures Number]
        /// </summary>
        public string PrefNum { get; set; } = string.Empty;
        /// <summary>
        /// 医療機関コード [Hospital Code]
        /// </summary>
        public string HospitalCode { get; set; } = string.Empty;
        /// <summary>
        /// 公費（1）コード [Kouhi 1 Code]
        /// </summary>
        public string Kouhi1Code { get; set; } = string.Empty;
        /// <summary>
        /// 保険者番号 [Insurer Number]
        /// </summary>
        public string INum { get; set; } = string.Empty;
        /// <summary>
        /// 組合員番号 [Hi hokensya Number]
        /// </summary>
        public string HNum { get; set; } = string.Empty;
        /// <summary>
        /// 性別 [Sex]
        /// </summary>
        public int Sex { get; set; } = -1;
        /// <summary>
        /// 生年月日 [Birthday]
        /// </summary>
        public DateTime Birthday { get; set; } = DateTime.MinValue;
        /// <summary>
        /// 特記 [Tokki]
        /// </summary>
        public int Tokki { get; set; } = -1;
        /// <summary>
        /// 通知区分 [Tuuchi Type]
        /// </summary>
        public int TuuchiType { get; set; } = -1;
        /// <summary>
        /// 医保　日数 [Iho Days]
        /// </summary>
        public int IhoDays { get; set; } = -1;
        /// <summary>
        /// 医保　請求点数 [Iho Point]
        /// </summary>
        public int IhoPoint { get; set; } = -1;
        /// <summary>
        /// 医保　負担金額 [Iho Price]
        /// </summary>
        public int IhoPrice { get; set; } = -1;
        /// <summary>
        /// 公費（1）日数 [Kouhi 1 Days]
        /// </summary>
        public int Kouhi1Days { get; set; } = -1;
        /// <summary>
        /// 公費（1）請求点数 [Kouhi 1 Point]
        /// </summary>
        public int Kouhi1Point { get; set; } = -1;
        /// <summary>
        /// 公費（1）負担金額 [Kouhi 1 Price]
        /// </summary>
        public int Kouhi1Price { get; set; } = -1;
        /// <summary>
        /// 公費（2）日数 [Kouhi 2 Days]
        /// </summary>
        public int Kouhi2Days { get; set; } = -1;
        /// <summary>
        /// 公費（2）請求点数 [Kouhi 2 Point]
        /// </summary>
        public int Kouhi2Point { get; set; } = -1;
        /// <summary>
        /// 公費（2）負担金額 [Kouhi 2 Price]
        /// </summary>
        public int Kouhi2Price { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　日数 [Syokuji Iho Days]
        /// </summary>
        public int SyokujiIhoDays { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　療養金額 [Syokuji Medical Price]
        /// </summary>
        public int SyokujiIhoMediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養医保　負担金額 [Syokuji Hutan Price]
        /// </summary>
        public int SyokujiIhoHutanPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　日数 [Syokuji Kouhi 1 Days]
        /// </summary>
        public int SyokujiKouhi1Days { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　療養金額 [Syokuji Kouhi 1 Medical Price]
        /// </summary>
        public int SyokujiKouhi1MediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（1）　負担金額 [Syokuji Kouhi 1 Hutan Price]
        /// </summary>
        public int SyokujiKouhi1HutanPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　日数 [Syokuji Kouhi 2 Days]
        /// </summary>
        public int SyokujiKouhi2Days { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　療養金額 [Syokuji Kouhi 2 Medical Price]
        /// </summary>
        public int SyokujiKouhi2MediPrice { get; set; } = -1;
        /// <summary>
        /// 食事療養公費（2）　負担金額 [Syokuji Kouhi 2 Hutan Price]
        /// </summary>
        public int SyokujiKouhi2HutanPrice { get; set; } = -1;
        /// <summary>
        /// 高額療養費 [Kougaku Price]
        /// </summary>
        public int KougakuPrice { get; set; } = -1;
        /// <summary>
        /// 再審査再入力 [Again]
        /// </summary>
        public int Again { get; set; } = -1;
        /// <summary>
        /// 公費（2）コード [Kouhi 2 Code]
        /// </summary>
        public string Kouhi2Code { get; set; } = string.Empty;
        /// <summary>
        /// ファイル名（拡張子付）
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// １回目入力状況 [Input Status 1] Consts#INPUT_STATUS参照
        /// </summary>
        public int InputStatus1 { get; set; } = (int)Consts.INPUT_STATUS.スキャン済;
        /// <summary>
        /// １回目入力者 [Input User 1] User#UserID参照
        /// </summary>
        public int InputUser1 { get; set; } = 0;
        /// <summary>
        /// ２回目入力状況 [Input Status 2] Consts#INPUT_STATUS参照
        /// </summary>
        public int InputStatus2 { get; set; } = (int)Consts.INPUT_STATUS.スキャン済;
        /// <summary>
        /// ２回目入力者 [Input User 2] User#UserID参照
        /// </summary>
        public int InputUser2 { get; set; } = 0;
        /// <summary>
        /// DRG区分 [DRG Type]
        /// </summary>
        public int DrgType { get; set; } = -1;
        /// <summary>
        /// 【DRGレセのみ】診療年月日（自） [DRG Medical Date 1]
        /// </summary>
        public DateTime DrgMediDate1 { get; set; } = DateTime.MinValue;
        /// <summary>
        /// 【DRGレセのみ】診療年月日（至） [DRG Medical Date 2]
        /// </summary>
        public DateTime DrgMediDate2 { get; set; } = DateTime.MinValue;


        /// <summary>
        /// (Not In Table) １回目入力状況（表示用）
        /// </summary>
        public string InputStatusView1
        {
            get
            {
                if (InputStatus1 == (int)Consts.INPUT_STATUS.入力済)
                {
                    return "入力完了";
                }
                return "未入力";
            }
        }
        /// <summary>
        /// (Not In Table) ２回目入力状況（表示用）
        /// </summary>
        public string InputStatusView2
        {
            get
            {
                if (InputStatus2 == (int)Consts.INPUT_STATUS.入力済)
                {
                    return "ﾍﾞﾘﾌｧｲ済";
                }
                return "未入力";
            }
        }
        /// <summary>
        /// (Not In Table) 申請書タイプ（表示用）
        /// </summary>
        public string MediAppTypeView
        {
            get
            {
                if(Enum.IsDefined(typeof(Consts.MEDIAPP_TYPE), MAType))
                {
                    var matype= (Consts.MEDIAPP_TYPE)MAType;
                    if(matype == Consts.MEDIAPP_TYPE.NULL)
                    {
                        return "";
                    }
                    return matype.ToString();
                }
                return "不明";
            }
        }
        /// <summary>
        /// (Not In Table) ナンバリング（表示用）
        /// </summary>
        public string NumberingView
        {
            get
            {
                if (Numbering != -1)
                {
                    return Numbering.ToString("000000");
                }
                return "";
            }
        }


        /// <summary>
        /// 都道府県と審査年月から申請書一覧を取得します。（MAID昇順）
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="iYM"></param>
        /// <returns></returns>
        public static List<MediApp> GetMediAppsByPIDIYM(int IYM, int pid)
        {
            var sql = $"SELECT * FROM mediapp WHERE pid={pid} AND iym={IYM} ORDER BY maid;";

            var l = DB.Query<MediApp>(sql);
            if (l == null)
            {
                return null;
            }
            var maList = new List<MediApp>();
            l.ToList().ForEach(ma => maList.Add(ma));
            return maList;
        }

        /// <summary>
        /// 審査年月から申請書一覧を取得します。（都道府県：昇順）
        /// </summary>
        /// <param name="IYM"></param>
        /// <returns></returns>
        public static List<MediApp> GetMediAppsByIYM(int IYM)
        {
            var sql = $"SELECT * FROM mediapp WHERE iym={IYM} ORDER BY pid;";

            var l = DB.Query<MediApp>(sql);
            if (l == null)
            {
                return null;
            }
            var maList = new List<MediApp>();
            l.ToList().ForEach(ma => maList.Add(ma));
            return maList;
        }

        /// <summary>
        /// 画像に対応するデータを作成します。
        /// insertMAIDは新たに作成されたデータのMAIDを表します。失敗した場合は-1を返します。
        /// </summary>
        /// <param name="tran"></param>
        /// <param name="insertMAID"></param>
        /// <returns></returns>
        public bool PreInsert(DB.Transaction tran, out int insertMAID)
        {
            insertMAID = -1;
            var sql = "INSERT INTO mediapp (pid, iym, filename) VALUES (@pid, @iym, @filename) RETURNING maid";
            var r = DB.Query<MediApp>(sql, this, tran);
            if (r != null)
            {
                insertMAID = r.ToList()[0].MAID;
                return true;
            }
            return false;
        }

        /// <summary>
        /// データを更新します。
        /// </summary>
        /// <returns></returns>
        public bool Update()
        {
            var sql =
                "UPDATE mediapp " +
                "SET " +
                "matype=@matype, batch=@batch, numbering=@numbering, mym=@mym, " +
                "meisaitype=@meisaitype, ippanrouken=@ippanrouken, " +
                "syahokouhi=@syahokouhi, honninkazoku=@honninkazoku, " +
                "prefnum=@prefnum, hospitalcode=@hospitalcode, kouhi1code=@kouhi1code, " +
                "inum=@inum, hnum=@hnum, sex=@sex, birthday=@birthday, " +
                "tokki=@tokki, tuuchitype=@tuuchitype, " +
                "ihodays=@ihodays, ihopoint=@ihopoint, ihoprice=@ihoprice, " +
                "kouhi1days=@kouhi1days, kouhi1point=@kouhi1point, kouhi1price=@kouhi1price, " +
                "kouhi2days=@kouhi2days, kouhi2point=@kouhi2point, kouhi2price=@kouhi2price, " +
                "syokujiihodays=@syokujiihodays, syokujiihomediprice=@syokujiihomediprice, " +
                "syokujiihohutanprice=@syokujiihohutanprice, " +
                "syokujikouhi1days=@syokujikouhi1days, syokujikouhi1mediprice=@syokujikouhi1mediprice, " +
                "syokujikouhi1hutanprice=@syokujikouhi1hutanprice, " +
                "syokujikouhi2days=@syokujikouhi2days, syokujikouhi2mediprice=@syokujikouhi2mediprice, " +
                "syokujikouhi2hutanprice=@syokujikouhi2hutanprice, " +
                "kougakuprice=@kougakuprice, again=@again, " +
                "kouhi2code=@kouhi2code, " +
                "inputstatus1=@inputstatus1, inputuser1=@inputuser1, " +
                "inputstatus2=@inputstatus2, inputuser2=@inputuser2, " +
                "drgtype=@drgtype, drgmedidate1=@drgmedidate1, drgmedidate2=@drgmedidate2 " +
                "WHERE " +
                "maid=@maid";
            return DB.Execute(sql, this);
        }

        /// <summary>
        /// 審査年月および都道府県に該当するすべてのデータを削除します。
        /// </summary>
        /// <returns></returns>
        public static bool DeleteByIYMPID(int iym, int[] pids, DB.Transaction tran)
        {
            var pidWhere = "";
            foreach(var pid in pids)
            {
                pidWhere += $"pid={pid} OR ";
            }
            var orIndex = pidWhere.LastIndexOf("OR");
            pidWhere = pidWhere.Substring(0, orIndex);
            var sql = $"DELETE FROM mediapp WHERE iym={iym} AND ({pidWhere})";
            return DB.Execute(sql, null, tran);
        }

        /// <summary>
        /// 画像ファイルパスを取得します。
        /// </summary>
        /// <param name="ma"></param>
        /// <returns></returns>
        public string GetImagePath(string imageHost, string dbName)
        {
            if (string.IsNullOrWhiteSpace(imageHost)) return null;
            if (string.IsNullOrWhiteSpace(dbName)) return null;
            //保険者別にimageHOstを変更する予定
            return $"{imageHost}\\scan\\{dbName}\\{IYM}\\{PID}\\{FileName}";
        }

    }
}
