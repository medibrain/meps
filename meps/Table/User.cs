﻿using System.Collections.Generic;
using System.Linq;

namespace meps.Table
{
    public class User
    {
        /// <summary>
        /// ユーザーID [User ID]
        /// </summary>
        public int UserID { get; private set; }
        /// <summary>
        /// 名前 [Name]
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ログイン名 [Login Name]
        /// </summary>
        public string LoginName{get;set;}
        /// <summary>
        /// パスワード [Password]
        /// </summary>
        public string Pass{get;set;}
        /// <summary>
        /// 有効／無効 [Enable]
        /// </summary>
        public bool Enable { get; set; }
        /// <summary>
        /// 管理者権限有／無 [Admin]
        /// </summary>
        public bool Admin { get; set; }

        private static Dictionary<int, User> userList = new Dictionary<int, User>();
        /// <summary>
        /// ログイン中のユーザー
        /// </summary>
        public static User CurrentUser { get; private set; } = null;

        //20200305174751 furukawa st ////////////////////////
        //コンフィグ設定用開発者フラグ
        
        /// <summary>
        /// 開発者フラグ
        /// </summary>
        public bool Developer { get; set; }

        //20200305174751 furukawa ed ////////////////////////


        public User() { }
             
        public User(int id, string name)
        {
            this.UserID = id;
            this.Name = name;
        }

        public bool Insert()
        {
            var currentDBName = DB.GetDBName();
            DB.SetDBName(Consts.DB_NAME_USER);

            try
            {
                var sql = "INSERT INTO users " +
                    "(userid, name, loginname, pass, enable, admin ,developer) " +
                    "VALUES(@userid, @name, @loginname, @pass, @enable, @admin ,@developer)";

                return DB.Execute(sql, this);
            }
            finally
            {
                DB.SetDBName(currentDBName);
            }
        }

        public bool Update()
        {
            var currentDBName = DB.GetDBName();
            DB.SetDBName(Consts.DB_NAME_USER);

            try
            {
                var sql = "UPDATE users SET " +
                    "name=@name, loginname=@loginname, pass=@pass, enable=@enable, admin=@admin ,developer=@developer " +
                    "WHERE userid=@userid;";

                return DB.Execute(sql, this);
            }
            finally
            {
                DB.SetDBName(currentDBName);
            }
        }
        
        public static void GetUsers()
        {
            var currentDBName = DB.GetDBName();
            DB.SetDBName(Consts.DB_NAME_USER);

            try
            {
                var sql = "SELECT userid, name, loginname, pass, enable, admin, developer " +
                    "FROM users";

                var l = DB.Query<User>(sql);
                userList.Clear();
                l.ToList().ForEach(u => userList.Add(u.UserID, u));
            }
            finally
            {
                DB.SetDBName(currentDBName);
            }
        }

        //UserIDに対応するユーザー名を取得
        public static string GetUserName(int userID)
        {
            if (userList.ContainsKey(userID))
                return userList[userID].Name;
            else
                return string.Empty;
        }

        //UserIDに対応するユーザーを取得
        public static User GetUser(int userID)
        {
            if (userList.ContainsKey(userID))
                return userList[userID];
            else
                return null;
        }

        //ログインしているユーザーを記憶
        public static void SetCurrentUser(int userID)
        {
            var u = GetUser(userID);
            CurrentUser = u;
        }

        public static List<User> GetList()
        {
            return userList.Values.ToList();
        }   
    }
}
