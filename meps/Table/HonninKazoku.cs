﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    class HonninKazoku
    {
        static int[] CODE =
        {
            1,2,3,4,5,6,7,8,9,0
        };

        /// <summary>
        /// 指定したコードが有効なものか確認します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Match(int code)
        {
            return CODE.Contains(code);
        }
    }
}
