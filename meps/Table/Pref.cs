﻿using System.Collections.Generic;
using System.Linq;

namespace meps.Table
{
    class Pref
    {
        static Dictionary<int, Pref> prefList = new Dictionary<int, Pref>();

        /// <summary>
        /// 都道府県番号
        /// </summary>
        public int PID { get; private set; }
        /// <summary>
        /// 都道府県名
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 都道府県名（行政区画名付）
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// フリガナ
        /// </summary>
        public string Kana { get; set; }
        /// <summary>
        /// 表示用文字列（［都道府県番号(nn)］:［都道府県名］）
        /// </summary>
        public string ShowItem => $"{PID.ToString("00")}: {Name}";

        /// <summary>
        /// (Not In Table) 納品データ出力先
        /// </summary>
        public string OuputPath { get; set; }

        /// <summary>
        /// 都道府県一覧を取得
        /// </summary>
        public static bool GetPrefList()
        {
            var sql = "SELECT * FROM pref ORDER BY pid;";

            var l = DB.Query<Pref>(sql);
            prefList.Clear();
            if (l != null)
            {
                Insurer.GetInsurerList();
                l.ToList().ForEach(p =>
                {
                    p.OuputPath = Insurer.GetInsurer(p.PID)?.OutputPath;//出力先は別のところから取得
                    prefList.Add(p.PID, p);
                });
                return true;
            }
            return false;
        }

        /// <summary>
        /// pidから都道府県情報を取得します。見付からない場合はnullを返します
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public static Pref GetPref(int pid)
        {
            if (prefList.ContainsKey(pid))
                return prefList[pid];
            else
                return null;
        }

        /// <summary>
        /// 都道府県名から検索します。見つからない場合はnullを返します
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Pref GetPrefByName(string name)
        {
            foreach(var pref in prefList.Values.ToList())
            {
                if (pref.Name == name) return pref;
                if (pref.FullName == name) return pref;
            }
            return null;
        }

        /// <summary>
        /// すべての都道府県情報を取得します
        /// </summary>
        /// <returns></returns>
        public static List<Pref> GetPrefAll()
        {
            return prefList.Values.ToList();
        }

        /// <summary>
        /// 都道府県番号の最大値を取得します
        /// </summary>
        /// <returns></returns>
        public static int GetMaxValue()
        {
            return prefList.Values.ToList()[prefList.Count - 1].PID;
        }

    }
}
