﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    public class Consts
    {
        public enum MEDIAPP_TYPE { NULL=-1, 申請書=1, バッチ=2, 続紙=3, 付箋=4, 不要=5, 再審査返戻=6 }
        public enum INPUT_STATUS { NULL=-1, スキャン済=0, 入力済=1}
        public enum INPUTTING_STATUS { 待機中=0, 入力中=1 }

        public static readonly IReadOnlyDictionary<int, string> DB_NAMES =
            new System.Collections.ObjectModel.ReadOnlyDictionary<int, string>(new Dictionary<int, string>()
            {
                {2, "gakko_02aomori"},
                {8, "gakko_08ibaraki"},
                {42, "gakko_42nagasaki"},
            });


        public const string DB_NAME_02_AOMORI = "gakko_02aomori";
        public const string DB_NAME_08_IBARAKI = "gakko_08ibaraki";
        public const string DB_NAME_42_NAGASAKI = "gakko_42nagasaki";

        public const string DB_NAME_MEPS = "zenkoku_gakko";
        public const string DB_NAME_USER = "jyusei";
        public const string CONNECT_USER = "postgres";


        //20201123175540 furukawa st ////////////////////////
        //aws用に変更
        
        public const string CONNECT_PASS = "medibrain1128";
        //public const string CONNECT_PASS = "pass";
        //20201123175540 furukawa ed ////////////////////////


        //public const string DB_NAME_MEPS = "meps_test";
        //public const string DB_NAME_USER = "meps_test";
        //public const string CONNECT_USER = "postgres";
        //public const string CONNECT_PASS = "Kukuku1-";
    }
}
