﻿using meps.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    class Inquiry
    {
        /// <summary>
        /// ID [Application ID]
        /// </summary>
        public int AID { get; set; }
        /// <summary>
        /// バッチ [Batch]
        /// </summary>
        public string Batch { get; set; }
        /// <summary>
        /// ナンバリング [Numbering]
        /// </summary>
        public int Numbering { get; set; }
        /// <summary>
        /// 被保険者番号 [Hi Hokensya Number]
        /// </summary>
        public string HNum { get; set; }
        /// <summary>
        /// 性別 [Sex]
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 生年月日 [Birthday]
        /// </summary>
        public DateTime Birthday { get; set; }
        /// <summary>
        /// 診療年月 [Medical Year Month]
        /// </summary>
        public int MYM { get; set; }
        /// <summary>
        /// 日数 [Days]
        /// </summary>
        public int Days { get; set; }
        /// <summary>
        /// 合計
        /// </summary>
        public int TotalPrice { get; set; }
        /// <summary>
        /// 施術師番号 [Doctor Number]
        /// </summary>
        public int DNum { get; set; }
        /// <summary>
        /// 照会理由 [Inquiry Reason]
        /// </summary>
        public string InquiryReason { get; set; }

        /// <summary>
        /// 支部ID [Pref ID]
        /// </summary>
        public int PID { get; set; }
        /// <summary>
        /// 画像ファイル名（拡張子付） [File Name]
        /// </summary>
        public int FileName { get; set; }
        /// <summary>
        /// 画像パス [File Path]
        /// </summary>
        public string FilePath => $"{Settings.ImageHost}\\scan\\{Consts.DB_NAMES[PID]}\\{Batch}\\{FileName}";


    }
}
