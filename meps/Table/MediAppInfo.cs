﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using meps.Utils;

namespace meps.Table
{
    public class MediAppInfo
    {
        /// <summary>
        /// MAID [MediAppInfo ID]
        /// </summary>
        public int MAID { get; set; }
        /// <summary>
        /// 審査年月 [Input Year Month](yyyyMM)
        /// </summary>
        public int IYM { get; set; }
        /// <summary>
        /// 支部番号 [Prefectures Number]
        /// </summary>
        public int PID { get; set; }
        
        /// <summary>
        /// (Not In Table) 審査年 [Input Year]
        /// </summary>
        //20190724145312 furukawa st ////////////////////////
        //引数変更により月も入れる
        
        public int IY => DateTimeEx.GetHsYearFromAd(IYM / 100, IYM % 100);
        //public int IY => DateTimeEx.GetHsYearFromAd(IYM / 100);
        //20190724145312 furukawa ed ////////////////////////


        /// <summary>
        /// (Not In Table) 審査月 [Input Month]
        /// </summary>
        public int IM => IYM - ((IYM / 100) * 100);
        /// <summary>
        /// (Not In Table) 件数 [MediAppInfo Count]
        /// </summary>
        public int MAICount { get; set; }


        /// <summary>
        /// 審査年月別　登録件数を取得します（降順）
        /// </summary>
        /// <returns></returns>
        public static List<MediAppInfo> GetCountList()
        {
            var sql = $"SELECT iym, COUNT(maid) AS maicount FROM mediappinfo GROUP BY iym ORDER BY iym DESC;";
            var l = DB.Query<MediAppInfo>(sql);
            if (l == null)
            {
                return null;
            }
            var maiList = new List<MediAppInfo>();
            l.ToList().ForEach(mai => maiList.Add(mai));
            return maiList;
        }

        /// <summary>
        /// 都道府県別　指定年月登録件数を取得します（昇順）
        /// </summary>
        /// <param name="yyyyMM"></param>
        /// <returns></returns>
        public static List<MediAppInfo> GetCountListByIYM(int yyyyMM)
        {
            var sql = $"SELECT iym, pid, COUNT(maid) AS maicount FROM mediappinfo WHERE iym={yyyyMM} GROUP BY pid ORDER BY pid ASC;";
            var l = DB.Query<MediAppInfo>(sql);
            if (l == null)
            {
                return null;
            }
            var maiList = new List<MediAppInfo>();
            l.ToList().ForEach(mai => maiList.Add(mai));
            return maiList;
        }

        /// <summary>
        /// 画像に対応する計算用データを作成します。
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        public bool Insert(DB.Transaction tran)
        {
            var sql = "INSERT INTO mediappinfo (maid, iym, pid) VALUES (@maid, @iym, @pid)";
            return DB.Execute(sql, this, tran);
        }

        /// <summary>
        /// 審査年月および都道府県に該当するすべてのデータを削除します。
        /// </summary>
        /// <returns></returns>
        public static bool DeleteByIYMPID(int iym, int[] pids, DB.Transaction tran)
        {
            var pidWhere = "";
            foreach (var pid in pids)
            {
                pidWhere += $"pid={pid} OR ";
            }
            var orIndex = pidWhere.LastIndexOf("OR");
            pidWhere = pidWhere.Substring(0, orIndex);
            var sql = $"DELETE FROM mediappinfo WHERE iym={iym} AND ({pidWhere})";
            return DB.Execute(sql, null, tran);
        }
    }
}
