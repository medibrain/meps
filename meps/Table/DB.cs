﻿using System;
using System.Collections.Generic;
using Npgsql;
using Dapper;
using meps.Forms;
using meps.Utils;

namespace meps.Table
{
    public class DB
    {
        static string dbAddress = string.Empty;
        static string portNo = string.Empty;
        static string dbName = string.Empty;
        static string dbEncording = string.Empty;
        static string userName = string.Empty;
        static string password = string.Empty;
        static string timeout = string.Empty;
        static string longTimeout = string.Empty;
        static string connectionStr = string.Empty;
        static string longConnectionStr = string.Empty;

        public static bool IsConnected { get; set; }



        /// <summary>
        /// 設定をロードします
        /// </summary>
        /// <param name="dbname">db名</param>
        /// <returns></returns>        
        public static bool SetDBName(string dbname)        
        {
            NpgsqlConnection.ClearAllPools();

            //なしはダメ
            if (dbname == null || dbname == "") return false;

            try
            {
                dbAddress = Settings.DataBaseHost;
                portNo = "5432";
                dbName = dbname;
                dbEncording = "UNICODE";
                userName = Consts.CONNECT_USER;
                password = Consts.CONNECT_PASS;
                timeout = Settings.CommandTimeout;
                longTimeout = "3600";

                //20201123180320 furukawa st ////////////////////////
                //awsを追加
                
                if (dbAddress.Contains("aws"))
                {
                    connectionStr = string.Format("Server={0};Port={1};Database={2};Encoding={3};" +
                        "User Id={4};Password={5};CommandTimeout={6};sslmode=require",
                    dbAddress,
                    portNo,
                    dbName,
                    dbEncording,
                    userName,
                    password,
                    timeout
                    );

                    longConnectionStr = string.Format("Server={0};Port={1};Database={2};Encoding={3};" +
                        "User Id={4};Password={5};CommandTimeout={6};sslmode=require",
                        dbAddress,
                        portNo,
                        dbName,
                        dbEncording,
                        userName,
                        password,
                        longTimeout
                        );
                }//20201123180320 furukawa ed ////////////////////////
                else
                {
                    connectionStr = string.Format("Server={0};Port={1};Database={2};Encoding={3};User Id={4};Password={5};CommandTimeout={6};",
                        dbAddress,
                        portNo,
                        dbName,
                        dbEncording,
                        userName,
                        password,
                        timeout
                        );

                    longConnectionStr = string.Format("Server={0};Port={1};Database={2};Encoding={3};User Id={4};Password={5};CommandTimeout={6};",
                        dbAddress,
                        portNo,
                        dbName,
                        dbEncording,
                        userName,
                        password,
                        longTimeout
                        );
                }
                //接続確認
                return ConnectTest(dbAddress);
            }
            catch
            {
                return false;
            }
        }

        public static string GetDBName()
        {
            return dbName;
        }


        /// <summary>
        /// トランザクション
        /// </summary>
        public class Transaction : IDisposable
        {
            public NpgsqlConnection conn { get; private set; }
            public NpgsqlTransaction tran { get; private set; }

            public Transaction()
            {
                open();
                tran = conn.BeginTransaction();
            }

            private bool open()
            {
                conn = new NpgsqlConnection(connectionStr);
#if DEBUG
                conn.Open();
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    //Log.ErrorWrite(ex);
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            internal void Commit()
            {
                tran.Commit();
            }

            internal void Rollback()
            {
                tran.Rollback();
            }

            public void Dispose()
            {
                conn.Close();
                tran.Dispose();
                conn.Dispose();
            }
        }

        /// <summary>
        /// トランザクションを開始します
        /// </summary>
        /// <returns></returns>
        public static Transaction CreateTransaction()
        {
            return new Transaction();
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public static Command CreateCmd(string commandText, Transaction tran)
        {
            return new Command(commandText, tran);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public static Command CreateCmd(string commandText)
        {
            return new Command(commandText);
        }

        /// <summary>
        /// SQLコマンドを作成します。
        /// </summary>
        /// <param name="commandText">コマンドtext</param>
        /// <returns></returns>
        public static Command CreateLongTimeoutCmd(string commandText)
        {
            return new Command(commandText, true);
        }

        public class Command : IDisposable
        {
            NpgsqlConnection conn;
            NpgsqlCommand cmd;
            bool longTimeout = false;
            System.Timers.Timer timer;
            CommandCancelForm ccf;
            DateTime startTime;

            private bool open()
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                conn = new NpgsqlConnection(longTimeout ? longConnectionStr : connectionStr);
#if DEBUG
                conn.Open();
#else
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    //Log.ErrorWrite(ex);
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                    return false;
                }
                if (conn.FullState != System.Data.ConnectionState.Open) return false;
#endif
                return true;
            }

            public NpgsqlParameterCollection Parameters
            {
                get { return cmd.Parameters; }
                set { value = cmd.Parameters; }
            }

            internal Command(string commandText)
            {
                open();
                cmd = new NpgsqlCommand(commandText, conn);
            }

            internal Command(string commandText, bool longTimeout)
            {
                this.longTimeout = longTimeout;
                startTime = DateTime.Now;
                open();
                cmd = new NpgsqlCommand(commandText, conn);
            }

            internal Command(string commandText, Transaction tran)
            {
                cmd = new NpgsqlCommand(commandText, tran.conn, tran.tran);
            }

            private bool reOpen()
            {
                if (!open()) return false;
                cmd.Connection = conn;
                return true;
            }

            /// <summary>
            /// トランザクションを使用しているかをチェックし、使用中ならプログラムを強制終了します。
            /// </summary>
            private void tranCheck()
            {
                if (cmd.Transaction != null)
                {
                    System.Environment.Exit(0);
                }
            }

            private void systemExit()
            {
                System.Environment.Exit(0);
            }

            /// <summary>
            /// このコマンドによって使用されているすべてのリソースを解放します。
            /// </summary>
            public void Dispose()
            {
                if (cmd.Transaction == null && conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
                cmd.Dispose();
            }

            /// <summary>
            /// Tryを含んだExcuteReaderを発行し、リストで返します。失敗した場合 null が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public List<object[]> TryExecuteReaderList()
            {
                var lst = new List<object[]>();

                //タイムアウト通知用タイマー
                if (longTimeout)
                {
                    timer = new System.Timers.Timer(20000);
                    timer.Elapsed += t_Elapsed;
                    timer.Start();
                }
#if DEBUG
                try
                {
                    using (var dr = cmd.ExecuteReader())
                    {
                        var c = dr.FieldCount;
                        while (dr.Read())
                        {
                            var ojs = new object[c];
                            dr.GetValues(ojs);
                            lst.Add(ojs);
                        }
                    }
                    return lst;
                }
                catch (NpgsqlException ex)
                {
                    //ユーザーによるキャンセル 57014
                    if (ex.Code == "57014") return lst;
                    throw ex;
                }
                finally
                {
                    if (timer != null)
                    {
                        timer.Stop();
                        timer.Dispose();
                    }

                    if (ccf != null) ccf.InvokeDispose();
                }

#else
                try
                {
                    try
                    {
                        using (var dr = cmd.ExecuteReader())
                        {
                            var c = dr.FieldCount;
                            while (dr.Read())
                            {
                                var ojs = new object[c];
                                dr.GetValues(ojs);
                                lst.Add(ojs);
                            }
                        }
                        return lst;
                    }
                    catch (NpgsqlException ex)
                    {
                        //ユーザーによるキャンセル 57014
                        if (ex.Code == "57014") return lst;
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        if (cmd.Transaction != null) throw ex;
                        if (!reOpen()) throw ex;
                    }

                    try
                    {
                        using (var dr = cmd.ExecuteReader())
                        {
                            var c = dr.FieldCount;
                            while (dr.Read())
                            {
                                var ojs = new object[c];
                                dr.GetValues(ojs);
                                lst.Add(ojs);
                            }
                        }
                        return lst;
                    }
                    catch (NpgsqlException ex)
                    {
                        //ユーザーによるキャンセル 57014
                        if (ex.Code == "57014") return lst;
                        throw ex;
                    }
                    catch (Exception ex)
                    {
                        var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                        var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                        ex.Data.Add("MethodName", data);
                        throw ex;
                    }
                }
                finally
                {
                    if (timer != null)
                    {
                        timer.Stop();
                        timer.Dispose();
                    }

                    if (ccf != null) ccf.InvokeDispose();
                }

#endif
            }

            void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                ccf = new CommandCancelForm(startTime);
                var t = (System.Timers.Timer)sender;
                t.Stop();
                t.Dispose();

                var res = ccf.ShowDialog();
                if (res == System.Windows.Forms.DialogResult.Cancel) cmd.Cancel();
            }

            /// <summary>
            /// Tryを含んだExecuteScalarを発行します。失敗した場合 DBNull.Value が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public object TryExecuteScalar()
            {
#if DEBUG
                return cmd.ExecuteScalar();
#else
                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    return cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }

            /// <summary>
            /// Tryを含んだExecuteNonQueryを発行します。失敗した場合 false が返ります。
            /// </summary>
            /// <param name="cmd"></param>
            /// <returns></returns>
            public bool TryExecuteNonQuery()
            {
#if DEBUG
                cmd.ExecuteNonQuery();
                return true;
#else
                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    if (cmd.Transaction != null) throw ex;
                    if (!reOpen()) throw ex;
                }

                try
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    var sfm = new System.Diagnostics.StackFrame(1).GetMethod();
                    var data = "Class:" + sfm.DeclaringType.FullName + " Method:" + sfm.Name;
                    ex.Data.Add("MethodName", data);
                    throw ex;
                }
#endif
            }
        }

        /// <summary>
        /// Dapperコマンド用オープン済みコネクション
        /// </summary>
        /// <returns></returns>
        private static NpgsqlConnection getOpenConn()
        {
            NpgsqlConnection con = null;

            try
            {
                con = new NpgsqlConnection(connectionStr);
                con.Open();
            }
            catch
            {
                try
                {
                    if (con != null) con.Dispose();
                    con = null;
                    con = new NpgsqlConnection(connectionStr);
                    con.Open();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteWithMsg(ex);
                    throw ex;
                }
            }
            return con;
        }

        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public static IEnumerable<Type> Query<Type>(string sql, object parameters = null)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    var res = con.Query<Type>(sql, parameters);
                    return res;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        /// <summary>
        /// Dapperによる自動マッピングQuery
        /// </summary>
        public static IEnumerable<Type> Query<Type>(string sql, object parameters, Transaction tran)
        {
            try
            {
                var res = tran.conn.Query<Type>(sql, parameters, tran.tran);
                return res;
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return null;
            }
        }

        /// <summary>
        /// Dapperによる自動マッピングExcute
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="mappingObjectOrMappingObjectList"></param>
        /// <returns></returns>
        public static bool Execute(string sql, object mappingObjectOrMappingObjectList)
        {
            try
            {
                using (var con = getOpenConn())
                {
                    con.Execute(sql, mappingObjectOrMappingObjectList);                    
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Dapperによる自動マッピングExcute
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="mappingObjectOrMappingObjectList"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public static bool Execute(string sql, object mappingObjectOrMappingObjectList, Transaction tran)
        {
            try
            {
                tran.conn.Execute(sql, mappingObjectOrMappingObjectList, tran.tran);
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// すでに登録済みの情報を使用して接続テストを行います。引数のhostを指定した場合はそちらを使用します
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public static bool ConnectTest(string host = null)
        {
            var tmpHost = dbAddress;
            if (host != null) tmpHost = host;
            try
            {
                //20201123175941 furukawa st ////////////////////////
                //awsを追加
                
                string tmpConnectionStr = string.Empty;

                if (tmpHost.Contains("aws"))
                {

                    tmpConnectionStr = string.Format(
                        "Server={0};Port={1};Database={2};Encoding={3};User Id={4};" +
                        "Password={5};CommandTimeout={6};sslmode=require",
                    tmpHost,
                    "5432",
                    GetDBName(),
                    "UNICODE",
                    Consts.CONNECT_USER,
                    Consts.CONNECT_PASS,
                    "5"
                    );
                }
                else
                {
                    tmpConnectionStr = string.Format(
                         "Server={0};Port={1};Database={2};Encoding={3};User Id={4};" +
                         "Password={5};CommandTimeout={6};",
                     tmpHost,
                     "5432",
                     GetDBName(),
                     "UNICODE",
                     Consts.CONNECT_USER,
                     Consts.CONNECT_PASS,
                     "5"
                     );

                }
                //var tmpConnectionStr = string.Format("Server={0};Port={1};Database={2};Encoding={3};User Id={4};Password={5};CommandTimeout={6};",
                //  tmpHost,
                //  "5432",
                //  GetDBName(),
                //  "UNICODE",
                //  Consts.CONNECT_USER,
                //  Consts.CONNECT_PASS,
                //  "5"
                //  );

                //20201123175941 furukawa ed ////////////////////////

                //接続確認
                var conn = new NpgsqlConnection(tmpConnectionStr);
                conn.Open();
                conn.Close();
                conn.Dispose();
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// データベースホストを変更します。
        /// </summary>
        /// <param name="dbHost"></param>
        public static void UpdateDatabaseHost(string dbHost)
        {
            dbAddress = dbHost;
        }
    }
}