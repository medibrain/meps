﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    class Tokki
    {
        static int[] CODE =
        {
            1,2,3,
            10,11,16,17,18,19,
            20,21,22,23,24,26,27,28,29,
            30,31,32,33,34,35,
            96,97,99
        };

        /// <summary>
        /// コードの最小値
        /// </summary>
        /// <returns></returns>
        public static int Min => CODE[0];
        /// <summary>
        /// コードの最大値
        /// </summary>
        public static int Max => CODE[CODE.Length - 1];

        /// <summary>
        /// 指定したコードが有効なものか確認します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Match(int code)
        {
            return CODE.Contains(code);
        }
    }
}
