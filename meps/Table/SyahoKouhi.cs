﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    class SyahoKouhi
    {
        static int[] CODE =
        {
            1,2,3
        };

        /// <summary>
        /// 社保
        /// </summary>
        public static int Syaho => CODE[0];
        /// <summary>
        /// 公費①併用
        /// </summary>
        public static int Kouhi1 => CODE[1];
        /// <summary>
        /// 公費①②併用
        /// </summary>
        public static int Kouhi12 => CODE[2];

        /// <summary>
        /// 公費①②の値から社保／公費のコードを取得します。
        /// </summary>
        /// <param name="code1"></param>
        /// <param name="code2"></param>
        /// <returns></returns>
        public static int GetCode(string code1, string code2)
        {
            var kc1Exist = false;
            var kc2Exist = false;
            if (!string.IsNullOrWhiteSpace(code1) && code1 != "00") kc1Exist = true;
            if (!string.IsNullOrWhiteSpace(code2) && code2 != "00") kc2Exist = true;
            if (!kc1Exist && !kc2Exist) return Syaho;
            if (kc1Exist && !kc2Exist) return Kouhi1;
            if (kc1Exist && kc2Exist) return Kouhi12;
            return -1;
        }

        /// <summary>
        /// 指定したコードが有効なものか確認します。
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool Match(int code)
        {
            return CODE.Contains(code);
        }
    }
}
