﻿using meps.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meps.Table
{
    public class Insurer
    {
        public int InsurerID { set; get; }
        public string InsurerName { set; get; }
        public string dbName { set; get; }
        public string InsNumber { get; set; }
        public string FormalName { get; set; }
        public int InsurerType { get; set; }
        public int ViewIndex { get; set; }
        public string OutputPath { get; set; }

        public static Dictionary<int, Insurer> dicIns = new Dictionary<int, Insurer>();

        //20200305174501 furukawa st ////////////////////////
        //保険者別に接続先、ポート、フォルダを持たせる
        
        public string ServerIP { get; set; }//サーバIP
        public int dbport { get; set; }//dbポート
        public string strImagePath { get; set; }//画像パス
        //20200305174501 furukawa ed ////////////////////////



        /// <summary>
        /// 保険者一覧を取得します。
        /// </summary>
        /// <returns></returns>
        public static bool GetInsurerList()
        {
            var currentDBName = DB.GetDBName();
            try
            {
                if(currentDBName != "jyusei")
                {
                    DB.SetDBName("jyusei");
                }

                var sql = "SELECT insurerid, insurername, dbname, " +
                    "insnumber, formalname, insurertype, viewindex, outputpath " +
                    "FROM insurer " +
                    "WHERE insurertype=4 AND enabled=true " +
                    "ORDER BY viewindex";

                var l = DB.Query<Insurer>(sql);
                if(l != null)
                {
                    l.ToList().ForEach(i => dicIns.Add(i.ViewIndex, i));
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.ErrorWriteWithMsg(ex);
            }
            finally
            {
                //接続先データベースを元に戻す
                DB.SetDBName(currentDBName);
            }
            return false;
        }

        /// <summary>
        /// 都道府県番号から保険者情報を取得します。失敗した場合はnullを返します。
        /// </summary>
        /// <param name="insurerID"></param>
        /// <returns></returns>
        public static Insurer GetInsurer(int pid)
        {
            if (dicIns.ContainsKey(pid))
                return dicIns[pid];
            else
                return null;
        }
    }
}
