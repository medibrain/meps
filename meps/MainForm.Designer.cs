﻿namespace meps
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonInquiry = new System.Windows.Forms.Button();
            this.buttonOther = new System.Windows.Forms.Button();
            this.buttonSimpleSearch = new System.Windows.Forms.Button();
            this.buttonDataExport = new System.Windows.Forms.Button();
            this.buttonInput = new System.Windows.Forms.Button();
            this.buttonScan = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewMonth = new System.Windows.Forms.DataGridView();
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonth)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(333, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 18);
            this.label4.TabIndex = 15;
            this.label4.Text = "->";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonInquiry);
            this.groupBox1.Controls.Add(this.buttonOther);
            this.groupBox1.Controls.Add(this.buttonSimpleSearch);
            this.groupBox1.Controls.Add(this.buttonDataExport);
            this.groupBox1.Controls.Add(this.buttonInput);
            this.groupBox1.Controls.Add(this.buttonScan);
            this.groupBox1.Location = new System.Drawing.Point(363, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(418, 184);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "作業を選択：";
            // 
            // buttonInquiry
            // 
            this.buttonInquiry.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonInquiry.Location = new System.Drawing.Point(16, 101);
            this.buttonInquiry.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInquiry.Name = "buttonInquiry";
            this.buttonInquiry.Size = new System.Drawing.Size(113, 57);
            this.buttonInquiry.TabIndex = 12;
            this.buttonInquiry.Text = "照会管理";
            this.buttonInquiry.UseVisualStyleBackColor = false;
            this.buttonInquiry.Visible = false;
            this.buttonInquiry.Click += new System.EventHandler(this.buttonInquiry_Click);
            // 
            // buttonOther
            // 
            this.buttonOther.Enabled = false;
            this.buttonOther.Location = new System.Drawing.Point(259, 101);
            this.buttonOther.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOther.Name = "buttonOther";
            this.buttonOther.Size = new System.Drawing.Size(113, 57);
            this.buttonOther.TabIndex = 11;
            this.buttonOther.Text = "その他";
            this.buttonOther.UseVisualStyleBackColor = true;
            this.buttonOther.Visible = false;
            this.buttonOther.Click += new System.EventHandler(this.buttonOther_Click);
            // 
            // buttonSimpleSearch
            // 
            this.buttonSimpleSearch.Enabled = false;
            this.buttonSimpleSearch.Location = new System.Drawing.Point(138, 101);
            this.buttonSimpleSearch.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSimpleSearch.Name = "buttonSimpleSearch";
            this.buttonSimpleSearch.Size = new System.Drawing.Size(113, 57);
            this.buttonSimpleSearch.TabIndex = 10;
            this.buttonSimpleSearch.Text = "簡易検索";
            this.buttonSimpleSearch.UseVisualStyleBackColor = true;
            this.buttonSimpleSearch.Visible = false;
            this.buttonSimpleSearch.Click += new System.EventHandler(this.buttonSimpleSearch_Click);
            // 
            // buttonDataExport
            // 
            this.buttonDataExport.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonDataExport.Location = new System.Drawing.Point(259, 29);
            this.buttonDataExport.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDataExport.Name = "buttonDataExport";
            this.buttonDataExport.Size = new System.Drawing.Size(113, 57);
            this.buttonDataExport.TabIndex = 8;
            this.buttonDataExport.Text = "納品データ\r\n出力";
            this.buttonDataExport.UseVisualStyleBackColor = false;
            this.buttonDataExport.Visible = false;
            this.buttonDataExport.Click += new System.EventHandler(this.buttonDataExport_Click);
            // 
            // buttonInput
            // 
            this.buttonInput.BackColor = System.Drawing.Color.LightSkyBlue;
            this.buttonInput.Location = new System.Drawing.Point(138, 29);
            this.buttonInput.Margin = new System.Windows.Forms.Padding(4);
            this.buttonInput.Name = "buttonInput";
            this.buttonInput.Size = new System.Drawing.Size(113, 57);
            this.buttonInput.TabIndex = 3;
            this.buttonInput.Text = "データ入力";
            this.buttonInput.UseVisualStyleBackColor = false;
            this.buttonInput.Click += new System.EventHandler(this.buttonInput_Click);
            // 
            // buttonScan
            // 
            this.buttonScan.BackColor = System.Drawing.Color.Turquoise;
            this.buttonScan.Location = new System.Drawing.Point(16, 29);
            this.buttonScan.Margin = new System.Windows.Forms.Padding(4);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(113, 57);
            this.buttonScan.TabIndex = 0;
            this.buttonScan.Text = "画像管理";
            this.buttonScan.UseVisualStyleBackColor = false;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 18);
            this.label2.TabIndex = 13;
            this.label2.Text = "審査年月を選択：";
            // 
            // dataGridViewMonth
            // 
            this.dataGridViewMonth.AllowUserToAddRows = false;
            this.dataGridViewMonth.AllowUserToDeleteRows = false;
            this.dataGridViewMonth.AllowUserToResizeColumns = false;
            this.dataGridViewMonth.AllowUserToResizeRows = false;
            this.dataGridViewMonth.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMonth.Location = new System.Drawing.Point(24, 42);
            this.dataGridViewMonth.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewMonth.MultiSelect = false;
            this.dataGridViewMonth.Name = "dataGridViewMonth";
            this.dataGridViewMonth.ReadOnly = true;
            this.dataGridViewMonth.RowHeadersVisible = false;
            this.dataGridViewMonth.RowTemplate.Height = 21;
            this.dataGridViewMonth.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMonth.Size = new System.Drawing.Size(300, 419);
            this.dataGridViewMonth.TabIndex = 14;
            // 
            // buttonSettings
            // 
            this.buttonSettings.Location = new System.Drawing.Point(145, 469);
            this.buttonSettings.Margin = new System.Windows.Forms.Padding(4);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(112, 35);
            this.buttonSettings.TabIndex = 18;
            this.buttonSettings.Text = "システム設定";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(24, 469);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(4);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(112, 35);
            this.buttonExit.TabIndex = 17;
            this.buttonExit.Text = "終了";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(363, 231);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(324, 185);
            this.panel1.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("ＭＳ 明朝", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(70, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "全国学校共済";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Georgia", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 23);
            this.label3.TabIndex = 9;
            this.label3.Text = "for";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 72F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(312, 109);
            this.label5.TabIndex = 7;
            this.label5.Text = "MEPS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(234, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Input System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Georgia", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(75, 155);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(152, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "Medibrain ,   Public School";
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersion.Location = new System.Drawing.Point(600, 477);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(181, 18);
            this.labelVersion.TabIndex = 20;
            this.labelVersion.Text = "version 1.2.0";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 512);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridViewMonth);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.buttonExit);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MEPS -メイン画面-";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMonth)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOther;
        private System.Windows.Forms.Button buttonSimpleSearch;
        private System.Windows.Forms.Button buttonDataExport;
        private System.Windows.Forms.Button buttonInput;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewMonth;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonInquiry;
        private System.Windows.Forms.Label labelVersion;
    }
}

